#!/bin/bash
#
# -----------------------------------------------------------------------------
# Crgrep start script.
#
# Author: Craig Ryan [cryan.dublin@gmail.com]
#
# Environment Variable Prequisites
#
#   CRGREP_HOME       (Optional) Home directory for crgrep
#   CRGREP_OPTS       (Optional) Java runtime options used when the command is
#                      executed.
#
#   CRGREP_TMPDIR     (Optional) Directory path location of temporary directory
#                      the JVM should use (java.io.tmpdir).  Defaults to
#                      $CRGREP_BASE/temp.
#
#   JAVA_HOME          Must point at your Java Development Kit installation.
#
#   CRGREP_JVM_OPTS   (Optional) Java runtime options used when the command is
#                       executed.
#
# -----------------------------------------------------------------------------

set -f

function usage()
{
    echo "Usage: $0 <args>"
    exit 1
}

[ $# -gt 0 ] || usage

ARGS="$@"

# OS specific support.  $var _must_ be set to either true or false.
cygwin=false
case "`uname`" in
CYGWIN*) cygwin=true;;
esac

# resolve links - $0 may be a softlink (including relative links)
PRGDIR=`dirname "$0"`
THIS_PROG=`cd "$PRGDIR" ; pwd`/`basename "$0"`

while [ -h "$THIS_PROG" ]; do
  ls=`ls -ld "$THIS_PROG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '^/.*' > /dev/null; then
    THIS_PROG="$link"
  else
    THIS_PROG=`dirname "$THIS_PROG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$THIS_PROG"`
CRGREP_HOME=`cd "$PRGDIR/.." ; pwd`

unset THIS_PROG

#if [ -r "$CRGREP_HOME"/bin/setenv.sh ]; then
#  . "$CRGREP_HOME"/bin/setenv.sh
#fi

# Checking for JAVA_HOME is required on *nix due
# to some distributions stupidly including kaffe in /usr/bin
if [ "$JAVA_HOME" = "" ] ; then
  echo "ERROR: JAVA_HOME not found in your environment."
  echo
  echo "Please, set the JAVA_HOME variable in your environment to match the"
  echo "location of the Java Virtual Machine you want to use."
  exit 1
fi

# For Cygwin, ensure paths are in UNIX format before anything is touched
if $cygwin; then
  [ -n "$CRGREP_HOME" ] && CRGREP_HOME=`cygpath --unix "$CRGREP_HOME"`
fi

if [ -z "$CRGREP_TMPDIR" ] ; then
  # Define the java.io.tmpdir to use for Crgrep
  CRGREP_TMPDIR="$CRGREP_HOME"/temp
  mkdir -p "$CRGREP_TMPDIR"
fi

# For Cygwin, switch paths to Windows format before running java
if $cygwin; then
  CRGREP_HOME=`cygpath --path --windows "$CRGREP_HOME"`
  CRGREP_TMPDIR=`cygpath --path --windows "$CRGREP_TMPDIR"`
fi

# OCR support
if [ x$TESSDATA_PREFIX == x ] 
then
  TESSDATA_PREFIX=$CRGREP_HOME/ocr
fi

export TESSDATA_PREFIX
if $cygwin; then
	if [ x$PROCESSOR_ARCHITECTURE == xx86 -a -n "$PROCESSOR_ARCHITEW6432" ]
	then
		CRGREP_LIB_PATH="$PATH:$CRGREP_HOME/ocr"
    else
		CRGREP_LIB_PATH="$PATH:$CRGREP_HOME/ocr/x64"
    fi
else 
	CRGREP_LIB_PATH="$PATH:$CRGREP_HOME/ocr"
	# In case any tesseract lib deps are placed into ocr/
	LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"$CRGREP_HOME/ocr"
	export LD_LIBRARY_PATH
fi

PATH="$PATH:$CRGREP_HOME/ocr"

# ----- Execute The Requested Command -----------------------------------------

#echo "Using CRGREP_HOME:   $CRGREP_HOME"
#echo "Using CRGREP_TMPDIR: $CRGREP_TMPDIR"
#echo "Using CRGREP_LIB_PATH: $CRGREP_LIB_PATH"
#echo "Using JAVA_HOME:      $JAVA_HOME"

# Uncomment to enable remote debugging
# DEBUG="-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=y"

"$JAVA_HOME"/bin/java $JVM_OPTS \
    $DEBUG \
    $CRGREP_JVM_OPTS \
    -Dcrgrep.home="$CRGREP_HOME" \
    -Djava.io.tmpdir="$CRGREP_TMPDIR" \
    -Djna.library.path="$CRGREP_LIB_PATH" \
    -Djava.ext.dirs="$CRGREP_HOME/lib" com.ryanfuse.crgrep.ResourceGrep $ARGS

exit 0
