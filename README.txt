Common Resource Grep

Version: 1.0.0, July 27th, 2014.

(C) Copyright 2013-2014 Craig Ryan. All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License
(LGPL) version 2.1 which accompanies this distribution, and is 
available at http://www.gnu.org/licenses/lgpl-2.1.html

CRGREP is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

Introduction
------------

CRGREP is  an open source COMMON  RESOURCE GREP utility to  search for
name and content matches in difficult  to access resource data such as
database  tables, ZIP  and  other archive  files,  images and  scanned
documents,  maven  dependencies,  web resources  and  combinations  of
supported resources.

INSTALL.txt: read this if you  download a binary distribution ready to
run.

BUILD.txt: read  this if you  download a source distribution  to build
CRGREP yourself.

README.txt (this file): read to discover what CRGREP is and how to use
it.

This document is accompanied by additional documents:

    docs/USAGE.txt			 usage, platform and configuration details
    docs/FILE_GREP.txt       file grep in detail
    docs/DATABASE_GREP.txt   database grep in detail
    docs/WEB_GREP.txt        web grep in detail

CRGREP in action
----------------

Here are some examples showing what you can do with CRGREP. 

1. What files and data are nested anywhere under my 'target' directory
   matching 'key' including data buried inside archives?

    $ crgrep -r key target
    
    target/simple_file.txt: a key moment
    target/misc.zip[misc/nested_monkey.txt]
    target/monkey-pics.txt:1:A file about happy monkeys.
    target/test-ear.ear[META-INF/MANIFEST.MF]:5:Created-By: Apache monkey
    
2. What column data in my database matches 'handle'?
   (~/.crgrep defines user/password)

    $ crgrep -d -U "jdbc:sqlite:/databases/db.sqlite3" handle '*'
    (relational database)

    customers: [id,name,status,joined_date,handle]
    customers: 3,Craig,active,2012-10-24 01:05:44,Craig's handle
    tags: 4,handle

    $ crgrep -d -U "http://localhost:7474/" handle '*'
    (Neo4J graph database)

	Node[3]:Customer {name:"Craig",handle:"Craig's handle"}
	Node[4]:Tags {tag:"handle"}
    
3. Does my scanned report document contain the word 'report'

    $ crgrep --ocr report report_scan.png

    report_scan.png:10: abc report for management
    
4. Which of my photo's are tagged with comments of our holiday in Perth?

    $ crgrep Perth pics/*.jpeg
    
    pics/pic1112.jpg: @{JpegComment=Lovely shot of Perth city, just arrived.} 
    pics/pic1113.jpg: @{JpegComment=Breakfast in a Perth cafe} 

5. Does the google home page contain a 'favicon' reference?

    $ crgrep google_favicon http://www.google.com
    
    http://www.google.com:<!doctype html><html itemscope="itemscope" itemtype="http://schema.org/WebPage"><head><meta itemprop="image" content="/images/google_favicon_128.png"><title>Google</title><script>(function(){
        
6. Do I have any maven (POM) dependencies in my project with content
   matching 'RunWith'?

    $ crgrep -m RunWith pom.xml
    
    C:/Users/Craig/.m2/repository/junit/junit/4.8.2/junit-4.8.2.jar[org/junit/runner/RunWith.class]

CRGREP supports arbitrary resources  nested within other resources and
will  recursively  search  for  matches  within  resources.  Therefore
arbitrary combinations of the above searches are possible.

Calling CRGREP
------------------------

All CRGREP operations involve a similar set of command line arguments:

    $ crgrep <pattern> <resource path(s)>

Simple wildcards ('*' and '?')  may be specified in either <pattern> or
<resource paths>.

To read from standard input (stdin) either specify no <resource path>:

    $ cat file.txt | crgrep sometext

or optionally specify a single hyphen, '-' in place of <resource path>.

See docs/USAGE.txt for further usage details.

Displaying Results
----------------------

Results are displayed in a format depending on the type of resource
and includes the name of  the resource, any nesting information, line
and page numbers and any matching text.

The basic format for displaying results for file based resources is

    <resource>[[:pagenum]:linenum:matching_content]

Some examples of CRGREP displayed results

     Output                                Match
     ------------------------------------------------------------------
     src/foo.java                          File listing match
     src/bar.txt:25:some text              File content match (+lineno)
     lib/all.zip[image.gif]                Archive file listing match
     lib/app.war[WEB-INF/web.xml]:6:<d..>  Archive file content match
     pom.xml->stuff.zip[doc.txt]           File listing match
     mypic.jpg: @{Size=25,Com=Scene}       File meta-data match
     TAB: [COL1,COL2,COL3]                 Table column name match
     TAB: data1,data2,data3                Table data match
     Node[1]:{name:"John"}                 Graph database node match
     sample.pdf:1:1:Sample PDF Document    Text extracted from a PDF 
                                           (+pageno and +linenum)

See docs/USAGE.txt for a detailed description of output formats.

File Grep
---------

A file grep will search for textual matches in the following resource
types:

* plain text files similar to normal grep
* resources within archives such as ZIP, TAR, WAR, EAR and JAR formats
* meta data in images (jpeg/gif/etc)
* text in scanned documents (jpeg/gif/tiff/bmp/png), extracted using Optical 
  Character Recognition (OCR) techniques.
* Maven POM files, following dependency trees of resource artifacts
* Text extracted from PDF files

The file docs/FILE_GREP.txt provides more details on file based grep.

Database Grep
-------------

By specifying a database grep  (-d option), the search will attempt to
match the search pattern against persisted data in either a relational
(SQL) database or graph database identified by a URI. For example

	$ crgrep -d -U jdbc:vendor:db 'john' 'mytable.column*'

See docs/DATABASE_GREP.txt for detailed database grep behaviour.

Web Grep
--------

A web page search is attempted when the <resource> begins with 'http://'
and no -d (database) option is specifed.

See docs/WEB_GREP.txt for usage and examples.
