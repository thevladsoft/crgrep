File Grep
-------------

A file based  grep will match text by resource  name and content, much
like a  combination of  'find' and 'grep'.  To exclude content  in the
search, the '-l' (listing only) option is specified.

Supported File Types
---------------------------------------

The file types supported include

	o Plain text files
	o Archives; zip, tar, 7z, jar, war, ear, etc..
	o Images; png, bmp, jpeg, gif, tif, ..
	o Maven pom; pom.xml
	o Documents; pdf

The exact search behaviour of CRGREP is determined by the file type.

Plain text files
---------------------------------------

For plain text file the search is a simple search of the file contents
and on the file name itself.

Arhives
---------------------------------------

For archives the files contained  within the archive are extracted (in
memory) and  their contents searched.  This process  may be recursive;
if files within an archive are themselves archives then these are fist
extracted and so on, to any depth.

Images
---------------------------------------

For  images,  the  meta  data  associated with  most  image  files  is
extracted  and then  searched  for  matches. If  the  --ocr option  is
specified  then OCR  text recognition  is enabled.  See  the following
'OCR' section for details.

Maven POM
---------------------------------------

Maven POM  files can be  processed and their  dependencies (artifacts)
included in the search path to a specified nesting limit (using the -m
option). A  simple example is where  a pom.xml has  a dependency entry
for an archive resource (zip or jar file) and CRGREP will include that
archive in  the search.  If  the archive referenced contains  files or
data which matches  the search pattern then those  will be included in
displayed results.

Be aware, if  enabled this search may result in  artifacts such as JAR
files  being downloaded into  your local  repository. If  the standard
$HOME/.m2/settings.xml   file  exists   then   default  active   proxy
configuration  will be  used to  access remote  repositories.  The POM
implementation does not make  use of -u/-p/-U/-P options for accessing
remote maven  repositories. See 'User Preferences'  for maven specific
settings.

Documents
---------------------------------------

PDF files are filtered to  extract text content which is then searched
for matches. The result displayed include page, line and text matches.

Optical Character Recognition (OCR)
---------------------------------------

CRGREP  supports  OCR,  or   'Optical  Character  Recognition',  which
involves scanning  image files containing characters  (alphabetics and
symbols)  and  translating  this  data  to  text  which  can  then  be
searched.  Many printers  offer similar  functionality for  scanning a
page and saving recognised text to a document file on a PC.

The CRGREP solution uses the open-source C++ based 'Tesseract' library
from Google.  On Windows platforms  CRGREP comes with  pre-bundled OCR
DLLs and English (only) language data and will work 'out of the box'.

The   64bit   versions   of   the   DLLs   were   taken   from   here:
https://github.com/charlesw/tesseract

If  you wish  to include  additional  language data  support then  see
INSTALL.txt for details.

If you're  using CRGREP on  Linux/MacOS then the native  libraries for
those platforms  will first need to  be installed in order  for OCR to
function. There  is a section  in INSTALL.txt describing  the required
steps.

A tip when using OCR,  results vary since recognising readable text in
an image  is not an exact science.   If you cannot get  output from an
image perhaps the  text has been interpreted wrongly  by OCR. If you'd
like to  see ALL the  text OCR  has extracted from  a file to  see why
you're not getting a match, run a wildcard pattern match:

    $ crgrep --ocr "*" file.png


Examples of File grep
---------------------------------------

        $ crgrep document myreport.pdf
        -> search a single file

        $ crgrep document file1 file2 file3
        -> search specific files

        $ crgrep -r foobah src/test/resources
        -> recursive search (including content) of files 
           under src/test/resources for 'foobah'

        $ crgrep -l foobah src/test/resources/*.ear
        -> search all '.ear' files in src/test/resources for 'foobah', 
           displaying matching files by name but not their content

        $ cat file.txt | crgrep document -
        -> search standard input (stdin)

