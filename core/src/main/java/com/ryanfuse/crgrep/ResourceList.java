/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.Iterator;

/**
 * Manages Resource list patterns provided on the command line.
 * 
 * Each implementation interprets the meaning of the list syntax. The command line values 
 * stored use basic wildcard characters, while the implementation specific value may 
 * present the characters using R.E. or SQL wildcards etc.
 * 
 * For file lists, the general format includes three parts: the path, the resource name and 
 * resource extent. ie 'path<seperator>name.extent'.
 *
 * For DB lists the syntax is <table>.<column> for relation databases and <label>.<property>
 * for graph databases.
 *
 * Only the first part is required, the path and last part of each entry is optional.
 * 
 * @author Craig Ryan
 * @param <E> the list type
 */
public interface ResourceList<E> extends Iterator<E> {
	// number of items in the list of resource paths
	int size();
	// List operation to access items by index
	E get(int index);
	
	// the path part of the current list item
	String pathPart();
    // the first part of the resource name for the current list item
    String firstPart();
	// the first part of the resource name for the current list item, as an implementation specific pattern
	String firstPartPattern();
    // the first part of the resource name as the underlying ResourcePattern (matcher)
	ResourcePattern firstPartResourcePattern();
	// the last part of the resource name for the current list item
	String lastPart();
	// the last part of the resource name for the current list item, as an implementation specific pattern
	String lastPartPattern();
    // the last part of the resource name as the underlying ResourcePattern (matcher)
	ResourcePattern lastPartResourcePattern();
	// is the entire path wildcarded for the current list item
	boolean isWild();
	// is the path wild for the current list item
	boolean isPathWild();
	// does the path contain any wildcard chars for the current list item
	boolean pathContainsWild();
	// is the first part of the resource name wild for the current list item
	boolean isFirstPartWild();
	// does the name contain any wildcard chars for the current list item?
	boolean firstPartContainsWild();
	// is the last part of the resource name wild for the current list item
	boolean isLastPartWild();
	// does the extent contain any wildcard chars for the current list item?
	boolean lastPartContainsWild();
}
