/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;

import com.ryanfuse.crgrep.util.Switches;

/**
 * Grep the content retrieved by a HTTP GET.
 * 
 * @author Craig
 */
public class HttpGrep extends AbstractGrep {

	public HttpGrep(Switches switches) {
		super(switches);
	}

	@Override
	public void execute() throws GrepException {
		String surl = getResourceList().get(0);
		URL url = null;
		String inputLine;
		try {
		    url = new URL(surl);
		} catch (MalformedURLException e) {
			getDisplay().debug("Bad HTTP URL, error: " + e.getLocalizedMessage());
            throw new GrepException("crgrep: failed http grep, bad URL '" + surl + "'", e);
		}
		BufferedReader in;
		try {
		    URLConnection con = url.openConnection();
		    con.setReadTimeout(3000);
		    con.setConnectTimeout(3000);
		    in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		    while ((inputLine = in.readLine()) != null) {
		    	Matcher matcher = getResourcePattern().getPattern().matcher(inputLine);
				if (matcher.find()) {
					getDisplay().result(surl + ":" + inputLine);
				}
		    }
		    in.close();
		} catch (IOException e) {
			ignoreResource(surl, e.getLocalizedMessage());
            throw new GrepException("crgrep: failed http grep for '" + surl + "'", e);
		}
	}
}
