/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.regex.Matcher;

/*
 * A class representing text matched by the grep search.
 */
public class ResultText {
	
	public static final String HIDDEN_TEXT_MARKER = "...";
	
	// Number of leading chars in a match before we include those in the displayed text results
	public static final int DEFAULT_MIN_DATA_VALUE_PREFIX = 10;
	// Max matched text length after which we trim the value displayed in results
	public static final int DEFAULT_MAX_DATA_VALUE_LENGTH = 50;

	private int minPrefixIndex;
	private int maxResultLength;
	private ResourcePattern resourcePattern;
	private boolean removeLineBreaks = Boolean.FALSE;
	
	public ResultText() {
		setTextLimits(DEFAULT_MIN_DATA_VALUE_PREFIX, DEFAULT_MAX_DATA_VALUE_LENGTH);
	}
	
	public ResultText(int min, int max) {
		setTextLimits(min, max);
	}
	
	public ResultText(ResourcePattern pattern) {
		setTextLimits(DEFAULT_MIN_DATA_VALUE_PREFIX, DEFAULT_MAX_DATA_VALUE_LENGTH);
		resourcePattern = pattern;
	}
	
	public ResultText(int min, int max, ResourcePattern pattern) {
		setTextLimits(min, max);
		resourcePattern = pattern;
	}

	public String result(String val) {
		return result(val, "");
	}
	
	/**
	 * Get the result string from the supplied text.
	 *
	 * @param val the input text from which to construct a result string.
	 * @return result string, possibly trimmed
	 */
	public String result(String val, String defaultVal) {
		if (val == null) {
			return defaultVal;
		}
		if (val.length() < getMaxResultLength()) {
			return val;
		}
		return trimmedValue(val);
	}

	public ResourcePattern getResourcePattern() {
		return resourcePattern;
	}

	/*
	 * Set a pattern to compare the result against.
	 */
	public void setResourcePattern(ResourcePattern resourcePattern) {
		this.resourcePattern = resourcePattern;
	}

	private void setTextLimits(int min, int max) {
		setMinPrefixIndex(min);
		setMaxResultLength(max);
	}

	/*
	 * Process result string and limit the length of the string displayed.
	 *
	 * The rules to determine trimming behaviour are:
	 *   - if the string is short then return it in full
	 *   - if the string contains a pattern match then return a subset of characters 'around' the match
	 *   - if the string doesn't contain a pattern match then return a subset starting from the beginning of the text
	 * The characters '...' are used to identified text trimmed (not displayed).
	 */
	private String trimmedValue(String val) {
		String trimmedVal;
		if (resourcePattern != null) {
			Matcher pm = resourcePattern.getPattern().matcher(val);
			if (pm.find()) {
				trimmedVal = stringRange(pm.start(), val);
			} else {
				trimmedVal = stringRange(0, val);
			}
		} else {
			trimmedVal = stringRange(0, val);
		}
		if (removeLineBreaks) {
			trimmedVal = filterLineBreaks(trimmedVal);
		}
		return trimmedVal;
	}

	private String filterLineBreaks(String trimmedVal) {
		if (trimmedVal == null) {
			return null;
		}
		return trimmedVal.replaceAll("\\r\\n|\\r|\\n", "\\\\n");
	}

	/*
	 * Extract a string 'around' the start:end index range.
	 *
	 * @param start index into the string 
	 * @param val the string to be trimmed.
	 * @return a subset of the string within the range
	 */
	private String stringRange(int start, String val) {
		int valLength = val.length();
		int maxConfiguredLength = getMaxResultLength();
		if (maxConfiguredLength < 1) {
			maxConfiguredLength = valLength; // unlimited.. so set to end of text
		}
		if (start < 0 || start >= valLength) {
			start = 0;
		}
		if (start > minPrefixIndex) {
			// enough room to include leading chars before the matching text
			start -= minPrefixIndex;
		}
		int end = start + maxConfiguredLength;
		if (end > valLength) {
			end = valLength;
		}
		if (start == 0 && end == valLength) { // the full text
			return val;
		}
		StringBuilder sb = new StringBuilder();
		if (start > 0) {
			sb.append(HIDDEN_TEXT_MARKER); // indicate leading chars not shown
		}
		sb.append(val.substring(start, end));
		if (end < valLength) {
			sb.append(HIDDEN_TEXT_MARKER); // indicate trailing chars not displayed
		}
		return sb.toString();
	}
	
	/*
	 * The aim is to show the matched text in the context of characters 
	 * before and after it, if possible. This setting determines how many
	 * characters must exist before the match text before we include those 
	 * characters in the displayed text. 
	 */
	public int getMinPrefixIndex() {
		return minPrefixIndex;
	}
	
	public void setMinPrefixIndex(int minPrefixIndex) {
		if (minPrefixIndex < 0) {
			minPrefixIndex = 0;
		}
		this.minPrefixIndex = minPrefixIndex;
	}
	
	public int getMaxResultLength() {
		return maxResultLength;
	}
	
	public void setMaxResultLength(Integer maxIndex) {
	    if (maxIndex == null) {
	        return;
	    }
		this.maxResultLength = maxIndex;
	}

	/*
	 * Determines if we want to display line breaks as '\n'
	 * instead of breaking displayed text across multiple lines.
	 */
	public boolean isRemoveLineBreaks() {
		return removeLineBreaks;
	}

	public void setRemoveLineBreaks(boolean stripLineBreaks) {
		this.removeLineBreaks = stripLineBreaks;
	}
}
