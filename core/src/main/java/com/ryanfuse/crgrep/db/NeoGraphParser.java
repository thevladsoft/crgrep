/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;

import org.apache.commons.collections.IteratorUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.rest.graphdb.RestGraphDatabase;
import org.neo4j.rest.graphdb.entity.RestNode;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;
import org.neo4j.rest.graphdb.util.QueryResult;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceList;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.util.Display;

/**
 * Parser class to query and parse data from a Neo4J graph database.
 * 
 * This parser uses a java wrapper for the Neo4J REST API so that 
 * calls are made to the remote server.
 * 
 * @author Craig Ryan
 */
public class NeoGraphParser {
	
	protected static final String NEO_DEF_USER = null;
	protected static final String NEO_DEF_PW = null;
	private static final String LOG_PREFIX = "Neo4j";
    private static final String REST_CONTEXT = "db/data";

    protected String uri;
    protected String user;
    protected String password;
    private ResourceList<String> resourceList;
    private boolean ignoreCase;
    private boolean verbose;
    private Display display;
    
    private RestCypherQueryEngine engine;
    private GraphDatabaseService graphdb;
    private boolean allContent;
    
	public NeoGraphParser(String uri, String user, String password) {
        this.uri = uri;
        this.user = user == null ? getDefaultUser() : user;
        this.password = password == null ? getDefaultPassword() : password;
        //doInitConnection();
	}

	/*
	 * Ensure we can connect to the database, either with the supplied URI or 
	 * modified to use the default RESTful URI.
	 */
    public boolean openConnection() {
        if (uri == null) {
            return false;
        }
        if (graphdb == null) {
            resetEngine();
        }
        if (pingDatabase()) {
            registerShutdownHook(graphdb);
            return true;
        }
        shutdownGraph();
        // 2nd attempt, try ensure a RESTful URI was specified and not just
        // the database browser URI.
        if (!uri.endsWith(REST_CONTEXT)) {
            if (uri.endsWith("/")) {
                uri += REST_CONTEXT;
            } else {
                uri += "/" + REST_CONTEXT;
            }
        }
        resetEngine();
        if (pingDatabase()) {
            registerShutdownHook(graphdb);
            return true;
        }
        return false;
    }

    /*
     * Initialise the RESTful wrapper and query engine.
     */
    private void resetEngine() {
        graphdb = user == null ? new RestGraphDatabase(uri) : new RestGraphDatabase(uri, user, password);
        engine = new RestCypherQueryEngine(((RestGraphDatabase)graphdb).getRestAPI());
    }
    
    /*
     * Ping ensures the DB is reachable and can be queried.
     */
    private boolean pingDatabase() {
        if (graphdb == null || !graphdb.isAvailable(10)) {
            return false;
        }
        try {
            engine.query("MATCH (n) RETURN n LIMIT 0", null);
        } catch (Exception e) {
//            e.printStackTrace();
            debug("Graph database ping operation failed: " + e.getLocalizedMessage());
            return false; 
        }
        return true;
    }

	/**
	 * The map contains all nodes matched by label, and the complete properties list for those nodes.
	 */
	public Map<String, List<Map.Entry<String, List<String>>>> getMatchingNodeMap() throws Exception {
        Collection<String> labels = ((RestGraphDatabase)graphdb).getAllLabelNames();
        Transaction tx = graphdb.beginTx();
        Map<String, List<Map.Entry<String, List<String>>>> nodeMap = new HashMap<String, List<Map.Entry<String, List<String>>>>();
		for (String label : labels) {
		    // Does the label match?
		    if (!resourceList.isFirstPartWild()) {
		        if (resourceList.firstPartContainsWild()) {
		            Matcher pm = resourceList.firstPartResourcePattern().getPattern(true).matcher(label);
		            if (!pm.find()) {
		                continue;
		            }
		        } else {
		            if (!label.toUpperCase().equals(resourceList.firstPart().toUpperCase())) {
		                continue;
		            }
		        }
		    }
		    debug("Node label '" + label + "' included in search for matching pattern [" + resourceList.firstPart() + "]");
		    
		    // Get the properties for the matched <label>
		    Iterable<RestNode> nodeIterable = ((RestGraphDatabase)graphdb).getRestAPI().getNodesByLabel(label);
		    Iterator<RestNode> nodeIter = nodeIterable.iterator();
		    while (nodeIter.hasNext()) {
		        RestNode node = nodeIter.next();
		        String nodeId = String.valueOf(node.getId());
		        Iterable<String> props = node.getPropertyKeys();
                List<String> matchedProps = IteratorUtils.toList(props.iterator());
                List<Map.Entry<String, List<String>>> entryList = nodeMap.get(label);
                if (entryList == null) {
                    debug("no node map entry yet for " + label);
                    entryList = new ArrayList<Map.Entry<String, List<String>>>();
                    nodeMap.put(label, entryList);
                }
		        if (!matchedProps.isEmpty()) {
                    debug("Properties for node with id: " + nodeId + " count: " + matchedProps.size());
                    debug("  Props: " + matchedProps.toString());
		            // map entry node ID -> list of node properties
		            Map.Entry<String, List<String>> entry = new AbstractMap.SimpleEntry(nodeId, matchedProps);
                    entryList.add(entry);
		        } else {
                    debug("Empty Properties for node with id: " + nodeId);
		            // Node has no properties, map only node ID
                    entryList.add(new AbstractMap.SimpleEntry(nodeId, new ArrayList<String>()));
		        }
		    }
		    if (isDebugOn()) {
		        List<Entry<String, List<String>>> l = nodeMap.get(label);
		        debug("Number of nodes mapped for label '" + label + "' is " + (l==null?0:l.size()));
		    }
		}
        tx.success();
        tx.close();
		return nodeMap;
	}


	/**
	 * Scan node properties and attempt to match by value.
	 * 
	 * A property is either a primitive type or an array of primitive types.
	 * 
	 * @param nodeId the node ID
	 * @param prop the property name
	 * @param match buffer carrying result of the match
	 * @param pattern the pattern to match value against
	 * @return true if a property value match was found
	 */
    public boolean matchesPropertyValue(String nodeId, String prop, StringBuilder match, ResourcePattern pattern) {
        Transaction tx = graphdb.beginTx();
        Map<String,Object> params = new HashMap<String, Object>(); 
        params.put("id", Integer.parseInt(nodeId)); 
        String byId = "START n=node({id}) return n." + prop + " as val";
        debug("Content match query: " + byId + "  (id:"+nodeId+")");
        QueryResult<Map<String, Object>> result = engine.query(byId, params);
        tx.success();
        tx.close();
        boolean hit = false;
        for (Map<String,Object> row : result) { // only one row will be returned
            Object propVal = row.get("val");
            if (propVal == null) {
                match.append(":null");
            } else {
                StringBuilder valueMatch = new StringBuilder(":");
                if (propVal instanceof List) {
                    hit = matchArray(((List)propVal).toArray(), valueMatch, pattern);
                } else {
                    hit = matchPrimitive(propVal, valueMatch, pattern);
                }
                if (hit) {
                    if (isTraceOn()) {
                        trace("property match for {" + prop + valueMatch.toString() + "}");
                    }
                    match.append(valueMatch.toString());
                }
            }
        }
        debug("  --> hit?: " + hit);
        return hit;
    }

    /*
     * Property is an array, match each primitive array element by value
     */
    private boolean matchArray(Object[] propVal, StringBuilder valueMatch, ResourcePattern pattern) {
        boolean hit = false;
        boolean first = true;
        if (propVal.length > 0) {
            trace("Array property value, length: " + propVal.length);
            valueMatch.append("[");
            for (Object prop : propVal) {
                if (!first) {
                    valueMatch.append(",");
                }
                if (matchPrimitive(prop, valueMatch, pattern)) {
                    hit = true;
                }
                first = false;
            }
            valueMatch.append("]");
        }
        return hit;
    }
    
    /*
     * Match a property primitive value. The value is either a primitive property value or
     * for an array property, the primitive value of a single array element.
     */
    private boolean matchPrimitive(Object propVal, StringBuilder primValueMatch, ResourcePattern pattern) {
        boolean hit = false;
        if ((propVal instanceof String)
            || (propVal instanceof Character)) {
            if (matchString(propVal.getClass().getName(), true, String.valueOf(propVal), primValueMatch, pattern)) {
                hit = true;
            }
        }
        else if ((propVal instanceof Boolean)
            || (propVal instanceof Short)
            || (propVal instanceof Integer)
            || (propVal instanceof Long)
            || (propVal instanceof Float
            || (propVal instanceof Double))) {
            if (matchString(propVal.getClass().getName(), false, String.valueOf(propVal), primValueMatch, pattern)) {
                hit = true;
            }
        } else if (propVal instanceof Byte) {
            if (allContent) {
                if (matchString("byte", false, String.valueOf((Byte)propVal), primValueMatch, pattern)) {
                    hit = true;
                }
            }
        }
        return hit;
    }

    /*
     * Match property primitive value after conversion to String value.
     */
    private boolean matchString(String propType, boolean quoteValue, String propVal, StringBuilder stringMatch, ResourcePattern pattern) {
        if (propVal == null || propVal.isEmpty()) {
            //match.append(":\"\"");
        } else {
            Matcher pm = pattern.getPattern(ignoreCase).matcher(propVal);
            if (pm.find()) {
                if (quoteValue) {
                    stringMatch.append("\"");
                }
                stringMatch.append(propVal);
                if (quoteValue) {
                    stringMatch.append("\"");
                }
                return true;
            }
        }
        return false;
    }

    /*
     * Ensures clean db shutdown.
     */
    private void registerShutdownHook(final GraphDatabaseService graphDb) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }    
    
    public String getUri() {
        return uri;
    }
    
    public String getUser() {
        return user;
    }
   
    public String getPassword() {
        return password;
    }

    public String getDefaultUser() {
        return NEO_DEF_USER;
    }

    public String getDefaultPassword() {
        return NEO_DEF_PW;
    }

    public String getLogPrefix() {
        return LOG_PREFIX;
    }

    public ResourceList<String> getResourceList() {
        return resourceList;
    }

    public void setResourceList(ResourceList<String> resourceList) {
        this.resourceList = resourceList;
    }
    
    public void cleanup() throws GrepException {
        try {
            shutdownGraph();
        } catch (Exception e) {
            throw new GrepException("Failed to shutdown graph database: " + e.getLocalizedMessage());
        } 
    }
    
    public void trace(String msg) {
        if (display != null) {
            display.trace(msg);
        }
    }

    public void debug(String msg) {
        if (display != null) {
            display.debug(msg);
        }
    }

    public boolean isDebugOn() {
        if (display != null) {
            return display.isDebugOn();
        }
        return false;
    }

    public boolean isTraceOn() {
        if (display != null) {
            return display.isTraceOn();
        }
        return false;
    }

    public void warning(String msg) {
        if (display != null) {
            display.warn(msg);
        }
    }

    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public Display getDisplay() {
        return display;
    }

    public void setDisplay(Display display) {
        this.display = display;
        if (display != null) {
            display.setLogPrefix(getLogPrefix());
        }
    }
    
    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }

    public boolean isIgnoreCase() {
        return this.ignoreCase;
    }

    public void setEngine(RestCypherQueryEngine restCypherQueryEngine) {
        engine = restCypherQueryEngine;
    }

    public void setGraphDatabase(GraphDatabaseService restGraphDatabase) {
        graphdb = restGraphDatabase;
    }

    public void shutdownGraph() {
        try {
            if ( graphdb != null ) {
                graphdb.shutdown();
            }
        } finally {
            graphdb = null;
        }
    }

    public void setAllContent(boolean allContent) {
        this.allContent = allContent;
    }
}
