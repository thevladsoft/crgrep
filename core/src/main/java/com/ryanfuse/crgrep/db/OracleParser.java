/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.ryanfuse.crgrep.ResourcePattern;

/**
 * Parser class to query and parse data from an Oracle database.
 * 
 * @author Craig Ryan
 */
public class OracleParser extends DatabaseParser {
	
	public static final String ORA_DEF_USER = "sysdba";
	public static final String ORA_DEF_PW = "password";
	private static final String LOG_PREFIX = "Oracle";
	private static final String GREPABLE_TYPES = "('VARCHAR2','VARCHAR','CHAR','NCHAR','NVARCHAR2',"
	        + "'NUMBER','XML','URI','DATE','TIMESTAMP','DATETIME')";

	private Connection conn = null;
	private String tables = null;

	public OracleParser(String uri, String user, String password) {
		super(uri, user, password);
		tables = getUser().equals(ORA_DEF_USER) ? "all_tab_columns" : "user_tab_columns";
	}

	@Override
	public String getDefaultUser() {
		return ORA_DEF_USER;
	}

	@Override
	public String getDefaultPassword() {
		return ORA_DEF_PW;
	}

	public String getLogPrefix() {
		return LOG_PREFIX;
	}

	@Override
	public Connection dbConnection() throws Exception {
		if (conn != null && !conn.isClosed()) {
			return conn;
		}
		if (getDbDriver() == null) {
			Class.forName ("oracle.jdbc.OracleDriver");
		}
		Properties props = new Properties();
		// possibly avoid OAUTH exceptions by using the older O3Logon authenticate protocol.
		// Starting with 11g release, JDBC thin driver implements O5Logon challenge-response which can be problematic.
		// See this thread for more details: https://community.oracle.com/message/10297234
		props.put("oracle.jdbc.thinLogonCapability", "o3");
		conn = openConnection(props);
		int len = getUri().length();
		len = len > 40 ? 40 : len;
		debug("DB CONNECT: user=" + getUser() + ",pw=" + getPassword() + ",uri=" + getUri().substring(0,len) + "...");
		return conn;
	}

	public void dbClose() throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
	
	@Override
	public Map<String, List<String>> getFullTableMap() throws Exception {
		String allColsQuery = "select TABLE_NAME, COLUMN_NAME from " + tables;
		/*
		 * Table/column syntax:
		 *    table.col  	exact column
		 *    .col 			all tables with specific column
		 *    table 		all columns from table
		 *    table*.*col? - limited wildcarded table name(s) and column(s)
		 */
		String tabFilter = dbResourceListFilter();
		
		String query = allColsQuery + (tabFilter == null ? "" : " WHERE " + tabFilter);
		trace("DB TABLE COLUMNS QUERY: " + query);		
		ResultSet rs = queryDb(dbConnection(), query);
		Map<String, List<String>> tableMap = mapTableColumns(rs);
		return tableMap;
	}

	@Override
	public List<String> getColumnSubset(String table) throws Exception {
	    StringBuilder colsQuery = new StringBuilder("select COLUMN_NAME from ");
        colsQuery.append(tables).append(" WHERE data_type IN ").append(GREPABLE_TYPES);
        colsQuery.append(" AND TABLE_NAME = '").append(table).append("'");
		debug("DB CONTENT SUBSET QUERY> " + colsQuery.toString());
		ResultSet rs = queryDb(dbConnection(), colsQuery.toString()); // non numeric/blob columns
		return listColumns(rs);
	}

	@Override
	public List<List<String>> getMatchingColumns(List<String> columnList, String table, ResourcePattern pattern) throws Exception {
		String dbPattern = pattern.getDatabaseWildcardExpression();
		String colCondition = buildColumnWhere(dbPattern, columnList);
		if (colCondition == null || colCondition.isEmpty()) {
			// no columns of the required type
			return null;
		}
		StringBuilder dataQuery = new StringBuilder();
        String colSelect = buildColumnSelect(columnList);
        dataQuery.append("SELECT ").append(colSelect).append(" from ").append(table);
		dataQuery.append(colCondition);
		debug("DB CONTENT QUERY> " + dataQuery.toString());
		ResultSet rs = queryDb(dbConnection(), dataQuery.toString());
		return resultSetToList(rs);
	}
	
	/*
	 * Build a map keyed by table name, and all columns in the result set (value=list of column names)
	 */
	private Map<String, List<String>> mapTableColumns(ResultSet rs) throws SQLException {
		Map<String, List<String>> tabs = new HashMap<String, List<String>>();
		while (rs.next()) {
			String tab = rs.getString("TABLE_NAME");
			String col = rs.getString("COLUMN_NAME");
			if (!tabs.containsKey(tab)) {
				debug("Add table [" + tab + "] to map.");
				tabs.put(tab, new ArrayList<String>());
			}
			trace("Map table [" + tab + "] column [" + col + "]");
			tabs.get(tab).add(col);
		}
		rs.close();
		return tabs;
	}

	/*
	 * Build a list of all columns in the result set
	 */
	private List<String> listColumns(ResultSet rs) throws SQLException {
		List<String> cols = new ArrayList<String>();
		while (rs.next()) {
			String col = rs.getString("COLUMN_NAME");
			if (!cols.contains(col)) {
				cols.add(col);
			}
		}
		rs.close();
		return cols;
	}
	
	/*
	 * Filter of tables and or columns based on limited RE.
	 *  table.col  		exact column
	 *  .col 			all tables with specific column
	 *  table 			all columns from table
	 *  table*.*col? 	limited wildcarded table name(s) and column(s)
	 */
	private String dbResourceListFilter() {
		String tab = null;
		String col = null;
		if (getResourceList() == null) {
			return null;
		}
		debug("Resource list: " + getResourceList().get(0));
		if (!getResourceList().isWild()) {
			if (!getResourceList().isFirstPartWild()) {
				tab = getResourceList().firstPartPattern();
			}
			if (!getResourceList().isLastPartWild()) { 
				col = getResourceList().lastPartPattern();
			}
		}
		StringBuilder q = new StringBuilder();
		if (tab != null) {
			if (getResourceList().firstPartContainsWild()) {
				q.append("TABLE_NAME LIKE '").append(tab).append("'");
			} else {
				q.append("TABLE_NAME = '").append(tab).append("'");
			}
		}
		if (col != null) {
			if (q.length() > 0) {
				q.append(" AND ");
			}
			// Oracle stores all objects as upper-case, the comparison
			// is case insensitive by using upper case 'col' value. 
			if (getResourceList().lastPartContainsWild()) {
				q.append("COLUMN_NAME LIKE '").append(col.toUpperCase()).append("'");
			} else {
				q.append("COLUMN_NAME = '").append(col.toUpperCase()).append("'");
			}
		}
		debug("TABLE LIST FILTER: " + q.toString());
		return q.length() == 0 ? null : q.toString();
	}

	/**
	 * How to represent a column name in a WHERE clause considering the current case sensitivity
	 * and DB's identifier quoting convention.
	 */
	@Override
	public String columnIdentifier(String col, boolean ignoreCase, boolean isSelect) {
		StringBuilder q = new StringBuilder();
        if (ignoreCase) {
			q.append(" UPPER(");
		}
		q.append(IDENTIFIER_DOUBLE_QUOTE).append(col).append(IDENTIFIER_DOUBLE_QUOTE);
        if (ignoreCase) {
			q.append(") ");
		}
		return q.toString();
	}	
}
