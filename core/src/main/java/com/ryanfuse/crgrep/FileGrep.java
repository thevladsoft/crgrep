/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import com.ryanfuse.crgrep.file.FileTypeFactory;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Grep files and directories.
 * 
 * A FileTypeFactory is called to supply a FileType implementation for each file resource
 * discovered. The FileType is then called to grep the resource, be it a plain file,
 * archive, image or other supported type of file resource.
 * 
 * @author Craig Ryan
 */
public class FileGrep extends AbstractGrep {

	private static final String BINARY_FILE_EXTENTS = ".*\\.(7z|exe|bzip2|bz2|tgz|class|o|obj|bin|rar|pdf|doc|docx|xsl)";
	
	private FileTypeFactory fileTypeFactory;
	
	public FileGrep(Switches switches) {
		super(switches);
		fileTypeFactory = new FileTypeFactory(this);
	}

	@Override
	public void execute() throws GrepException {
		try {
			grepResources();
		} catch (Exception e) {
			throw new GrepException("File crgrep failed.", e);
		}
	}

    /**
     * Main entry point for FileType implementations to pass back processing of a 
     * file resource they do not directly support.
     * 
     * The various FileType implementations do not communicate directly with each other
     * so, for example, the ArchiveFileType may discover an Image file within an archive
     * and by passing the file back through here the ImageFileType implementation will
     * have a chance to process the resource. 
     * 
     * Through this mechanism we're able to support any combination of arbitrary nesting
     * (eg text within an image nested in a jar which is nested in an EAR file which was
     * discovered via a POM artifact... and so on).
     * 
     * @param file
     */
    public void grepByContents(File file) {
        fileTypeFactory.getFileType(file).grepEntry(file);
    }
    
    /**
     * Alternative to {@link #grepByContents(File)} for entries which are read as streams
     * such as a resource within an archive.
     * 
     * @param entry the entry stream
     * @param entryName the name of the entry excluding any parent path
     * @param entryPath entry path including entry name
     */
    public void grepByContentsStream(InputStream entry, String entryName, String entryPath) {
        fileTypeFactory.getFileTypeByName(entryName).grepEntryStream(entry, entryName, entryPath);
    }
    
	/*
	 * Main entry point, grep all resources matching the resource pattern.
	 * 
	 * Handles file wilcards, but not path wildcards so
	 *     \a\b\c\foo?.j*             OK
	 *     \a\*\c\food.java    Wont work
	 */
	private void grepResources() {
		ResourceList<String> resList = getResourceList();
		if (resList.size() == 1 && resList.get(0).equals("-")) {
			// Read from STDIN
			grepByContentsStream(System.in, "-", null);
			return;
		}
		Collection<File> allFiles = new ArrayList<File>();
		while (resList.hasNext()) {
			String nextFile = resList.next();
			Collection<File> files = processFileArgument(nextFile); 
			debug("FileGrep processed resource argument '" + nextFile + "', resulted in " + files.size() + " paths.");
			if (files.isEmpty()) {
				getDisplay().error("crgrep: " + nextFile + ": No such file or directory");
				continue;
			}
			allFiles.addAll(files);
		}
		Iterator<File> matchingFiles = allFiles.iterator();
		while (matchingFiles.hasNext()) {
			File file = matchingFiles.next();
			debug("FileGrep MATCHING resource '" + file.getPath() + "'");
			fileTypeFactory.getFileType(file).grepEntry(file);
		}
	}
	
	private Collection<File> processFileArgument(String p) {
		String path = null;
		String wild = "*";
		boolean recurse = getSwitches().isRecurse();
		/*
		 * Prior to the use of Java 7 Paths pkg, using commons FilenameUtils but with difficulty.
		 * This is an approximation of the logic needed to support basic wildcards combined 
		 * with the recursive search option. 
		 * TODO: this needs a proper implementation to handle fully wildcarded paths.
		 */
		if (p.equals("*")) {
			p = "." + File.separator + "*";
		}
		File dir = new File(p);
		if (dir.exists()) {
			if (dir.isDirectory()) {
				if (!recurse) {
					return Arrays.asList(new File[]{dir});
				}
				// recursive.. continue
				path = p;
			} else {
				// file
				return Arrays.asList(new File[]{dir});
			}
		} else {
			path = FilenameUtils.getFullPathNoEndSeparator(p);
			if (path == null || path.isEmpty()) {
				path = ".";
				recurse = false;
			}
			wild = FilenameUtils.getName(p);
			if (!wild.equals("*")) {
				recurse = false;
			}
			dir = new File(path);
			if (!dir.exists()) {
				getDisplay().warn("crgrep: dont know how to process path '" + path + "' (try removing embedded wildcards)");
				return Collections.emptyList();
			}
		}
		WildcardFileFilter fileFilter = new FullPathWildcardFileFilter(wild);
		return FileUtils.listFiles(dir, fileFilter, recurse ? TrueFileFilter.INSTANCE : null);
	}

    class FullPathWildcardFileFilter extends WildcardFileFilter {
		private static final long serialVersionUID = 1L;
		private String systemWildcard;
		public FullPathWildcardFileFilter(String wildcard) {
			super(wildcard);
			this.systemWildcard = wildcard;
		}
		@Override
		public boolean accept(File file) {
			String name = file.getName();
			boolean accept = FilenameUtils.wildcardMatch(name, systemWildcard);
			trace("Accept [" + name + "] matching [" + systemWildcard + "]: " + accept);
			return accept;
		}    	
	}
	
	public void closeResource(Reader r) {
		if (r == null) {
			return;
		}
		try {
			r.close();
		} catch (IOException e) {
		}
	}

	public void closeResource(InputStream is) {
		if (is == null) {
			return;
		}
		try {
			is.close();
		} catch (IOException e) {
		}
	}

	// Guess if its binary or not
	public boolean isBinaryFile(File f) throws FileNotFoundException, IOException {
		Pattern archiveExtent = Pattern.compile(BINARY_FILE_EXTENTS, Pattern.CASE_INSENSITIVE);
		if (archiveExtent.matcher(f.getName()).matches()) {
			trace("FileGrep file '" + f.getName() + "' is binary (by file extent)");
			return true;
		}
		// still not sure - check the content for too many non-ascii chars
	    FileInputStream in = new FileInputStream(f);
	    byte[] data = new byte[512/*size*/];
	    int len = in.read(data);
	    in.close();
	    boolean isBin = isBinaryData(data, len);
		trace("FileGrep file '" + f.getPath() + "' is " + (isBin ? "" : "not") + " binary");
		return isBin;
	}

	public boolean isBinaryData(byte[] data, int length) {
	    int ascii = 0;
	    int other = 0;
	    for (int i = 0; i < length; i++) {
	        byte b = data[i];
	        if (b < 0x09) {
	        	return true;
	        }
	        if (b == 0x09 || b == 0x0A || b == 0x0C || b == 0x0D) {
	        	ascii++;
	        }
	        else if (b >= 0x20  &&  b <= 0x7E) {
	        	ascii++;
	        }
	        else {
	        	other++;
	        }
	    }
	    if (other == 0) {
	    	return false;
	    }
	    return (ascii + other) * 100 / other > 95;
	}
}
