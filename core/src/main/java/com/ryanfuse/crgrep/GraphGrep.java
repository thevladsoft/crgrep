/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;

import com.ryanfuse.crgrep.db.NeoGraphParser;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Grep a graph database for matches against node:label names and optionally properties (content).
 * 
 * @author Craig Ryan
 */
public class GraphGrep extends AbstractGrep {

	// Number of leading chars in a match before we include those in the displayed text results
	private static final int MIN_DATA_VALUE_PREFIX = 10;
	// Max matched text length after which we trim the value displayed in results
	private static final int MAX_DATA_VALUE_LENGTH = 50;
	
	private NeoGraphParser parser;
	
	public GraphGrep(Switches switches) {
		super(switches);
	}

	@Override
	public void execute() throws GrepException {
	    if (parser == null) {
	        parser = new NeoGraphParser(getUri(), getUser(), getPw());
//	        if (parser == null) {
//	            getDisplay().error("crgrep: unknown/unsupported graph database with URI " + getUri());
//	            return;
//	        }
	    }
	    parser.setDisplay(getDisplay());
	    parser.setResourceList(getResourceList());
	    parser.setIgnoreCase(getSwitches().isIgnoreCase());
	    parser.setAllContent(getSwitches().isAllcontent());
		try {
			grepGraph();
			parser.cleanup();
		} catch (Exception e) {
			getDisplay().error("crgrep: failed graph database grep [" + e.getLocalizedMessage() + "]");
			if (e instanceof GrepException) {
			    throw (GrepException)e;
			}
			throw new GrepException("crgrep: failed graph database grep", e);
		}
	}
	
	/*
	 * Generic entry point for all graph database parser implementations.
	 * 
	 * Examples of valid graph patterns and their effect on results:
	 * 
	 * 		crgrep  "*"     "FOO.*"          match all nodes (*) against all properties (.*) in nodes labeled FOO
	 * 		crgrep  "BAH"   "FOO.*"          match nodes containing BAH against all properties (.*) in node labeled FOO
	 * 		crgrep  "*"     "*.PROP"          match all labels against PROP attributes in all nodes
	 * 		crgrep  "*"     "LAB.C*"         match all properties against property names beginning with 'C' in node with label LAB
	 * 		crgrep  "BAH"   "T*.*"           match properties containing BAH against all properties (.*) in nodes with label matching 'T*'
	 */
	private void grepGraph() throws Exception {
	    //key:label -> list (node id's + properties)
	    if (!parser.openConnection()) {
	        getDisplay().error("Graph database not connected at '" + parser.getUri() + "'");
	        return;
	    }
	    
		Map<String, List<Map.Entry<String, List<String>>>> nodeMap = parser.getMatchingNodeMap(); 
		if (nodeMap == null || nodeMap.isEmpty()) {
		    getDisplay().warn("Graph database has no nodes with matching label!");
			return;
		}
        Set<Entry<String, List<Entry<String, List<String>>>>> matchedNodes = nodeMap.entrySet();
        for (Entry<String, List<Entry<String, List<String>>>> e : matchedNodes) {
            String label = e.getKey();
            List<Entry<String, List<String>>> nodes = e.getValue();
            // Match each node independently. Each node instance may vary by what properties it defines.
            // Each node is reported on its own line.
            for (Entry<String, List<String>> node : nodes) {
                String nodeId = node.getKey();
                List<String> includedProps = node.getValue();
                List<String> matchingProps = resourceListMatch(includedProps); 
                if (matchingProps.isEmpty()) {
                    // <table>.<column> pattern must match this table/column by name otherwise ignore the whole table.
                    continue;
                }
                // Grep for pattern against included label/properties
                if (isContent() || isAllcontent()) {
                    matchNodeContents(parser, label, nodeId, matchingProps);
                } else {
                    matchNodeListing(label, nodeId, matchingProps);
                }
            }
        }
	}

    /*
     * Subset of a node's properties which match required resource list pattern by name. 
     */
    public List<String> resourceListMatch(List<String> includedProps) throws Exception {
        List<String> matchedProps = new ArrayList<String>();
        trace("Included properties test against pattern [" + getResourceList().lastPart() + "]");
        if (!getResourceList().isLastPartWild()) {
            matchedProps = new ArrayList<String>();
            for (String prop : includedProps) {
                trace("Included property [" + prop + "] test (ignore case)");
                if (getResourceList().lastPartContainsWild()) {
                    Matcher pm = getResourceList().lastPartResourcePattern().getPattern(true).matcher(prop);
                    if (!pm.find()) {
                        continue;
                    }
                } else {
                    if (!prop.toUpperCase().equals(getResourceList().lastPart().toUpperCase())) {
                        continue;
                    }
                }
                trace("  -> matched!");
                matchedProps.add(prop);
            }
        } else {
            // all properties match
            matchedProps = includedProps;
        }
        if (matchedProps.size() > 0) {
            debug("Included properties count " + includedProps.size() + ", matching subset count " + matchedProps.size() 
                + " for pattern " + getResourceList().lastPart());
        }
        return matchedProps;
    }

	/*
	 * Match by node label and property names only.
	 */
    private void matchNodeListing(String label, String nodeId, List<String> includedProps) {
        // Of the filtered resources, attempt a match against <pattern>
        debug("GraphGrep match node listing (case always ignored)");
        StringBuilder match = new StringBuilder();
        // get case insensitive pattern matcher 
        Matcher pm = getResourcePattern().getPattern(true).matcher(label);
        boolean hit = pm.find();
        match.append("Node[").append(nodeId).append("]:").append(label);
        match.append(" {");
        boolean first = true;
        for (String p : includedProps) {
            pm = getResourcePattern().getPattern(true).matcher(p);
            if (pm.find()) {
                hit = true;
                if (!first) {
                    match.append(",");
                }
                match.append(p);
                first = false;
            }
        }
        match.append("}");
        if (hit) {
            getDisplay().result(match.toString());
        }
    }

    /*
     * Match by node label/properties name or property values (content).
     * Properties matching by name only display name, by value display both name/value.
     * If no property matches nor node label match, nothing is displayed.
     */
    private void matchNodeContents(NeoGraphParser par, String label, String nodeId, List<String> includedProps) {
        // Of the filtered resources, attempt a match against <pattern>
        StringBuilder match = new StringBuilder();
        // get case insensitive pattern matcher 
        Matcher pm = getResourcePattern().getPattern(true).matcher(label);
        boolean hit = false;
        hit = pm.find();
        match.append("Node[").append(nodeId).append("]:").append(label);
        boolean first = true;
        match.append(" {");
        for (String p : includedProps) {
            pm = getResourcePattern().getPattern(true).matcher(p);
            boolean nameMatched = pm.find();
            StringBuilder valueMatch = new StringBuilder();
            boolean valueMatched = par.matchesPropertyValue(nodeId, p, valueMatch, getResourcePattern());
            if (nameMatched || valueMatched) {
                hit = true;
                if (!first) {
                    match.append(",");
                }
                match.append(p);
                first = false;
                if (valueMatched) {
                    match.append(valueMatch);
                }
                debug("Prop [" + p + ":" + valueMatch.toString() + "] matches pattern [" + getResourcePattern().getPatternString() + "]");
            }
        }
        match.append("}");
        if (hit) {
            getDisplay().result(match.toString());
        } else {
            debug("No content match for [" + match.toString() + "]");
        }
    }

    // For testing, allow mock injection
    public void setParser(NeoGraphParser parser) {
        this.parser = parser;
    }
}
