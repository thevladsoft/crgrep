/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import com.ryanfuse.crgrep.ResourcePattern;

/**
 * Provides a matcher class to control which lines match the expected pattern.
 * Supports the -v option to reverse the match to lines not matching the pattern.
 * 
 * @author 'Craig Ryan'
 */
public class GrepMatcher {

    public static GrepMatcher getMatcher(ResourcePattern pattern, Switches switches) {
        return new GrepMatcher(pattern, switches);
    }
    
    private GrepMatcher(ResourcePattern pattern, Switches switches) {
        //switches.isReverseMatch();
    }
    
    public boolean find() {
        return false;
    }
}
