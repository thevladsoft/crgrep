/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.ryanfuse.crgrep.db.DatabaseFactory;
import com.ryanfuse.crgrep.db.DatabaseParser;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Grep a database for matches against column names and optionally column data (content).
 * 
 * @author Craig Ryan
 */
public class DbGrep extends AbstractGrep {

	// Number of leading chars in a match before we include those in the displayed text results
	private static final int MIN_DATA_VALUE_PREFIX = 10;
	// Max matched text length after which we trim the value displayed in results
	private static final int MAX_DATA_VALUE_LENGTH = 50;
	
	private DatabaseParser parser;
	
	public DbGrep(Switches switches) {
		super(switches);
	}

	@Override
	public void execute() throws GrepException {
	    if (parser == null) {
	        if ((parser = DatabaseFactory.getDatabaseParser(getUri(), getUser(), getPw())) == null) {
	            getDisplay().error("crgrep: unknown/unsupported database with URI " + getUri());
	            return;
	        }
	    }
		parser.setDisplay(getDisplay());
		parser.setResourceList(getResourceList());
		parser.setIgnoreCase(getSwitches().isIgnoreCase());
		try {
			grepDatabase(parser);
			parser.cleanup();
		} catch (Exception e) {
			getDisplay().error("crgrep: failed database grep [" + e.getLocalizedMessage() + "]");
			throw new GrepException("crgrep: failed database grep", e);
		}
	}
	
	/*
	 * Generic entry point for all DB parser implementations.
	 * 
	 * Examples of valid database patterns and their effect on results:
	 * 
	 * 		crgrep  "*"     "FOO.*"          match all rows (*) against all columns (.*) in table FOO
	 * 		crgrep  "BAH"   "FOO.*"          match rows containing BAH against all columns (.*) in table FOO
	 * 		crgrep  "*"     "*.COL"          match all rows against COL columns in all tables
	 * 		crgrep  "*"     "TAB.C*"         match all rows against column names beginning with 'C' in table TAB
	 * 		crgrep  "BAH"   "T*.*"           match rows containing BAH against all columns (.*) in tables named beginning with 'T'
	 */
	private void grepDatabase(DatabaseParser par) throws Exception {
		Map<String, List<String>> tableMap = par.getFullTableMap();
		if (tableMap == null) {
			getDisplay().warn("Database has no tables!");
			return;
		}
		Set<Entry<String, List<String>>> entrySet = tableMap.entrySet();
		for (Entry<String, List<String>> e : entrySet) {
			String tab = e.getKey();
			List<String> includedCols = null;
	        if (isAllcontent()) {
	            includedCols =  e.getValue(); // all columns
	        } else {
	            // Only columns whose types we're interested in (ie char/non-binary)
	            includedCols = par.getColumnSubset(tab);
	        }
			
			List<String> matchingCols = new ArrayList<String>();
            boolean resListMatches = resourceListMatch(par, tab, includedCols, matchingCols);
			
            if (!resListMatches) {
				// <table>.<column> pattern must match this table/column by name otherwise ignore the whole table.
                if (getSwitches().isDebug()) {
                    debug("DbGrep, table '" + tab + "' and its column names don't match list pattern '" + par.getResourceList().get(0) + "' (ignored)");
                }
				continue;
			}
            
            String colsCommaList = StringUtils.join(matchingCols.toArray(), ",").toUpperCase();
            String byNameResult = tab + ": [" + colsCommaList + "]";

            boolean byNameMatch = matchTableName(tab, colsCommaList, byNameResult);

			// content grep - grep all char data in each table (content) or all data (allContent)
            if (isContent() || isAllcontent()) {
				matchTableContents(par, tab, matchingCols, byNameMatch, byNameResult);
			}
		}
	}

    /*
	 * Grep for column data matches. 
	 */
	private void matchTableContents(DatabaseParser par,	String tab, List<String> matchingColumns, boolean byNameMatch, String byNameResult) throws Exception {
		debug("DbGrep match contents for table '" + tab + "'");
		boolean shownByName = false;
		if (!matchingColumns.isEmpty()) {
			List<List<String>> rs = par.getMatchingColumns(matchingColumns, tab, getResourcePattern());
			if (rs != null && !rs.isEmpty()) {
			    if (!shownByName && !byNameMatch) {
			        // We have column data hits but make sure we display table and columns
			        // by name to show what columns the data maps to.
			        getDisplay().result(byNameResult);
			        shownByName = true;
			    }
				// Matched on subset of columns, but display ALL column data in the result
		        displayTableHits(tab, matchingColumns, rs);
			}
		} else {
			// No columns to match. Ignore the table.
			debug("No columns to query for table '" + tab + "'");
		}
	}

	/*
	 * Match by table and column(s), this is always a case insensitive match
	 * as behaviour across RDB vendors is totally inconsistent. 
	 */
	private boolean matchTableName(String tab, String colsCommaList, String byNameResult) {
		// Of the filtered resources, attempt a match against <pattern>
		StringBuilder cb = new StringBuilder();
		cb.append(tab).append(": ").append(colsCommaList);
        debug("DBGrep match by name '" + cb.toString() + "' (case always ignored)");
        // get case insensitive pattern matcher 
		Matcher pm = getResourcePattern().getPattern(true).matcher(cb);
		if (pm.find()) {
		    getDisplay().result(byNameResult);
			return true;
		}
		return false;
	}

	/* 
	 * Determine if this table matches the resource list and is to be included in the search.
	 * true if the <table>.<column> pattern matches this table/column(s) by name.
	 */
	private boolean resourceListMatch(DatabaseParser parser, String tab, List<String> includedCols, List<String> matchingCols) {
		ResourceList<String> rl = parser.getResourceList();
		boolean tabMatches, colMatches = false;
        trace("DbGrep match resource list.");
        if (rl.isFirstPartWild()) {
            // '*' all tables match
            tabMatches = true;
        } else if (rl.firstPartContainsWild()) {
			tabMatches = matchPattern(tab, ResourcePattern.convertWildcardToRegex(rl.firstPart()));
            trace("DbGrep first part '" + rl.firstPart() + "' is wild, pattern matches '" + tab + "'? " + tabMatches);
		} else {
			tabMatches = matchString(tab, rl.firstPart());
            trace("DbGrep first part '" + rl.firstPart() + "' exact matches '" + tab + "'? " + tabMatches);
		}
        if (rl.isLastPartWild()) {
            // '*' all columns match
            matchingCols.addAll(includedCols);
            colMatches = true;
        } else {
            for (String col : includedCols) {
                boolean oneColMatches;
                if (rl.lastPartContainsWild()) {
                    oneColMatches = matchPattern(col, ResourcePattern.convertWildcardToRegex(rl.lastPart()));
                    trace("DbGrep last part '" + rl.lastPart() + "' is wild, pattern matches '" 
                            + col + "'? " + oneColMatches);
                } else {
                    oneColMatches = matchString(col, rl.lastPart());
                    trace("DbGrep last part '" + rl.lastPart() + "' exact matches '" 
                            + col + "'? " + oneColMatches);
                }
                if (oneColMatches) {
                    /*
                     * List of all columns which match the resource list
                     * pattern. These are the only columns included in the
                     * results.
                     */
                    matchingCols.add(col);
                    colMatches = true;
                }
            }
        }
		trace("DbGrep resource list matches table '" + tab + "'? " + (tabMatches && colMatches));
		// both table name and at least one column name must match the resource list pattern
		return tabMatches && colMatches;
	}

	// Match name against pattern ignoring case
	private boolean matchPattern(String name, String pattern) {
		trace("DbGrep match name '" + name + "' against pattern '" + pattern + "' (ignoreCase)"); //? " + getSwitches().isIgnoreCase() + ")");
		Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
		return p.matcher(name).matches();
	}

	// Match names exactly, ignoring case
	private boolean matchString(String name, String s2) {
		trace("DbGrep match name '" + name + "' against string '" + s2 + "' (ignoreCase)");// + getSwitches().isIgnoreCase() + ")");
		return name.equalsIgnoreCase(s2);
	}

	/*
	 * Display all rows from the table which match the grep criteria. Results are given as CSV.
	 */
	private void displayTableHits(String table, List<String> allCols, List<List<String>> rs) throws Exception {
		StringBuilder sb = new StringBuilder();
		int colCount = allCols.size();
		ResultText result = new ResultText(getResourcePattern());
		result.setMaxResultLength(getSwitches().getMaxResultLength());
		Iterator<List<String>> iter = rs.iterator();
		while (iter.hasNext()) {
			List<String> row = iter.next();
			sb.setLength(0);
			sb.append(table).append(": ");
			for (int i = 0; i < colCount; i++) {
				String val = row.get(i);
				if (i > 0) {
					sb.append(",");
				}
				sb.append(result.result(val)); // trimmed text
			}
			getDisplay().result(sb.toString());
		}
	}
	
	public void setParser(DatabaseParser par) {
	    this.parser = par;
	}
}
