/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

/**
 * Common crgrep exception for resource errors across all implementations. 
 * 
 * @author 'Craig Ryan'
 */
public class GrepException extends Exception {
	private static final long serialVersionUID = -4736750181784117788L;

	public GrepException(String msg) {
        super(msg);
    }
    public GrepException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
