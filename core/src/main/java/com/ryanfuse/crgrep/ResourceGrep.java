/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;

import com.ryanfuse.crgrep.util.Display;
import com.ryanfuse.crgrep.util.Switches;


/**
 * Common Resource Grep
 * <p>
 * This utility extends the standard plain text grep to support archives, databases, web, images and other resources, 
 * to match a text pattern against resource names and content. 
 * </p>
 * <p>  
 * Two levels of matches are supported, the default 'listing' grep which behaves like a 'find -name <pattern>' and 
 * detailed 'content' grep which behaves like a file grep.
 * </p>
 * <p>
 * If <user.home>/.crgrep exists this properties file can be used to set global options. 
 * <table>
 * <tr><td><b>Property<b></td><td><b>Use</b></td></tr>
 * <tr><td>user</td><td>user name (for authentication)</td></tr>
 * <tr><td>password</td><td>password for user</td></tr>
 * <tr><td>uri</td><td>URI (jdbc URL etc)</td></tr>
 * <tr><td>maven.remoteRepos</td><td>List of maven remote repo URLs (semi-colon seperated)</td></tr>
 * <tr><td>maven.localRepo</td><td>Local maven remote if not $HOME/.m2/repository</td></tr>
 * <tr><td>maven.depth</td><td>Limit the POM depth of search</td></tr>
 * <tr><td>http.proxyHost</td><td>Proxy host if required by your company</td></tr>
 * <tr><td>http.proxyPort</td><td>Proxy port</td></tr>
 * <tr><td>feature.ocr</td><td>Enable OCR ('true' or 'enable')</td></tr>
 * <tr><td>setting.maxResultLength</td><td>Max characters in a matched text value to display</td></tr>
 * <tr><td>setting.removeLineBreaks</td><td>Remove embedded line breaks from matching text (values: true or enable)</td></tr>
 * </table>
 * </p>
 * <p>
 * Typical Usages:
 * </p>
 * <table>
 * <th>
 *      <td>Resource</td>
 *      <td>Description</td>
 *      <td>Example</td>
 * </th>
 * <tr>
 * <td>Database</td>
 * <td>
 *  	Search for matching table and column names and [optionally] column data.<br/>
 *  	The arguments '<pattern> <table-column> search for '<pattern>' matches against<br/>
 *      the <table-column> specifier with format 'TABLE-PATTERN[.COLUMN-PATTERN]'.<br/>
 *      
 * </td>
 * <td>
 *      crgrep -d -l "foo" "CUSTOMER_*.*"
 * </td>
 * </tr>
 *      
 * <tr>
 * <td>File</td>
 * <td>
 *  	Search for matching files and content.<br/>
 *  	Text files are simply checked for matches. Archive files (tar, gz, zip, jar, war, ear) are all treated as directories<br/>
 *  	and there listings are matched. For a content match the files within archives are extracted and their contents are<br/>
 *  	matched. Only non-binary files are extracted.<br/>
 * 		Image file usage:<br/>
 * 		Greps the meta-data within an image such as description, vendor data.<br/>
 *		Maven POM - determine dependency tree and grep the tree (jar files on the classpath) for class files etc.
 * </td>
 * <td>
 *      crgrep "foo" "src/*"
 * </td>
 * </tr>
 *
 * <tr>
 * <td>HTTP</td>
 * <td>
 * 		Given a URL of the form 'http://..' attempt to fetch the page (similar to wget) and grep the result for matches.
 * 		Recursive could mean follow href links to fetch referenced pages also (not implemented).
 * 
 * </td>
 * <td>
 *      crgrep "favicon" "http://www.google.com"
 * </td>
 * </tr>
 * </table>
 *
 * @author Craig Ryan
 */
public class ResourceGrep {

    private static String VERSION = "1.0.0";

    private String user;
	private String pw;
	private String uri;
	private String proxy;
	private int proxyPort;
	private List<String> fileList;
	private String pattern;
	private AbstractGrep grep; 

    private static Options options = new Options();

    private Switches switches;

	/**
	 * Common Resource Grep.
	 * 
	 * @param args options followed by <pattern> <resources> (both support wildcards)
	 */
	public static void main(String[] args) {
		ResourceGrep rg = new ResourceGrep(args);
		try {
			rg.execute();
		} catch (GrepException e) {
			if (e.getCause() == null) {
				System.err.println("Failed crgrep. Reason: " + e.getLocalizedMessage());
			} else {
				System.err.println("Failed crgrep. Reason: " + e.getCause().getLocalizedMessage());
			}
		}
	}

	/**
	 * Mostly for testing, to separate creation from argument processing.
	 *
	 * Must call {@link #processCommandLineArgs(String[])} before calling
	 * {@link #execute()}.
	 */
	public ResourceGrep() {
	}
	
    public ResourceGrep(String[] args) {
	    processCommandLineArgs(args);
	}

    /**
     * Process options and arguments and prepare the grep'er for execute. 
     */
	public void processCommandLineArgs(String[] args) {
        if (determineSearchCriteria(args)) {
            createGrep();
        }
    }

    /**
	 * Execute the resource grep.
	 *
	 * @throws GrepException implementation specific error wrapped in a common GrepException 
	 */
	public void execute() throws GrepException {
		if (grep != null) {
			grep.execute();
		}
	}

	/*
	 * Parse options and arguments.
	 */
	private boolean determineSearchCriteria(String[] args) {
		// create the command line parser
		CommandLineParser parser = new GnuParser();

		// create the Options
		options.addOption( "r", "recurse", false, "Recursive search into resources" );
		// TODO: exclusions and inclusions to come, disable -e for now. 
		//options.addOption( "e", "exclude", false, "Exclude resource types. Comma sep. list. 'l' listing" );
		options.addOption( "l", "list", false, "List only the name of resources which produce a match. No content is displayed." );
		options.addOption( "a", "text", false, "Process binary files or database columns as if it were text" );
		options.addOption( "i", "ignore-case", false, "Ignore case distinctions in matched text" );
		options.addOption( "d", "database", false, "Database grep (disables file search)" );
		options.addOption( "u", "user", true, "User ID or username required to access a resource" );
        options.addOption( "U", "uri", true, "URI to specify a JDBC database resource" );
        options.addOption( "m", "maven", false, "Include Maven POM file dependencies in search");
        options.addOption( OptionBuilder.withLongOpt("ocr")
        	.withDescription("Enable OCR text extraction from images; requires tesseract libraries. See README.txt")
        	.create());
		options.addOption( "p", "password", true, "Password required to access a resource, optionally used with -u" );
		options.addOption( "P", "proxy", true, "Proxy settings for http access, specified as <host>[:<port>]" );
        //options.addOption( "v", "invert-match", false, "Invert the sense of matching, to select non-matching lines" );
		options.addOption( "h", "help", false, "Help" );
        options.addOption( "V", "version", false, "Print the version number of CRGREP to the standard output stream" );
        options.addOption(OptionBuilder.withLongOpt("warn").withDescription("Display all warnings to standard output").create());
        options.addOption( 
                OptionBuilder.withLongOpt("extensions")
                    .withDescription("Enable one or more extensions; comma sep. list such as -Xdebug,trace")
                    .hasArg()
                    .create('X'));
		CommandLine line;
		try {
		    // parse the command line arguments
		    line = parser.parse( options, args );

		    switches = new Switches(line);
		    
            if (line.hasOption('h')) {
                usage(null);
                exitWithStatus(0);
                return false; // safeguard if exitWithStatus is mocked in tests (same for all following calls)
            }
            if (line.hasOption('V')) {
                printVersion();
                exitWithStatus(0);
                return false;
            }
		    if (line.hasOption('u')) {
		    	user = line.getOptionValue('u');
		    }
		    if (line.hasOption('p')) {
		    	pw = line.getOptionValue('p');
		    }
		    if (line.hasOption('P')) {
		    	try {
		    		String px = line.getOptionValue('P').trim();
		    		if (!px.contains("://")) {
		    			px = "http://" + px;
		    		}
		    		URL pr = new URL(px);
		    		pr.toURI(); // limited validation of the URL
		    		proxy = pr.getHost();
		    		proxyPort = pr.getPort();
		    		System.setProperty("http.proxyHost", proxy);
		    		System.setProperty("http.proxyPort", String.valueOf(proxyPort));
		    	} catch (MalformedURLException e) {
                    usage("Invalid proxy URL syntax: " + e.getLocalizedMessage());
                    exitWithStatus(1);
                    return false;
		    	} catch (URISyntaxException e) {
                    usage("Invalid proxy URL syntax: " + e.getLocalizedMessage());
                    exitWithStatus(1);
                    return false;
                }
		    }
		    if (line.hasOption('U')) {
		    	uri = line.getOptionValue('U');
		    }
		    if (line.hasOption('X')) {
		    	String ex = line.getOptionValue('X');
		    	switches.processExtensions(ex);
		    }
		}
		catch (ParseException exp) {
		    usage("Unable to read command line arguments: " + exp.getMessage());
            exitWithStatus(1);
			return false;
		} 
	
		loadUserProperties();
				
		String[] postOptionArgs = line.getArgs();
		if (postOptionArgs == null || postOptionArgs.length < 1) {
            usage("No arguments given, expecting <search-pattern> [<resources-pattern>]");
            exitWithStatus(1);
            return false;
		} else {
			// <pattern> [<resources>]
			pattern = postOptionArgs[0];
			
			if (postOptionArgs.length == 1) {
			    if (switches.isDatabase()) {
		            usage("Database grep requires both <search-pattern> and <resources-pattern> arguments");
		            exitWithStatus(1);
		            return false;
			    } else {
			        fileList = Arrays.asList("-");
			    }
			} else {
				fileList = new ArrayList<String>(Arrays.asList(postOptionArgs));
				fileList.remove(0);
			}
		}
		
		return true;
	}

	/*
	 * Check for ~/.crgrep for properties. These are only used if command line equivalents are not set (or available)
	 */
    private void loadUserProperties() {
    	String home = System.getProperty("user.home");
    	File dotFile = new File(home, ".crgrep");
    	if (!dotFile.exists() || !dotFile.isFile()) {
    		return;
    	}
    	Properties p = new Properties();
    	try {
    		//getDisplay().trace("Loading user preferences from file '" + dotFile.getPath());
			p.load(FileUtils.openInputStream(dotFile));
		} catch (IOException e) {
            System.err.println("Warning: failed to read user preferences from file .crgrep in HOME directory '" + home + "'.");
			return;
		}
    	switches.setUserProperties(p);
    	
    	// Command line settings take precedence
    	if (user == null) {
    		user = p.getProperty("user");
    	}
    	if (pw == null) {
    		pw = p.getProperty("password");
    	}
    	if (uri == null) {
    		uri = p.getProperty("uri");
    	}
    	if (proxy == null) {
    	    proxy = p.getProperty("http.proxyHost");
    	    if (proxy != null) {
    	        System.setProperty("http.proxyHost", proxy);
    	    }
    	}
    	if (proxyPort == 0) {
    		String port = p.getProperty("http.proxyPort");
    	    try {
    	        if (port != null) {
    	            proxyPort = Integer.parseInt(port); 
    	            System.setProperty("http.proxyPort", String.valueOf(proxyPort));
    	        }
            } catch (NumberFormatException e) {
                System.err.println("Warning: http.proxyPort value '" + port + "' in $HOME/.crgrep ignored as invalid number");
                // ignore
            }
    	}
    	if (!switches.isOcr()) {
    		String ocr = p.getProperty("feature.ocr");
    		if (ocr != null && (ocr.equals("true") || ocr.equals("enable"))) {
    			switches.setOcr(true);
    		}
    	}
        String max = p.getProperty("setting.maxResultLength");
		if (max != null) {
			int maxResult = 0;
			try {
				maxResult = Integer.parseInt(max);
				switches.setMaxResultLength(maxResult);
			} catch (NumberFormatException nf) {
			    System.err.println("Warning: setting.maxResultLength value '" + max + "' in $HOME/.crgrep ignored as invalid number");
			}
		}
        String stripLineBreaks = p.getProperty("setting.removeLineBreaks");
        if (stripLineBreaks != null && (stripLineBreaks.equals("true") || stripLineBreaks.equals("enable"))) {
            switches.setRemoveLineBreaks(true);
        }
        
    }

	/*
	 * Establish a grep'er.
	 */
	private void createGrep() {
		ResourcePattern rp = new ResourcePattern(pattern);
		rp.setIgnoreCase(switches.isIgnoreCase());
		if (switches.isDatabase()) {
		    if (uri == null) {
		        usage("Missing URL for database search (-U / --uri)");
                exitWithStatus(1);
                return;
		    }
		    if (fileList.size() > 1) {
                usage("Only one resource is supported for database searches following argument [" + pattern + "].");
                exitWithStatus(1);
                return;
            }
		    if (uri.startsWith("http:")) {
		        grep = new GraphGrep(switches);
		    } else {
		        grep = new DbGrep(switches);
		    }
			grep.setArguments(rp, new DbResourceList(fileList.get(0)));
		} else {
			// file or http
			if (fileList.get(0).startsWith("http:")) {
				if (fileList.size() > 1) {
					usage("Only one URI is supported for HTTP searches following argument [" + pattern + "].");
					exitWithStatus(1);
	                return;
				}
				grep = new HttpGrep(switches);
				grep.setArguments(rp, new FileResourceList(fileList));
			} else {
				grep = new FileGrep(switches);
				grep.setArguments(rp, new FileResourceList(fileList));
			}
		}
		grep.setUri(uri);
		grep.setUser(user);
		grep.setPw(pw);
		
		if (switches.isTrace()) {
			StringBuilder sb = new StringBuilder();
			for (String p : fileList) {
				sb.append(" ").append(p);
			}
			getDisplay().trace("Search pattern '" + pattern + "'");
			getDisplay().trace("Resource path(s) to search '" + sb.toString() + "'");
		}
	}
	
	public Display getDisplay() {
		if (grep == null) {
			return null;
		}
		return grep.getDisplay();
	}

	public void setDisplay(Display display) {
		if (grep != null) {
			grep.setDisplay(display);
		}
	}

    private void usage(String msg) {
		HelpFormatter formatter = new HelpFormatter();
        String header = "crgrep: Common Resource Grep.";
		if (msg != null) {
		    header += "\n  failed with: " + msg;
		}
		String syntax = "crgrep [options] <search-pattern> [<resources-pattern>]";
		String footer = "If <resources-pattern> is not specified, or is '-', read from stdin\n" 
		        + "Please report issues at https://bitbucket.org/cryanfuse/crgrep/issues";
		formatter.printHelp(syntax, header, options, footer, false);
	}
    
    private void printVersion() {
        StringBuilder sb = new StringBuilder("CRGREP ");
        sb.append(VERSION).append("\n").append("\n");
        sb.append("(C) Copyright 2013-2014 Craig Ryan. All rights reserved.").append("\n");
        sb.append("License GNU Lesser General Public License (LGPL) version 2.1 <http://www.gnu.org/licenses/lgpl-2.1.html>").append("\n");
        sb.append("This program is free software; you can redistribute or change it.").append("\n");
        sb.append("There is NO WARRANTY, to the extent permitted by law.").append("\n").append("\n");
        System.out.println(sb.toString());
    }

    protected void exitWithStatus(int status) {
        System.exit(status);
    }

	public Switches getSwitches() {
		return switches;
	}
}
