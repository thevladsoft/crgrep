package com.ryanfuse.crgrep;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.googlecode.junittoolbox.SuiteClasses;
import com.googlecode.junittoolbox.WildcardPatternSuite;

/**
 * Run all unit tests, ignoring any tests under 'integration' package.
 * 
 * Used with (jacoco) Coverage As -> Junit coverage reporting.
 */
@RunWith(WildcardPatternSuite.class)
@SuiteClasses({"**/*Test.class", "!**/integration/*.class"})
public class AllUnitTestsTestSuite {
}
