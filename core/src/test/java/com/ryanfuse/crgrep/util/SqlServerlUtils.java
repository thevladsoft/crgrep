/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * A java application to re-create a SQLServer test database on localhost named 'mydb' 
 * and required test tables, user, grants and seeding data.
 * 
 * Assumes SQLServer is running and root login is "sa" / "test". If need
 * be, change DB_ROOT_USER and DB_ROOT_PW accordingly.
 *
 * @author 'Craig Ryan'
 */
public class SqlServerlUtils {

    private static final boolean JTDS_DRIVER = true;
    
    private static final String DB_ROOT_URL = JTDS_DRIVER ? "jdbc:jtds:sqlserver://localhost:1433/master" : "jdbc:sqlserver://localhost:1433;database=master";
    private static final String DB_ROOT_URL_MYDB = JTDS_DRIVER ? "jdbc:jtds:sqlserver://localhost:1433/mydb" : "jdbc:sqlserver://localhost:1433;database=mydb";
    private static final String DB_ROOT_USER = "sa";
    private static final String DB_ROOT_PW = "test";
    private static final String DB_URL = JTDS_DRIVER ? "jdbc:jtds:sqlserver://localhost:1433" : "jdbc:sqlserver://localhost:1433";
    private static final String DB_URL_MYDB = JTDS_DRIVER ? "jdbc:jtds:sqlserver://localhost:1433/mydb" : "jdbc:sqlserver://localhost:1433;database=mydb";
	private static final String DB_USER = "mydb";
	private static final String DB_PW = "password";

    private static final String DB_SCHEMA = "mydb";
//  private static final String DB_SCHEMA = null;  // uses 'dbo' default schema
    
	public static void main(String[] args) {
		// re-create the entire test db
		try {
			dropTestData();
            dropSchema();
            dropUser();
            dropLogin();
			dropTestDatabase();
			createTestDatabase();
            createLogin();
            createUser();
            createSchema();
			createGrants();
            createTestTables();
            insertTestData();
		} catch (SQLException e) {
			System.out.println("Failed to re-create db, msg: " + e.getLocalizedMessage());
		}
		System.out.println("Done.");
	}

	private static void dropTestData() {
	    System.out.println("Dropping test data:");
	    Connection c = null;
	    try {
	        c = connection(false, false);
	        Statement stmt = c.createStatement();
	        String sql = "DELETE from ALL_BINARY_COLUMNS";
	        stmt.executeUpdate(sql);
	        sql = "DROP TABLE ALL_BINARY_COLUMNS";
	        stmt.executeUpdate(sql);
	        sql = "DELETE from CAUSES";
	        stmt.executeUpdate(sql);
	        sql = "DROP TABLE CAUSES";
	        stmt.executeUpdate(sql);
	        sql = "DELETE from CATALOG";
	        stmt.executeUpdate(sql);
	        sql = "DROP TABLE CATALOG";
	        stmt.executeUpdate(sql);
	        sql = "DELETE from HISTORY";
	        stmt.executeUpdate(sql);
	        sql = "DROP TABLE HISTORY";
	        stmt.executeUpdate(sql);
	        System.out.println("Dropped seed data plus tables successfully...");
	        c.close();
	    } catch (SQLException e) {
	        System.out.println("(warn) drop test data failed: " + e.getLocalizedMessage());
	    }
	}
	
    private static void dropLogin() {
        System.out.println("Dropping login:");
        Connection c = null;
        try {
            c = connection(true, true);
            Statement stmt = c.createStatement();
            String sql = "DROP LOGIN mydb";
            System.out.println("Drop login:");
            stmt.executeUpdate(sql);
            System.out.println("Login dropped successfully...");
        } catch (SQLException e) {
            System.out.println("(warn) drop login failed: " + e.getLocalizedMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void dropUser() {
        System.out.println("Dropping user:");
        Connection c = null;
        Statement stmt = null;
        try {
            c = connection(true, false);
            stmt = c.createStatement();
            String sql = "DROP USER mydb";
            System.out.println("Drop user:");
            stmt.executeUpdate(sql);
            c.close();
            System.out.println("User dropped successfully...");
        } catch (SQLException e) {
            System.out.println("(warn) drop user failed: " + e.getLocalizedMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void dropSchema() {
        System.out.println("Dropping schema:");
        if (DB_SCHEMA == null) {
            System.out.println("No schema specified.");
            return;
        }
        Connection c = null;
        try {
            c = connection(true, false);
            Statement stmt = c.createStatement();
            String sql = "DROP SCHEMA " + DB_SCHEMA;
            stmt.executeUpdate(sql);
            System.out.println("Database schema dropped successfully...");
        } catch (SQLException e) {
            System.out.println("(warn) drop schema failed: " + e.getLocalizedMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    private static void dropTestDatabase() {
        System.out.println("Dropping test database:");
        Connection c = null;
        try {
            // if this fails with 'in use' run 'sp_who' in dbvis to see dbname 'mydb' and any
            // processes attached to it. Use 'spid' to run 'kill <spdi>'
            c = connection(true, true);
            Statement stmt = c.createStatement();
            String sql = "DROP DATABASE mydb";
            stmt.executeUpdate(sql);
            System.out.println("Database dropped successfully...");
        } catch (SQLException e) {
            System.out.println("(warn) drop database failed: " + e.getLocalizedMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	
    private static void createLogin() throws SQLException {
        System.out.println("Creating login:");
        Connection c = null;
        // To see users:
        // SELECT * FROM sys.database_principals
        // To see logins:
        // SELECT * FROM sys.server_principals
        try {
            c = connection(true, true);
            Statement stmt = c.createStatement();
            String sql = "CREATE LOGIN mydb WITH PASSWORD = 'password'"; //, DEFAULT_DATABASE = mydb";
            System.out.println("Create login:");
            stmt.executeUpdate(sql);
            System.out.println("Created login successfully...");
        } catch (SQLException e) {
            System.out.println("(warn) create login failed: " + e.getLocalizedMessage());
            throw e;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void createUser() throws SQLException {
        System.out.println("Creating user:");
        Connection c = null;
        // To see users:
        // SELECT * FROM sys.database_principals
        // To see logins:
        // SELECT * FROM sys.server_principals
        try {
            c = connection(true, false);
            Statement stmt = c.createStatement();
            String sql = "CREATE USER mydb FOR LOGIN mydb";
            if (DB_SCHEMA != null) {
                sql += " WITH DEFAULT_SCHEMA = " + DB_SCHEMA;
            }
            System.out.println("Create user:");
            stmt.executeUpdate(sql);
            System.out.println("Created user successfully...");
        } catch (SQLException e) {
            System.out.println("(warn) create user failed: " + e.getLocalizedMessage());
            throw e;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

	private static void createGrants() throws SQLException {
	    System.out.println("Creating grants in database:");
	    Connection c = null;
	    try {
	        c = connection(true, false);
	        Statement stmt = c.createStatement();
	        String sql = "GRANT ALTER,DELETE,INSERT,SELECT,UPDATE ON SCHEMA :: ";
	        if (DB_SCHEMA != null) {
	            sql += DB_SCHEMA;
	        } else {
	            sql += " dbo ";
	        }
	        sql += " TO mydb";
	        System.out.println("Grant schema permissions for user:");
	        stmt.executeUpdate(sql);
	        System.out.println("Grant db permissions for user:");
	        sql = "GRANT CREATE TABLE to mydb";
	        stmt.executeUpdate(sql);
	        System.out.println("Grants created successfully...");
	    } catch (SQLException e) {
	        System.out.println("(warn) create grants failed: " + e.getLocalizedMessage());
	    } finally {
	        if (c != null) {
	            try {
	                c.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}


	private static void createSchema() throws SQLException {
	    System.out.println("Creating schema:");
	    if (DB_SCHEMA == null) {
	        System.out.println("No schema specified, default to 'dbo'");
	        return;
	    }
	    Connection c = null;
	    try {
	        c = connection(true, false);
	        Statement stmt = c.createStatement();
	        String sql = "CREATE SCHEMA " + DB_SCHEMA + " AUTHORIZATION mydb";
	        stmt.executeUpdate(sql);
	        stmt.close();
	        System.out.println("Database schema created successfully...");
	    } catch (SQLException e) {
	        System.out.println("(warn) create schema failed: " + e.getLocalizedMessage());
	        throw e;
	    } finally {
	        if (c != null) {
	            try {
	                c.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
	
	private static void createTestDatabase() throws SQLException {
		System.out.println("Creating test database:");
        Connection c = null;
        try {
            c = connection(true, true);
            Statement stmt = c.createStatement();
            String sql = "CREATE DATABASE mydb";
            stmt.executeUpdate(sql);
            System.out.println("Database created successfully...");
        } catch (SQLException e) {
            System.out.println("(warn) drop database failed: " + e.getLocalizedMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}



	/* 
	 * Database consists of these tables [: columns]
	 * 		ALL_BINARY_COLUMNS: NUM1 (BINARY), NUM2 (BINARY)
	 *      -> no columns types of interest
	 *              1, 2
	 *              12345, 9876
	 * 		CAUSES: ID (INT), NAME (VC50), CAUSE_STUFF (VC20)
	 *      -> table/col names and data both match
	 *              0,cause 1,the cause is mine          
	 *              1,cause 2,also mine          
	 *              2,no match,nada
	 *      HISTORY: EVENT (VC100), author (VC20)
	 *      -> table/col names don't match data
	 *      -> match in one column of a row with diff column in another row and vice versa eg 'ted' and 'cra'
	 *              completed all outstanding tasks!, craig
	 *              crafting new ideas, ted
	 *              More Crazy ideas, Stan
	 *      CATALOG: NAME (VC30), CHUNK (BINARY)
	 *      -> same column name as CAUSES table
	 *              
	 */
    private static void createTestTables() throws SQLException {
        System.out.println("Creating test tables:");
        Connection c = connection(false, false);
        Statement stmt = c.createStatement();
        // create tables
        String sql = "CREATE TABLE ALL_BINARY_COLUMNS (NUM1 binary(8), NUM2 binary(8))";
        stmt.executeUpdate(sql);
        sql = "CREATE TABLE CAUSES (ID integer, NAME VARCHAR(50), CAUSE_STUFF VARCHAR(20))";
        stmt.executeUpdate(sql);
        sql = "CREATE TABLE HISTORY (EVENT VARCHAR(100), author VARCHAR(20))"; // mixed case col names
        stmt.executeUpdate(sql);
        sql = "CREATE TABLE CATALOG (NAME VARCHAR(30), CHUNK binary(8))";
        stmt.executeUpdate(sql);
        System.out.println("Database tables created successfully...");
        c.close();
    }

    private static void insertTestData() throws SQLException {
        System.out.println("Creating test data:");
        Connection c = connection(false, false);
        Statement stmt = c.createStatement();
        // seed tables
        String sql = "INSERT INTO ALL_BINARY_COLUMNS (NUM1, NUM2) VALUES (CONVERT(binary(8), '0x4E616D65', 1), CONVERT(binary(8), '0xAABB', 1))";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO ALL_BINARY_COLUMNS (NUM1, NUM2) VALUES (CONVERT(binary(8), '0xDEADFACE', 1), CONVERT(binary(8), '0xAABB125E', 1))";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO CAUSES(ID, NAME, CAUSE_STUFF) VALUES (0, 'cause 1', 'the cause is mine')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO CAUSES(ID, NAME, CAUSE_STUFF) VALUES (1, 'cause 2', 'also mine')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO CAUSES(ID, NAME, CAUSE_STUFF) VALUES (2, 'no match', 'nada')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO HISTORY(EVENT, author) VALUES ('completed all outstanding tasks!', 'craig')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO HISTORY(EVENT, author) VALUES ('crafting new ideas', 'ted')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO HISTORY(EVENT, author) VALUES ('More Crazy ideas', 'Stan')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO CATALOG(NAME) VALUES ('cat')";
        stmt.executeUpdate(sql);
        sql = "INSERT INTO CATALOG(NAME, CHUNK) VALUES ('dan', CONVERT(binary(8), '0xDEADFACE', 1))";
        stmt.executeUpdate(sql);

        System.out.println("Database seed data created successfully...");
        c.close();
    }

	private static Connection connection(boolean asRoot, boolean toMaster) throws SQLException {
		try {
			Driver dbDriver = (Driver)Class.forName (
			        JTDS_DRIVER ? "net.sourceforge.jtds.jdbc.Driver" : "com.microsoft.sqlserver.jdbc.SQLServerDriver"
			        ).newInstance();
			if (asRoot) {
				return DriverManager.getConnection(
				        toMaster ? DB_ROOT_URL : DB_ROOT_URL_MYDB, 
				        DB_ROOT_USER, DB_ROOT_PW);
			}
			return DriverManager.getConnection(
			        toMaster ? DB_URL : DB_URL_MYDB, 
			        DB_USER, DB_PW);
		} catch (InstantiationException e) {
			System.out.println("Failed SQLServer connection, inst ex msg: " + e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			System.out.println("Failed SQLServer connection, illegal access msg: " + e.getLocalizedMessage());
		} catch (ClassNotFoundException e) {
			System.out.println("Failed SQLServer connection, class not found. Add to path, driver: " + e.getLocalizedMessage());
		} catch (SQLException e) {
			System.out.println("Failed SQLServer connection, sql ex msg: " + e.getLocalizedMessage());
		}
		throw new SQLException("No connection (" + (asRoot?"as root":"as normal user")+")");
	}
}
