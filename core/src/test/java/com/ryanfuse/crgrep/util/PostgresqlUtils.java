/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * A java application to re-create a PostgreSQL test database on localhost named 'mydb' 
 * and required test tables, user, grants and seeding data within the schema 'mydb'
 * 
 * Assumes postgresql server is running and root login is "postgres" / "password". If need
 * be, change DB_ROOT_USER and DB_ROOT_PW accordingly.
 *
 * @author 'Craig Ryan'
 */
public class PostgresqlUtils {

	private static final String DB_ROOT_URL = "jdbc:postgresql://localhost:5432/";
    private static final String DB_ROOT_USER = "postgres";
    private static final String DB_ROOT_PW = "password";
	private static final String DB_URL = "jdbc:postgresql://localhost:5432/mydb";
	private static final String DB_USER = "mydb";
	private static final String DB_PW = "password";
    
    private static final String DB_SCHEMA = "mydb";
//    private static final String DB_SCHEMA = null;  // uses 'public' default schema

	public static void main(String[] args) {
		// re-create the entire test db
		try {
			dropTestData();
            dropSchema();
			dropTestDatabase();
            dropUser();
            createUser();
			createTestDatabase();
            createGrants();
			createSchema();
			createTestData();
		} catch (SQLException e) {
			System.out.println("Failed to re-create db, msg: " + e.getLocalizedMessage());
		}
		System.out.println("Done.");
	}

	private static void createGrants() throws SQLException {
		System.out.println("Creating grants in database:");
		Connection c = null;
		try {
		    c = connection(true);
		    Statement stmt = c.createStatement();
		    String sql = "GRANT ALL PRIVILEGES ON DATABASE mydb TO mydb";
		    stmt.executeUpdate(sql);
		    System.out.println("Database grants created successfully...");
        } catch (SQLException e) {
            System.out.println("(warn) create grants failed: " + e.getLocalizedMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	private static void dropUser() throws SQLException {
	    System.out.println("Dropping user:");
	    Connection c = null;
	    try {
	        c= connection(true);
	        Statement stmt = c.createStatement();
	        String sql = "DROP USER mydb";
	        stmt.executeUpdate(sql);
	        System.out.println("User dropped successfully...");
	    } catch (SQLException e) {
	        System.out.println("(warn) drop user failed: " + e.getLocalizedMessage());
	    } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}
	
    private static void createUser() {
        System.out.println("Creating user:");
        Connection c = null;
        try {
            c = connection(true);
            Statement stmt = c.createStatement();
            String sql = "CREATE USER mydb WITH PASSWORD 'password'";
            stmt.executeUpdate(sql);
            System.out.println("Database user created successfully...");
        } catch (SQLException e) {
            System.out.println("(warn) create user failed: " + e.getLocalizedMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

	private static void dropTestDatabase() {
		System.out.println("Dropping test database:");
        Connection c = null;
		try {
		    c = connection(true);
			Statement stmt = c.createStatement();
			String sql = "DROP DATABASE mydb";
			stmt.executeUpdate(sql);
			System.out.println("Database dropped successfully...");
		} catch (SQLException e) {
			System.out.println("(warn) drop database failed: " + e.getLocalizedMessage());
		} finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

    private static void createSchema() {
        System.out.println("Creating schema:");
         if (DB_SCHEMA == null) {
             System.out.println("No schema specified, default to 'public'");
             return;
         }
         Connection c = null;
         try {
             c = connection(false);
             Statement stmt = c.createStatement();
             String sql = "CREATE SCHEMA " + DB_SCHEMA;
             stmt.executeUpdate(sql);
             stmt.close();
             System.out.println("Database schema created successfully...");
         } catch (SQLException e) {
             System.out.println("(warn) create schema failed: " + e.getLocalizedMessage());
         } finally {
             if (c != null) {
                 try {
                     c.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
             }
         }
     }

    private static void dropSchema() {
        System.out.println("Dropping schema:");
         if (DB_SCHEMA == null) {
             System.out.println("No schema specified.");
             return;
         }
         Connection c = null;
         try {
             c = connection(false);
             Statement stmt = c.createStatement();
             String sql = "DROP SCHEMA " + DB_SCHEMA;
             stmt.executeUpdate(sql);
             System.out.println("Database schema dropped successfully...");
         } catch (SQLException e) {
             System.out.println("(warn) drop schema failed: " + e.getLocalizedMessage());
         } finally {
             if (c != null) {
                 try {
                     c.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
             }
         }
     }

	private static void createTestDatabase() throws SQLException {
		System.out.println("Creating test database:");
        Connection c = null;
        try {
            c = connection(true);
            Statement stmt = c.createStatement();
            String sql = "CREATE DATABASE mydb";
            stmt.executeUpdate(sql);
            System.out.println("Database created successfully...");
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	private static void dropTestData() {
		System.out.println("Dropping test data:");
		Connection c = null;
		try {
			c = connection(false);
			Statement stmt = c.createStatement();
			String sql = "DELETE from " + fqn("ALL_BINARY_COLUMNS");
			stmt.executeUpdate(sql);
			sql = "DROP TABLE " + fqn("ALL_BINARY_COLUMNS");
			stmt.executeUpdate(sql);
            sql = "DELETE from " + fqn("CAUSES");
			stmt.executeUpdate(sql);
			sql = "DROP TABLE " + fqn("CAUSES");
            stmt.executeUpdate(sql);
            sql = "DELETE from " + fqn("CATALOG");
            stmt.executeUpdate(sql);
            sql = "DROP TABLE " + fqn("CATALOG");
            stmt.executeUpdate(sql);
            sql = "DELETE from " + fqn("HISTORY");
            stmt.executeUpdate(sql);
            sql = "DROP TABLE " + fqn("HISTORY");
            stmt.executeUpdate(sql);
		
			System.out.println("Dropped seed data plus tables successfully...");
		} catch (SQLException e) {
			System.out.println("(warn) drop tables failed: " + e.getLocalizedMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

    /* 
	 * Database consists of two tables [: columns]
	 * 		ALL_BINARY_COLUMNS: NUM1 (BYTEA), NUM2 (BYTEA)
	 *      -> no columns types of interest
	 *              1, 2
	 *              12345, 9876
	 * 		CAUSES: ID (INT), NAME (VC50), CAUSE_STUFF (VC20)
	 *      -> table/col names and data both match
	 *              0,cause 1,the cause is mine          
	 *              1,cause 2,also mine          
	 *              2,no match,nada
	 *      HISTORY: EVENT (VC100), author (VC20)
	 *      -> table/col names don't match data
	 *      -> match in one column of a row with diff column in another row and vice versa eg 'ted' and 'cra'
	 *              completed all outstanding tasks!, craig
	 *              crafting new ideas, ted
	 *              More Crazy ideas, Stan
	 *      CATALOG: NAME (VC30), CHUNK (BYTEA)
	 *      -> same column name as CAUSES table
	 *              
	 */
	private static void createTestData() throws SQLException {
		System.out.println("Creating test data:");
		Connection c = null;
		try {
		    c = connection(false);
		    Statement stmt = c.createStatement();
		    // create tables
		    String sql = "CREATE TABLE " + fqn("ALL_BINARY_COLUMNS") + "(NUM1 BYTEA, NUM2 BYTEA)";
		    stmt.executeUpdate(sql);
		    sql = "CREATE TABLE " + fqn("CAUSES") + "(ID integer, NAME VARCHAR(50), CAUSE_STUFF VARCHAR(20))";
		    stmt.executeUpdate(sql);
		    sql = "CREATE TABLE " + fqn("HISTORY") + "(EVENT VARCHAR(100), author VARCHAR(20))"; // mixed case col names
		    stmt.executeUpdate(sql);
		    sql = "CREATE TABLE " + fqn("CATALOG") + "(NAME VARCHAR(30), CHUNK BYTEA)";
		    stmt.executeUpdate(sql);
		    System.out.println("Database tables created successfully...");

		    // seed tables
		    sql = "INSERT INTO " + fqn("ALL_BINARY_COLUMNS") + "(NUM1, NUM2) VALUES ('1', '2')";
		    stmt.executeUpdate(sql);
		    sql = "INSERT INTO " + fqn("ALL_BINARY_COLUMNS") + "(NUM1, NUM2) VALUES ('12345', '9876')";
		    stmt.executeUpdate(sql);
		    sql = "INSERT INTO " + fqn("CAUSES") + "(ID, NAME, CAUSE_STUFF) VALUES (0, 'cause 1', 'the cause is mine')";
		    stmt.executeUpdate(sql);
		    sql = "INSERT INTO " + fqn("CAUSES") + "(ID, NAME, CAUSE_STUFF) VALUES (1, 'cause 2', 'also mine')";
		    stmt.executeUpdate(sql);
		    sql = "INSERT INTO " + fqn("CAUSES") + "(ID, NAME, CAUSE_STUFF) VALUES (2, 'no match', 'nada')";
		    stmt.executeUpdate(sql);
		    sql = "INSERT INTO " + fqn("HISTORY") + "(EVENT, author) VALUES ('completed all outstanding tasks!', 'craig')";
		    stmt.executeUpdate(sql);
		    sql = "INSERT INTO " + fqn("HISTORY") + "(EVENT, author) VALUES ('crafting new ideas', 'ted')";
		    stmt.executeUpdate(sql);
		    sql = "INSERT INTO " + fqn("HISTORY") + "(EVENT, author) VALUES ('More Crazy ideas', 'Stan')";
		    stmt.executeUpdate(sql);
		    sql = "INSERT INTO " + fqn("CATALOG") + "(NAME) VALUES ('cat')";
		    stmt.executeUpdate(sql);
		    sql = "INSERT INTO " + fqn("CATALOG") + "(NAME, CHUNK) VALUES ('dan', 'a chunk of data')";
		    stmt.executeUpdate(sql);

		    System.out.println("Database seed data created successfully...");
		} finally {
		    if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
		}
	}

	private static String fqn(String table) {
	    if (DB_SCHEMA == null) {
	        return table;
	    }
	    return DB_SCHEMA + "." + table;
	}


	private static Connection connection(boolean asRoot) throws SQLException {
		try {
			Driver dbDriver = (Driver)Class.forName ("org.postgresql.Driver").newInstance();
			if (asRoot) {
				return DriverManager.getConnection(DB_ROOT_URL, DB_ROOT_USER, DB_ROOT_PW);
			}
			return DriverManager.getConnection(
			        //DB_SCHEMA == null ? DB_URL : DB_URL + "?searchpath=" + DB_SCHEMA,
			        DB_URL,
			        DB_USER, DB_PW);
		} catch (InstantiationException e) {
			System.out.println("Failed Postgresql connection, msg: " + e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			System.out.println("Failed Postgresql connection, msg: " + e.getLocalizedMessage());
		} catch (ClassNotFoundException e) {
			System.out.println("Failed Postgresql connection, msg: " + e.getLocalizedMessage());
		} catch (SQLException e) {
			System.out.println("Failed Postgresql connection, msg: " + e.getLocalizedMessage());
		}
		throw new SQLException("No connection");
	}
}
