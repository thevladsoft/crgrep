/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import static org.junit.Assert.*;

import org.junit.Test;

public class ResultTextTest {

	private static final String DDD = "...";
	private ResourcePattern patStartWild = new ResourcePattern("*PATTERN");
	private ResourcePattern patEndWild = new ResourcePattern("PATTERN*");
	private ResourcePattern patFixed = new ResourcePattern("PATTERN");

	private String t1 = "x";
	private String t2 = "xy";
	private String t3 = "xyz";
	private String t4 = "xyza";
	private String tshort = "a small result";
	
	private String tpatStart = "PATTERN which starts our string";
	private String tpatEnd = "a result which matches a PATTERN";
	private String tpatMid = "a result which matches a PATTERN in the middle of it";
	
	private String tlong = "xy string with lots of stuff in it which will match more than the expected results" + 
			"will ever be and we want to trim this text to something smaller to display on one line";
	private String tmultilines = "xy string\nwith lots of stuff\r\nin it which will\rmatch more than the expected results\n"; 
	private String tmultilines_stripped = "xy string\\nwith lots of stuff\\nin it which will\\nmatch more than the expected results\\n"; 

	@Test
	public void testResultStringDefaults() {
		ResultText rt = new ResultText();
		assertEquals(ResultText.DEFAULT_MIN_DATA_VALUE_PREFIX, rt.getMinPrefixIndex());
		assertEquals(ResultText.DEFAULT_MAX_DATA_VALUE_LENGTH, rt.getMaxResultLength());
		assertNull(rt.getResourcePattern());
		assertFalse(rt.isRemoveLineBreaks());
		rt.setRemoveLineBreaks(true);
		assertTrue(rt.isRemoveLineBreaks());
	}

	@Test
	public void testResultStringMinMax() {
		// 0, 0 - max will be set to unlimited
		ResultText rt = new ResultText(0, 0);
		String r = rt.result("");
		assertEquals("", r);
		r = rt.result(t1);
		assertEquals(t1, r);
		r = rt.result(t2);
		assertEquals(t2, r);
		r = rt.result(t3);
		assertEquals(t3, r);
		r = rt.result(tshort);
		assertEquals(tshort, r);
		r = rt.result(tlong);
		assertEquals(tlong, r);
		
		// 0, 1 - both used as-is
		rt = new ResultText(0, 1);
		r = rt.result("");
		assertEquals("", r);
		r = rt.result(t1);
		assertEquals(t1, r);
		r = rt.result(t2);
		assertEquals(t1+DDD, r);
		r = rt.result(t3);
		assertEquals(t1+DDD, r);
		r = rt.result(tshort);
		assertEquals(tshort.substring(0, 1)+DDD, r);
		r = rt.result(tlong);
		assertEquals(tlong.substring(0, 1)+DDD, r);
		
		// 1, 1 max will be set to 1
		rt = new ResultText(1, 1);
		r = rt.result("");
		assertEquals("", r);
		r = rt.result(t1);
		assertEquals(t1, r);
		r = rt.result(t2);
		assertEquals(t1+DDD, r);
		r = rt.result(t3);
		assertEquals(t1+DDD, r);
		r = rt.result(tshort);
		assertEquals(tshort.substring(0, 1)+DDD, r);
		r = rt.result(tlong);
		assertEquals(tlong.substring(0, 1)+DDD, r);
		
		// 1, -1 max will be set to unlimited
		rt = new ResultText(1, -1);
		r = rt.result("");
		assertEquals("", r);
		r = rt.result(t1);
		assertEquals(t1, r);
		r = rt.result(t2);
		assertEquals(t2, r);
		r = rt.result(t3);
		assertEquals(t3, r);
		r = rt.result(tshort);
		assertEquals(tshort, r);
		r = rt.result(tlong);
		assertEquals(tlong, r);
		
		// 1, -1 min will be set to 0, max will be set to unlimited
		rt = new ResultText(-1, -1);
		r = rt.result("");
		assertEquals("", r);
		r = rt.result(t1);
		assertEquals(t1, r);
		r = rt.result(t2);
		assertEquals(t2, r);
		r = rt.result(t3);
		assertEquals(t3, r);
		r = rt.result(tshort);
		assertEquals(tshort, r);
		r = rt.result(tlong);
		assertEquals(tlong, r);
		
		// 5, 10 both used as-is.
		rt = new ResultText(5, 10);
		r = rt.result(tshort);
		assertEquals(tshort.substring(0, 10)+DDD, r);
		r = rt.result(tlong);
		assertEquals(tlong.substring(0, 10)+DDD, r);
	}
	
	@Test
	public void testResultPattern() {
		ResourcePattern rp = new ResourcePattern("PATTERN");
		
		// 0, max < 1 means unlimited
		ResultText rt = new ResultText(0, 0);
		rt.setResourcePattern(rp);
		
		String r = rt.result(t1);
		assertNotNull(r);
		assertEquals(t1, r);

		r = rt.result(tpatStart);
		assertNotNull(r);
		assertEquals(tpatStart, r);
		r = rt.result(tpatMid);
		assertNotNull(r);
		assertEquals("...PATTERN in the middle of it", r);
		r = rt.result(tpatEnd);
		assertNotNull(r);
		assertEquals("...PATTERN", r);

		// 0, max 1
		rt = new ResultText(0, 1, rp);
		r = rt.result(t1);
		assertNotNull(r);
		assertEquals(t1, r);
		r = rt.result(tpatStart);
		assertNotNull(r);
		assertEquals("P...", r);
		r = rt.result(tpatMid);
		assertNotNull(r);
		assertEquals("...P...", r);
		r = rt.result(tpatEnd);
		assertNotNull(r);
		assertEquals("...P...", r);

		// 0, max >1
		rt = new ResultText(0, 10, rp);
		r = rt.result(t1);
		assertNotNull(r);
		assertEquals(t1, r);
		r = rt.result(tpatStart);
		assertNotNull(r);
		assertEquals("PATTERN wh...", r);
		r = rt.result(tpatMid);
		assertNotNull(r);
		assertEquals("...PATTERN in...", r);
		r = rt.result(tpatEnd);
		assertNotNull(r);
		assertEquals("...PATTERN", r);

		// 5 to max >1
		rt = new ResultText(5, 20, rp);
		r = rt.result(t1);
		assertNotNull(r);
		assertEquals(t1, r);
		r = rt.result(tpatStart);
		assertNotNull(r);
		assertEquals("PATTERN which starts...", r);
		r = rt.result(tpatMid);
		assertNotNull(r);
		assertEquals("...es a PATTERN in the ...", r);
		r = rt.result(tpatEnd);
		assertNotNull(r);
		assertEquals("...es a PATTERN", r);
	}

	@Test
	public void testResultLong() {
		ResourcePattern rp = new ResourcePattern("stuff");
		
		// 10, unlimited
		ResultText rt = new ResultText(10, -1, rp);
		
		String r = rt.result(tlong);
		assertNotNull(r);
		assertEquals(DDD+tlong.substring(13), r);
		
		rt = new ResultText(10, 10, rp);
		r = rt.result(tlong);
		assertNotNull(r);
		assertEquals(DDD+tlong.substring(13, 23)+DDD, r);
	}

	@Test
	public void testNewLineFiltering() {
		ResourcePattern rp = new ResourcePattern("xy");
		// 
		ResultText rt = new ResultText(0, -1, rp);
		rt.setRemoveLineBreaks(true);
		String r = rt.result(tmultilines);
		assertNotNull(r);
		assertEquals(tmultilines_stripped, r);

		rt.setRemoveLineBreaks(false);
		r = rt.result(tmultilines);
		assertNotNull(r);
		assertEquals(tmultilines, r);
	}
}
