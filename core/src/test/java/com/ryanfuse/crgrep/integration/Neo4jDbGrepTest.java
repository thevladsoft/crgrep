/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.db.NeoGraphParser;
import com.ryanfuse.crgrep.util.DisplayRecorder;

/**
 * Integration test for Neo4J graph database tests.
 * 
 * To run, first start Neo4j Community console supplied with Neo4J, for
 * Database location click Browse and choose the database in the path
 * core/src/test/databases/neo4j[_64] depending on which arch you are
 * on and then Start the database. The test can then be run as a junit test.
 * 
 * @author Craig Ryan
 */
public class Neo4jDbGrepTest extends TestCommon {

    private static final String DB_USER = "sa";
    private static final String DB_PW = "password";
    private static final String DB_URL = "http://localhost:7474/db/data";
    
    @Test
    public void testNeo4jDefaults() throws GrepException {
        NeoGraphParser hp = new NeoGraphParser("http://localhost/stuff", "user", "pw");
        assertNull(hp.getDefaultPassword());
        assertNull(hp.getDefaultUser());
        assertEquals("user", hp.getUser());
        assertEquals("pw", hp.getPassword());
        assertEquals("http://localhost/stuff", hp.getUri());
    }
    
    @Test
    public void testSimpleURI() throws GrepException {
        NeoGraphParser hp = new NeoGraphParser("http://localhost:7474", null, null);
        assertTrue("simple URI", hp.openConnection());
        assertEquals(DB_URL, hp.getUri());
        hp.cleanup();
        hp = new NeoGraphParser("http://localhost:7474/", null, null);
        assertTrue("simple URI with slash", hp.openConnection());
        assertEquals(DB_URL, hp.getUri());
        hp.cleanup();
    }

    @Test
    public void testPatternUpperExact_TabWildColWild_ByName_Case() throws GrepException {
        title("Neo4j", "CAUSE", "*");
        List<String> expected = Arrays.asList(
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CAUSES {cause_stuff}"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "CAUSE", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternMixedExact_TabWildColWild_ByName_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "StufF", "*");
        List<String> expected = Arrays.asList(
                //"Node[nn]:CAUSES {ID:yy,NAME:\"\",CAUSE_STUFF:\"\"}"
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CAUSES {cause_stuff}"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "StufF", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildColWild_NameData_Case() throws GrepException {
        title("Neo4j", "cause", "*");
        List<String> expected = Arrays.asList(
//                "Node[nn]:CAUSES {ID:yy,NAME:\"\",CAUSE_STUFF:\"\"}",
//                "CAUSES: 0,cause 1,the cause is mine",
//                "CAUSES: 1,cause 2,also mine"
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\",name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {cause_stuff,name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {cause_stuff}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildColWild_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "cause", "*");
        List<String> expected = Arrays.asList(
//                "Node[nn]:CAUSES {ID:yy,NAME:\"\",CAUSE_STUFF:\"\"}",
//                "CAUSES: 0,cause 1,the cause is mine",
//                "CAUSES: 1,cause 2,also mine"
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\",name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {cause_stuff,name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {cause_stuff}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildColWild_TwoNameData_Case() throws GrepException {
        title("Neo4j", "ca", "*");
        List<String> expected = Arrays.asList(
//                "CAUSES: [ID,NAME,CAUSE_STUFF]",
//                "CAUSES: 0,cause 1,the cause is mine",
//                "CAUSES: 1,cause 2,also mine",
//                "CATALOG: [NAME]",
//                "CATALOG: cat"
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\",name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {cause_stuff,name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CATALOG {name:\"cat\"}",
                "Node[nnnn]:CATALOG {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildColWild_TwoNameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "cA", "*");
        List<String> expected = Arrays.asList(
//                "CAUSES: [ID,NAME,CAUSE_STUFF]",
//                "CAUSES: 0,cause 1,the cause is mine",
//                "CAUSES: 1,cause 2,also mine",
//                "CATALOG: [NAME]",
//                "CATALOG: cat"
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\",name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {cause_stuff,name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CATALOG {name:\"cat\"}",
                "Node[nnnn]:CATALOG {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cA", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternMixedExact_TabWildColWild_NoMatches_Case() throws GrepException {
        title("Neo4j", "MiNe", "*.*");
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "MiNe", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testPatternMixedExact_TabWildColWild_NoMatches_IgnoreCase() throws GrepException {
        title("Neo4j", "SomePattern", "*.*");
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "SomePattern", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testPatternLowerExact_TabColWild_ByName_Case() throws GrepException {
        title("Neo4j", "CAT", "CATALOG.*");
        List<String> expected = Arrays.asList(
                //"CATALOG: [NAME]"
                "Node[nnnn]:CATALOG {}",
                "Node[nnnn]:CATALOG {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "CAT", "CATALOG.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabColWild_ByName_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "CATA", "CATALOG.*");
        List<String> expected = Arrays.asList(
//                "CATALOG: [NAME]"
                "Node[nnnn]:CATALOG {}",
                "Node[nnnn]:CATALOG {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "CATA", "CATALOG.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabColWild_NameData_Case() throws GrepException {
        title("Neo4j", "cause", "CAUSES.*");
        List<String> expected = Arrays.asList(
//                "CAUSES: [ID,NAME,CAUSE_STUFF]",
//                "CAUSES: 0,cause 1,the cause is mine",
//                "CAUSES: 1,cause 2,also mine"
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\",name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {cause_stuff,name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {cause_stuff}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "CAUSES.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabColWild_NameData_IgnoreCase() throws GrepException {
        title("Neo4j", "cause", "CAUSES.*");
        List<String> expected = Arrays.asList(
//                "CAUSES: [ID,NAME,CAUSE_STUFF]",
//                "CAUSES: 0,cause 1,the cause is mine",
//                "CAUSES: 1,cause 2,also mine"
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\",name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {cause_stuff,name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {cause_stuff}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "CAUSES.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWild_ByName_Case() throws GrepException {
        title("Neo4j", "TORY", "*.AUTHOR");
        List<String> expected = Arrays.asList(
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TORY", "*.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWild_ByName_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "TOrY", "*.author");
        List<String> expected = Arrays.asList(
//                "HISTORY: [AUTHOR]"
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TOrY", "*.author"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWild_NameData_Case() throws GrepException {
        title("Neo4j", "n", "*.EVENT");
        List<String> expected = Arrays.asList(
            "Node[nnnn]:HISTORY {EVENT:\"completed all outstanding tasks!\"}",
            "Node[nnnn]:HISTORY {EVENT:\"crafting new ideas\"}",
            "Node[nnnn]:HISTORY {EVENT}"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "n", "*.EVENT"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternUpperExact_TabWild_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "N", "*.EVENT");
        List<String> expected = Arrays.asList(
//                "HISTORY: [EVENT]",
//                "HISTORY: completed all outstanding tasks!",
//                "HISTORY: crafting new ideas"
                "Node[nnnn]:HISTORY {EVENT:\"completed all outstanding tasks!\"}",
                "Node[nnnn]:HISTORY {EVENT:\"crafting new ideas\"}",
                "Node[nnnn]:HISTORY {EVENT}"
                );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "N", "*.EVENT"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }


    @Test
    public void testPatternLowerExact_TabWild_TwoNameData_Case() throws GrepException {
        title("Neo4j", "ca", "*.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2",
//                "CATALOG: [NAME]",
//                "CATALOG: cat"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}",
                "Node[nnnn]:CATALOG {name:\"cat\"}",
                "Node[nnnn]:CATALOG {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca", "*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWild_TwoNameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "ca", "*.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2",
//                "CATALOG: [NAME]",
//                "CATALOG: cat"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}",
                "Node[nnnn]:CATALOG {name:\"cat\"}",
                "Node[nnnn]:CATALOG {}"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca", "*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }
    

    @Test
    public void testPatternLowerExact_TabCol_ByName_Case() throws GrepException {
        title("Neo4j", "TORY", "HISTORY.AUTHOR");
        List<String> expected = Arrays.asList(
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TORY", "HISTORY.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternUpperExact_TabCol_ByName_Case() throws GrepException {
        title("Neo4j", "R", "HISTORY.AUTHOR");
        List<String> expected = Arrays.asList(
                "Node[nnnn]:HISTORY {AUTHOR}",
                "Node[nnnn]:HISTORY {Author}",
                "Node[nnnn]:HISTORY {author}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "R", "HISTORY.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }
    @Test
    public void testPatternLowerExact_TabCol_ByName_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "TOrY", "HISTORY.author");
        List<String> expected = Arrays.asList(
//                "HISTORY: [AUTHOR]"
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TOrY", "HISTORY.author"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabCol_NameData_Case() throws GrepException {
        title("Neo4j", "cau", "CAUSES.NAME");
        List<String> expected = Arrays.asList(
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau", "CAUSES.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabCol_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "cau", "CAUSES.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau", "CAUSES.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_ByName_Case() throws GrepException {
        title("Neo4j", "C?T", "*.*");
        List<String> expected = Arrays.asList(
//                "CATALOG: [NAME]" // exclude CHUNK?
                "Node[nnnn]:CATALOG {}",
                "Node[nnnn]:CATALOG {}"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "C?T", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_ByName_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "*UFF", "*");
        List<String> expected = Arrays.asList(
                //"CAUSES: [ID,NAME,CAUSE_STUFF]"
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CAUSES {cause_stuff}"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "*UFF", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_NameData_Case() throws GrepException {
        title("Neo4j", "c??s*", "*");
        List<String> expected = Arrays.asList(
//                "CAUSES: [ID,NAME,CAUSE_STUFF]",
//                "CAUSES: 0,cause 1,the cause is mine",
//                "CAUSES: 1,cause 2,also mine"
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\",name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {cause_stuff,name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {cause_stuff}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "c??s*", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "ca?S?", "*.*");
        List<String> expected = Arrays.asList(
//                "CAUSES: [ID,NAME,CAUSE_STUFF]",
//                "CAUSES: 0,cause 1,the cause is mine",
//                "CAUSES: 1,cause 2,also mine"
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\",name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {cause_stuff,name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {cause_stuff}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca?S?", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_ManyNameData_Case() throws GrepException {
        title("Neo4j", "?a?*", "*");
        List<String> expected = Arrays.asList(
//                "ALL_BINARY_COLUMNS: []",
//                "CATALOG: [NAME]",
//                "CATALOG: cat",
//                "CATALOG: dan",               
//                "CAUSES: 0,cause 1,the cause is mine",
//                "CAUSES: 1,cause 2,also mine",
//                "CAUSES: 2,no match,nada",
//                "CAUSES: [ID,NAME,CAUSE_STUFF]",
//                "HISTORY: More Crazy ideas,Stan",
//                "HISTORY: [EVENT,AUTHOR]",
//                "HISTORY: completed all outstanding tasks!,craig",
//                "HISTORY: crafting new ideas,ted"
                "Node[nnnn]:CATALOG {name:\"cat\"}",
                "Node[nnnn]:CATALOG {name:\"dan\"}",
                "Node[nnnn]:CAUSES {cause_stuff,name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {cause_stuff:\"nada\",name:\"no match\"}",
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\",name:\"cause 1\"}",
                "Node[nnnn]:HISTORY {AUTHOR:\"craig\",EVENT:\"completed all outstanding tasks!\"}",
                "Node[nnnn]:HISTORY {EVENT:\"crafting new ideas\"}",
                "Node[nnnn]:HISTORY {author:\"Stan\",EVENT:\"More Crazy ideas\"}",
              //"Node[nnnn]:MIXED {mxboolArr,mxbool,mxdblArr,mxfltArr,mxstrArr,mxcharArr,mxchar,mxlongArr,mxintArr,mxbytearr: \"YSByYW5kb20gc3RyaW5n\",mxbyte}"
                "Node[nnnn]:MIXED {mxboolArr:[,false,],mxdblArr,mxfltArr,mxstrArr,mxcharArr,mxchar,mxlongArr,mxintArr,mxbytearr:\"YSByYW5kb20gc3RyaW5n\"}"

            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "?a?*", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        // this test sorts results differently on Win7 / 8.1! Have to sort results.
        display.setSortedPaths(true);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_TwoNameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "Ca?", "*");
        List<String> expected = Arrays.asList(
//                "CAUSES: [ID,NAME,CAUSE_STUFF]",
//                "CAUSES: 0,cause 1,the cause is mine",
//                "CAUSES: 1,cause 2,also mine",
//                "CATALOG: [NAME]",
//                "CATALOG: cat"
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\",name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {cause_stuff,name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CATALOG {name:\"cat\"}",
                "Node[nnnn]:CATALOG {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug,trace", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "Ca?", "*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_NoMatches_Case() throws GrepException {
        title("Neo4j", "N??E", "*.*");
        List<String> expected = Arrays.asList(
                "Node[nnnn]:CATALOG {name}",
                "Node[nnnn]:CATALOG {name}",
                "Node[nnnn]:CAUSES {name}",
                "Node[nnnn]:CAUSES {name}",
                "Node[nnnn]:CAUSES {name}"
                );
        
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "N??E", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        display.setSortedPaths(true);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_NoMatches_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "S*Not?ed", "*.*");
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "S*Not?ed", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testPattern_TabWild_ByName_Case() throws GrepException {
        title("Neo4j", "?OR*", "*.AUTHOR");
        List<String> expected = Arrays.asList(
//                "HISTORY: [AUTHOR]"
                "Node[nnnn]:HISTORY {AUTHOR}",
                "Node[nnnn]:HISTORY {Author}",
                "Node[nnnn]:HISTORY {author}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "?OR*", "*.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabWild_ByName_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "?Ory", "*.author");
        List<String> expected = Arrays.asList(
//                "HISTORY: [AUTHOR]"
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "?Ory", "*.author"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabWild_NameData_Case() throws GrepException {
        title("Neo4j", "?ing", "*.EVENT");
        List<String> expected = Arrays.asList(
            //"HISTORY: [EVENT]",
            //"HISTORY: completed all outstanding tasks!",
            //"HISTORY: crafting new ideas"
            "Node[nnnn]:HISTORY {EVENT:\"completed all outstanding tasks!\"}",
            "Node[nnnn]:HISTORY {EVENT:\"crafting new ideas\"}"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "?ing", "*.EVENT"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabWild_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "?Ng ", "*.EVENT");
        List<String> expected = Arrays.asList(
                "Node[nnnn]:HISTORY {EVENT:\"completed all outstanding tasks!\"}",
                "Node[nnnn]:HISTORY {EVENT:\"crafting new ideas\"}"
                //"HISTORY: [EVENT]",
                //"HISTORY: completed all outstanding tasks!",
                //"HISTORY: crafting new ideas"
                );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "?Ng", "*.EVENT"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }


    @Test
    public void testPattern_TabWild_TwoNameData_Case() throws GrepException {
        title("Neo4j", "ca?", "*.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2",
//                "CATALOG: [NAME]",
//                "CATALOG: cat"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}",
                "Node[nnnn]:CATALOG {name:\"cat\"}",
                "Node[nnnn]:CATALOG {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca?", "*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabWild_TwoNameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "ca?*", "*.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2",
//                "CATALOG: [NAME]",
//                "CATALOG: cat"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}",
                "Node[nnnn]:CATALOG {name:\"cat\"}",
                "Node[nnnn]:CATALOG {}"
                );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca?*", "*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
     
    @Test
    public void testPattern_TabCol_ByName_Case() throws GrepException {
        title("Neo4j", "TO?Y", "HISTORY.AUTHOR");
        List<String> expected = Arrays.asList(
//                "HISTORY: [AUTHOR]"
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TO?Y", "HISTORY.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabCol_ByName_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "T?rY", "HISTORY.author");
        List<String> expected = Arrays.asList(
//                "HISTORY: [AUTHOR]"
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "T?rY", "HISTORY.author"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabCol_NameData_Case() throws GrepException {
        title("Neo4j", "cau?e", "CAUSES.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau?e", "CAUSES.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabCol_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "cau??", "CAUSES.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau??", "CAUSES.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColPatt_ByName_Case() throws GrepException {
        title("Neo4j", "TO?Y", "HISTORY.AUT??R");
        List<String> expected = Arrays.asList(
//                "HISTORY: [AUTHOR]"
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TO?Y", "HISTORY.AUT??R"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColPatt_ByName_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "T?rY", "HISTORY.au*hor");
        List<String> expected = Arrays.asList(
//                "HISTORY: [AUTHOR]"
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "T?rY", "HISTORY.au*hor"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColPatt_NameData_Case() throws GrepException {
        title("Neo4j", "cau?e", "CAUSES.N?M*");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}" 
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau?e", "CAUSES.N?M*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColPatt_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "cau??", "CAUSES.?AME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau??", "CAUSES.?AME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
    
    @Test
    public void testPattern_TabPattCol_ByName_Case() throws GrepException {
        title("Neo4j", "TO?Y", "H??TORY.AUTHOR");
        List<String> expected = Arrays.asList(
                //"HISTORY: [AUTHOR]"
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TO?Y", "H??TORY.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattCol_ByName_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "T?rY", "HIS*RY.author");
        List<String> expected = Arrays.asList(
//                "HISTORY: [AUTHOR]"
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "T?rY", "HIS*RY.author"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattCol_NameData_Case() throws GrepException {
        title("Neo4j", "cau?e", "?AUSE?.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}" 
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau?e", "?AUSE?.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattCol_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "cau??", "?AUS*S.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau??", "?AUS*S.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }
    
    @Test
    public void testPattern_TabPattColPatt_ByName_Case() throws GrepException {
        title("Neo4j", "TO?Y", "H??TORY.*UT?OR");
        List<String> expected = Arrays.asList(
//                "HISTORY: [AUTHOR]"
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TO?Y", "H??TORY.*UT?OR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattColPatt_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "cau??", "?AUS*S.N*E");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau??", "?AUS*S.N*E"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattColWild_ByName_Case() throws GrepException {
        title("Neo4j", "TO?Y", "H??TORY.*");
        List<String> expected = Arrays.asList(
                //"HISTORY: [EVENT,AUTHOR]"
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}",
                "Node[nnnn]:HISTORY {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TO?Y", "H??TORY.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattColWild_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "cau??", "?AUS*S");
        List<String> expected = Arrays.asList(
//                "CAUSES: [ID,NAME,CAUSE_STUFF]",
//                "CAUSES: 0,cause 1,the cause is mine",
//                "CAUSES: 1,cause 2,also mine"
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\",name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {cause_stuff,name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {cause_stuff}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "-i",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cau??", "?AUS*S"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabColPatt_NameData_Case() throws GrepException {
        title("Neo4j", "cause", "CAUSES.NA*");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}"
        );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "CAUSES.NA*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildCol_NameData_Case() throws GrepException {
        title("Neo4j", "cause", "*.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabWildCol_TwoNameData_Case() throws GrepException {
        title("Neo4j", "ca", "*.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2",
//                "CATALOG: [NAME]",
//                "CATALOG: cat"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}",
                "Node[nnnn]:CATALOG {name:\"cat\"}",
                "Node[nnnn]:CATALOG {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca", "*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabPattCol_NoMatch_Case() throws GrepException {
        title("Neo4j", "cause", "NOTAB*.CAUSE_STUFF");
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "NOTAB*.CAUSE_STUFF"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testPatternLowerExact_TabPattCol_NameData_Case() throws GrepException {
        title("Neo4j", "mine", "CA*.CAUSE_STUFF");
        List<String> expected = Arrays.asList(
//                "CAUSES: [CAUSE_STUFF]",
//                "CAUSES: the cause is mine",
//                "CAUSES: also mine"
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\"}",
                "Node[nnnn]:CAUSES {cause_stuff:\"also mine\"}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "mine", "CA*.CAUSE_STUFF"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabPattCol_TwoNameData_Case() throws GrepException {
        title("Neo4j", "ca", "CA*.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]",
//                "CAUSES: cause 1",
//                "CAUSES: cause 2",
//                "CATALOG: [NAME]",
//                "CATALOG: cat"
                "Node[nnnn]:CAUSES {name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {}",
                "Node[nnnn]:CATALOG {name:\"cat\"}",
                "Node[nnnn]:CATALOG {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "ca", "CA*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabPatt_ByNameOnly_Case() throws GrepException {
        title("Neo4j", "USES", "CA*.NAME");
        List<String> expected = Arrays.asList(
//                "CAUSES: [NAME]"
                "Node[nnnn]:CAUSES {}",
                "Node[nnnn]:CAUSES {}",
                "Node[nnnn]:CAUSES {}"
            );
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "USES", "CA*.NAME"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabNoExistColNoExist_NoMatches_Case() throws GrepException {
        title("Neo4j", "cause", "NOTAB.NOCOL");
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "NOTAB.NOCOL"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testPatternLowerExact_TabNoExistColWild_NoMatches() throws GrepException {
        title("Neo4j", "cause", "NOTAB.*");
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "NOTAB.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
    }

    @Test
    public void testPatternLowerExact_TabWildColNonExist_NoMatches() throws GrepException {
        title("Neo4j", "cause", "*.NOCOL");
        String[] args = new String[] {
                "-d",
                //"-X", "debug", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "*.NOCOL"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(actual.isEmpty());
    }
    
    @Test
    public void testPatternMixedExact_TabWildColWild_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "cAuSe", "*.*");
        List<String> expected = Arrays.asList(
//                "CAUSES: [ID,NAME,CAUSE_STUFF]",
//                "CAUSES: 0,cause 1,the cause is mine",
//                "CAUSES: 1,cause 2,also mine"
                "Node[nnnn]:CAUSES {cause_stuff:\"the cause is mine\",name:\"cause 1\"}",
                "Node[nnnn]:CAUSES {cause_stuff,name:\"cause 2\"}",
                "Node[nnnn]:CAUSES {cause_stuff}"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug,trace", 
                "--warn",
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cAuSe", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternUpperExact_TabColWild_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "TED", "HISTORY.*");
        List<String> expected = Arrays.asList(
//                "HISTORY: [EVENT,AUTHOR]",
//                "HISTORY: completed all outstanding tasks!,craig",
//                "HISTORY: crafting new ideas,ted"
                "Node[nnnn]:HISTORY {EVENT:\"completed all outstanding tasks!\"}",
                "Node[nnnn]:HISTORY {Author:\"ted\"}"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "TED", "HISTORY.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternMixedExact_TabColWild_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "crA", "HISTORY.*");
        List<String> expected = Arrays.asList(
//                "HISTORY: [EVENT,AUTHOR]",
//                "HISTORY: completed all outstanding tasks!,craig",
//                "HISTORY: crafting new ideas,ted",
//                "HISTORY: More Crazy ideas,Stan"
                "Node[nnnn]:HISTORY {AUTHOR:\"craig\"}",
                "Node[nnnn]:HISTORY {EVENT:\"crafting new ideas\"}",
                "Node[nnnn]:HISTORY {EVENT:\"More Crazy ideas\"}"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "crA", "HISTORY.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabColWild2_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "stan", "HISTORY.*");
        List<String> expected = Arrays.asList(
//                "HISTORY: [EVENT,AUTHOR]",
//                "HISTORY: completed all outstanding tasks!,craig",
//                "HISTORY: More Crazy ideas,Stan"
                "Node[nnnn]:HISTORY {EVENT:\"completed all outstanding tasks!\"}",
                "Node[nnnn]:HISTORY {author:\"Stan\"}"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "stan", "HISTORY.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPatternLowerExact_TabCol2_NameData_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "stan", "HISTORY.AUTHOR");
        List<String> expected = Arrays.asList(
//                "HISTORY: [AUTHOR]",
//                "HISTORY: Stan"
                "Node[nnnn]:HISTORY {author:\"Stan\"}"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "stan", "HISTORY.AUTHOR"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_Listing_Case() throws GrepException {
        title("Neo4j (ignore case)", "cause", "*.*");
        List<String> expected = Arrays.asList(
                //"CAUSES: [ID,NAME,CAUSE_STUFF]"
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CAUSES {cause_stuff}"
            );
        String[] args = new String[] {
                "-d",
                "-l",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabColWild_Listing_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "cause", "*.*");
        List<String> expected = Arrays.asList(
                //"CAUSES: [ID,NAME,CAUSE_STUFF]"
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CAUSES {cause_stuff}",
                "Node[nnnn]:CAUSES {cause_stuff}"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                "-l",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "cause", "*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testPattern_TabPattColWild_TwoListing_IgnoreCase() throws GrepException {
        title("Neo4j (ignore case)", "n", "CA*.*");
        List<String> expected = Arrays.asList(
//                "CAUSES: [ID,NAME,CAUSE_STUFF]",
//                "CATALOG: [NAME]"
                "Node[nnnn]:CAUSES {name}",
                "Node[nnnn]:CAUSES {name}",
                "Node[nnnn]:CAUSES {name}",
                "Node[nnnn]:CATALOG {name}",
                "Node[nnnn]:CATALOG {chunk,name}"
            );
        String[] args = new String[] {
                "-d",
                "-i",
                "-l",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "n", "CA*.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }
    
    @Test
    public void testMixed_TabColWild_NumberData_Case() throws GrepException {
        title("Neo4j ", "1", "MIXED.*");
        List<String> expected = Arrays.asList(
                //"Node[nnnn]:MIXED {mxdblArr:[1.0, 2.0, 4.0],mxdbl:1.12345E2,mxfltArr:[1.0, 2.0, 4.0],mxflt:0.1234E02,mxstrArr:[\"one\", \"two\", \"three\"],mxstr:\"hello\",mxcharArr: \"abc\",mxchar: \"a\",mxlongArr: [1, 4, 8],mxlong: 2,mxintArr: [1, 2, 3],mxint: 1,mxbytearr: \"YSByYW5kb20gc3RyaW5n\",mxbyte: 127}"
                //"Node[nnnn]:MIXED {mxboolArr:[true, false, true],mxbool:true,mxdblArr:[1.0, 2.0, 4.0],mxdbl:112.345,mxfltArr:[1.0, 2.0, 4.0],mxflt:12.34,mxstrArr:["one", "two", "three"],mxstr:"hello",mxcharArr:"abc",mxchar:"a",mxlongArr:[1, 4, 8],mxlong:2,mxintArr:[1, 2, 3],mxint:1,mxbytearr:"YSByYW5kb20gc3RyaW5n",mxbyte:156}
                "Node[nnnn]:MIXED {mxdblArr:[1.0,,],mxdbl:112.345,mxfltArr:[1.0,,],mxflt:12.34,mxlongArr:[1,,],mxintArr:[1,,],mxint:1,mxbyte:156}"
            );
        String[] args = new String[] {
                "-d",
                //"-i",
                //"-l",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "1", "MIXED.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }
    
    @Test
    public void testMixed_TabColWild_LetterData_Case() throws GrepException {
        title("Neo4j ", "a", "MIXED.*");
        List<String> expected = Arrays.asList(
                //"Node[nnnn]:MIXED {mxdblArr: [1.0, 2.0, 4.0],mxdbl: 1.12345E2,mxfltArr: [1.0, 2.0, 4.0],mxflt: 0.1234E02,mxstrArr: [\"one\", \"two\", \"three\"],mxstr: \"hello\",mxcharArr: \"abc\",mxchar: \"a\",mxlongArr: [1, 4, 8],mxlong: 2,mxintArr: [1, 2, 3],mxint: 1,mxbytearr: \"YSByYW5kb20gc3RyaW5n\",mxbyte: 127}"
                //"Node[nnnn]:MIXED {mxboolArr:[,false,],mxcharArr:\"abc\",mxchar:\"a\",mxbytearr:\"YSByYW5kb20gc3RyaW5n\"}"
                "Node[nnnn]:MIXED {mxboolArr:[,false,],mxdblArr,mxfltArr,mxstrArr,mxcharArr:\"abc\",mxchar:\"a\",mxlongArr,mxintArr,mxbytearr:\"YSByYW5kb20gc3RyaW5n\"}"

            );
        String[] args = new String[] {
                "-d",
                //"-l",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "a", "MIXED.*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }
    
    @Test
    public void testMixed_TabColPatt_Data_IgnoreCase() throws GrepException {
        title("Neo4j ", "1", "MIXED.mxb*");
        List<String> expected = Arrays.asList(
                "Node[nnnn]:MIXED {mxbyte:156}"
            );
        String[] args = new String[] {
                "-d",
                //"-i",
                //"-l",
                //"-X", "debug,trace", 
                "-u", DB_USER, "-p", DB_PW, "-U", DB_URL,
                "1", "MIXED.mxb*"
        };
        ResourceGrep rg = new ResourceGrep(args);
        initDisplay(rg);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
            actualList(actual, expected),
            ListUtils.isEqualList(expected, actual));
    }
    
    private void initDisplay(ResourceGrep rg) {
        display = new DisplayRecorder(rg.getDisplay());
        display.setSortedPaths(false);
        // Filter out IDs, eg "Node[12345]:.."
        display.filter("\\[[0-9]+\\]", "[nnnn]");
        rg.setDisplay(display);
    }
}
