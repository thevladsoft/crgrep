/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.db.SqliteParser;
import com.ryanfuse.crgrep.util.DisplayRecorder;

/*
 * SQLite DB tests
 * 
 * To connect to this DB in DbVis I use this JDBC URL including drive:path
 *      jdbc:sqlite:d:/crgrep/core/src/test/databases/development.sqlite3
 * 
 * @author Craig Ryan
 */
public class SqliteDbGrepTest extends TestCommon {

	private static final String DB_URL = "jdbc:sqlite:" + projectDir() + "/src/test/databases/development.sqlite3";

	   @Test
	    public void testSqliteDefaults() throws GrepException {
	        SqliteParser hp = new SqliteParser("jdbc:sqlite:stuff", "user", "pw");
	        assertNotNull(hp.getDefaultPassword());
	        assertNotNull(hp.getDefaultUser());
	        assertEquals("user", hp.getUser());
	        assertEquals("pw", hp.getPassword());
	        assertEquals("jdbc:sqlite:stuff", hp.getUri());
	    }
	    
	    @Test
	    public void testPatternUpperExact_TabWildColWild_ByName_Case() throws GrepException {
	        title("Sqlite", "CAUSES", "*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]"
	        );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug,trace", 
	                "--warn",
	                "-U", DB_URL,
	                "CAUSES", "*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternMixedExact_TabWildColWild_ByName_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "StufF", "*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]"
	        );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "StufF", "*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabWildColWild_NameData_Case() throws GrepException {
	        title("Sqlite", "cause", "*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug,trace", 
	                "--warn",
	                "-U", DB_URL,
	                "cause", "*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabWildColWild_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "cause", "*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug,trace", 
	                "-i",
	                "-U", DB_URL,
	                "cause", "*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabWildColWild_TwoNameData_Case() throws GrepException {
	        title("Sqlite", "ca", "*");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]",
	                "catalog: cat",
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug,trace", 
	                "--warn",
	                "-U", DB_URL,
	                "ca", "*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabWildColWild_TwoNameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "cA", "*");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]",
	                "catalog: cat",
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	                );
	            String[] args = new String[] {
	                    "-d",
	                    //"-X", "debug,trace", 
	                    "-i",
	                    "-U", DB_URL,
	                    "cA", "*"
	            };
	            ResourceGrep rg = new ResourceGrep(args);
	            initDisplay(rg);
	            rg.execute();
	            List<String> actual = display.messages();
	            assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternMixedExact_TabWildColWild_NoMatches_Case() throws GrepException {
	        title("Sqlite", "MiNe", "*.*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	                );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "MiNe", "*.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternMixedExact_TabWildColWild_NoMatches_IgnoreCase() throws GrepException {
	        title("Sqlite", "SomePattern", "*.*");
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "--warn",
	                "-U", DB_URL,
	                "SomePattern", "*.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(actual.isEmpty());
	    }

	    @Test
	    public void testPatternLowerExact_TabColWild_ByName_Case() throws GrepException {
	        title("Sqlite", "CAT", "CATALOG.*");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]",
	                "catalog: cat"           // Sqlite ignores case since default collation is _ci
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "CAT", "CATALOG.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabColWild_ByName_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "CATA", "CATALOG.*");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug,trace", 
	                "-i",
	                "-U", DB_URL,
	                "CATA", "CATALOG.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabColWild_NameData_Case() throws GrepException {
	        title("Sqlite", "cause", "CAUSES.*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "cause", "CAUSES.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabColWild_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite", "cause", "CAUSES.*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "cause", "CAUSES.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabWild_ByName_Case() throws GrepException {
	        title("Sqlite", "TORY", "*.AUTHOR");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "TORY", "*.AUTHOR"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabWild_ByName_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "TOrY", "*.author");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "TOrY", "*.author"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabWild_NameData_Case() throws GrepException {
	        title("Sqlite", "n", "*.EVENT");
	        List<String> expected = Arrays.asList(
	            "history: [EVENT]",
	            "history: completed all outstanding tasks!",
	            "history: crafting new ideas"
	        );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-U", DB_URL,
	                "n", "*.EVENT"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternUpperExact_TabWild_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "N", "*.EVENT");
	        List<String> expected = Arrays.asList(
	                "history: [EVENT]",
	                "history: completed all outstanding tasks!",
	                "history: crafting new ideas"
	                );
	        String[] args = new String[] {
	                "-d",
	                "-i",
	                //"-X", "debug", 
	                "-U", DB_URL,
	                "N", "*.EVENT"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }


	    @Test
	    public void testPatternLowerExact_TabWild_TwoNameData_Case() throws GrepException {
	        title("Sqlite", "ca", "*.NAME");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]",
	                "catalog: cat",
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-U", DB_URL,
	                "ca", "*.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabWild_TwoNameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "ca", "*.NAME");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]",
	                "catalog: cat",
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                "-i",
	                //"-X", "debug", 
	                "-U", DB_URL,
	                "ca", "*.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }
	    

	    @Test
	    public void testPatternLowerExact_TabCol_ByName_Case() throws GrepException {
	        title("Sqlite", "TORY", "HISTORY.AUTHOR");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "TORY", "HISTORY.AUTHOR"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabCol_ByName_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "TOrY", "HISTORY.author");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "TOrY", "HISTORY.author"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabCol_NameData_Case() throws GrepException {
	        title("Sqlite", "cau", "CAUSES.NAME");
	        List<String> expected = Arrays.asList(
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "cau", "CAUSES.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabCol_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "cau", "CAUSES.NAME");
	        List<String> expected = Arrays.asList(
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "cau", "CAUSES.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColWild_ByName_Case() throws GrepException {
	        title("Sqlite", "C?T", "*.*");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]",
	                "catalog: cat"  // Sqlite case insensitive by default 
	        );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "C?T", "*.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColWild_ByName_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "*UFF", "*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]"
	        );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "*UFF", "*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColWild_NameData_Case() throws GrepException {
	        title("Sqlite", "c??s*", "*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug,trace", 
	                "--warn",
	                "-U", DB_URL,
	                "c??s*", "*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColWild_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "ca?S?", "*.*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug,trace", 
	                "-i",
	                "-U", DB_URL,
	                "ca?S?", "*.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColWild_ManyNameData_Case() throws GrepException {
	        title("Sqlite", "?a?*", "*");
	        List<String> expected = Arrays.asList(
	                "history: [EVENT,AUTHOR]",
	                "history: completed all outstanding tasks!,craig",
	                "history: crafting new ideas,ted",
	                "history: More Crazy ideas,Stan",
	                "catalog: [NAME]",
	                "catalog: cat",
	                "catalog: dan",            
	                "all_binary_columns: []",
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine",
	                "causes: 2,no match,nada"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug,trace", 
	                "--warn",
	                "-U", DB_URL,
	                "?a?*", "*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColWild_TwoNameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "Ca?", "*");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]",
	                "catalog: cat",
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug,trace", 
	                "-i",
	                "-U", DB_URL,
	                "Ca?", "*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColWild_NoMatches_Case() throws GrepException {
	        title("Sqlite", "M??E", "*.*");
	        List<String> expected = Arrays.asList(
	                "history: [EVENT,AUTHOR]",
	                "history: completed all outstanding tasks!,craig",
	                "history: More Crazy ideas,Stan",
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	                );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "M??E", "*.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColWild_NoMatches_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "S*Not?ed", "*.*");
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "--warn",
	                "-U", DB_URL,
	                "S*Not?ed", "*.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(actual.isEmpty());
	    }

	    @Test
	    public void testPattern_TabWild_ByName_Case() throws GrepException {
	        title("Sqlite", "?OR*", "*.AUTHOR");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "?OR*", "*.AUTHOR"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabWild_ByName_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "?Ory", "*.author");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "?Ory", "*.author"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabWild_NameData_Case() throws GrepException {
	        title("Sqlite", "?ing", "*.EVENT");
	        List<String> expected = Arrays.asList(
	            "history: [EVENT]",
	            "history: completed all outstanding tasks!",
	            "history: crafting new ideas"
	        );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-U", DB_URL,
	                "?ing", "*.EVENT"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabWild_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "?Ng ", "*.EVENT");
	        List<String> expected = Arrays.asList(
	                "history: [EVENT]",
	                "history: completed all outstanding tasks!",
	                "history: crafting new ideas"
	                );
	        String[] args = new String[] {
	                "-d",
	                "-i",
	                //"-X", "debug", 
	                "-U", DB_URL,
	                "?Ng", "*.EVENT"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }


	    @Test
	    public void testPattern_TabWild_TwoNameData_Case() throws GrepException {
	        title("Sqlite", "ca?", "*.NAME");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]",
	                "catalog: cat",
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-U", DB_URL,
	                "ca?", "*.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabWild_TwoNameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "ca?*", "*.NAME");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]",
	                "catalog: cat",
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	                );
	        String[] args = new String[] {
	                "-d",
	                "-i",
	                //"-X", "debug", 
	                "-U", DB_URL,
	                "ca?*", "*.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }
	     
	    @Test
	    public void testPattern_TabCol_ByName_Case() throws GrepException {
	        title("Sqlite", "TO?Y", "HISTORY.AUTHOR");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "TO?Y", "HISTORY.AUTHOR"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabCol_ByName_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "T?rY", "HISTORY.author");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "T?rY", "HISTORY.author"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabCol_NameData_Case() throws GrepException {
	        title("Sqlite", "cau?e", "CAUSES.NAME");
	        List<String> expected = Arrays.asList(
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "cau?e", "CAUSES.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabCol_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "cau??", "CAUSES.NAME");
	        List<String> expected = Arrays.asList(
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "cau??", "CAUSES.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColPatt_ByName_Case() throws GrepException {
	        title("Sqlite", "TO?Y", "HISTORY.AUT??R");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "TO?Y", "HISTORY.AUT??R"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColPatt_ByName_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "T?rY", "HISTORY.au*hor");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "T?rY", "HISTORY.au*hor"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColPatt_NameData_Case() throws GrepException {
	        title("Sqlite", "cau?e", "CAUSES.N?M*");
	        List<String> expected = Arrays.asList(
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "cau?e", "CAUSES.N?M*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColPatt_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "cau??", "CAUSES.?AME");
	        List<String> expected = Arrays.asList(
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "cau??", "CAUSES.?AME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }
	    
	    @Test
	    public void testPattern_TabPattCol_ByName_Case() throws GrepException {
	        title("Sqlite", "TO?Y", "H??TORY.AUTHOR");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "TO?Y", "H??TORY.AUTHOR"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabPattCol_ByName_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "T?rY", "HIS*RY.author");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "T?rY", "HIS*RY.author"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabPattCol_NameData_Case() throws GrepException {
	        title("Sqlite", "cau?e", "?AUSE?.NAME");
	        List<String> expected = Arrays.asList(
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "cau?e", "?AUSE?.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabPattCol_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "cau??", "?AUS*S.NAME");
	        List<String> expected = Arrays.asList(
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "cau??", "?AUS*S.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }
	    
	    @Test
	    public void testPattern_TabPattColPatt_ByName_Case() throws GrepException {
	        title("Sqlite", "TO?Y", "H??TORY.*UT?OR");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "TO?Y", "H??TORY.*UT?OR"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabPattColPatt_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "cau??", "?AUS*S.N*E");
	        List<String> expected = Arrays.asList(
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "cau??", "?AUS*S.N*E"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabPattColWild_ByName_Case() throws GrepException {
	        title("Sqlite", "TO?Y", "H??TORY.*");
	        List<String> expected = Arrays.asList(
	                "history: [EVENT,AUTHOR]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "TO?Y", "H??TORY.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabPattColWild_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "cau??", "?AUS*S");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "-i",
	                "-U", DB_URL,
	                "cau??", "?AUS*S"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	                actualList(actual, expected),
	                ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabColPatt_NameData_Case() throws GrepException {
	        title("Sqlite", "cause", "CAUSES.NA*");
	        List<String> expected = Arrays.asList(
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	        );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "cause", "CAUSES.NA*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabWildCol_NameData_Case() throws GrepException {
	        title("Sqlite", "cause", "*.NAME");
	        List<String> expected = Arrays.asList(
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "cause", "*.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabWildCol_TwoNameData_Case() throws GrepException {
	        title("Sqlite", "ca", "*.NAME");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]",
	                "catalog: cat",
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "ca", "*.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabPattCol_NoMatch_Case() throws GrepException {
	        title("Sqlite", "cause", "NOTAB*.CAUSE_STUFF");
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "cause", "NOTAB*.CAUSE_STUFF"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(actual.isEmpty());
	    }

	    @Test
	    public void testPatternLowerExact_TabPattCol_NameData_Case() throws GrepException {
	        title("Sqlite", "mine", "CA*.CAUSE_STUFF");
	        List<String> expected = Arrays.asList(
	                "causes: [CAUSE_STUFF]",
	                "causes: the cause is mine",
	                "causes: also mine"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "mine", "CA*.CAUSE_STUFF"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabPattCol_TwoNameData_Case() throws GrepException {
	        title("Sqlite", "ca", "CA*.NAME");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]",
	                "catalog: cat",
	                "causes: [NAME]",
	                "causes: cause 1",
	                "causes: cause 2"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "ca", "CA*.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabPatt_ByNameOnly_Case() throws GrepException {
	        title("Sqlite", "USES", "CA*.NAME");
	        List<String> expected = Arrays.asList(
	                "causes: [NAME]"
	            );
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "USES", "CA*.NAME"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabNoExistColNoExist_NoMatches_Case() throws GrepException {
	        title("Sqlite", "cause", "NOTAB.NOCOL");
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "cause", "NOTAB.NOCOL"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(actual.isEmpty());
	    }

	    @Test
	    public void testPatternLowerExact_TabNoExistColWild_NoMatches() throws GrepException {
	        title("Sqlite", "cause", "NOTAB.*");
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "cause", "NOTAB.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(actual.isEmpty());
	    }

	    @Test
	    public void testPatternLowerExact_TabWildColNonExist_NoMatches() throws GrepException {
	        title("Sqlite", "cause", "*.NOCOL");
	        String[] args = new String[] {
	                "-d",
	                //"-X", "debug", 
	                "--warn",
	                "-U", DB_URL,
	                "cause", "*.NOCOL"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(actual.isEmpty());
	    }
	    
	    @Test
	    public void testPatternMixedExact_TabWildColWild_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "cAuSe", "*.*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]",
	                "causes: 0,cause 1,the cause is mine",
	                "causes: 1,cause 2,also mine"
	            );
	        String[] args = new String[] {
	                "-d",
	                "-i",
	                //"-X", "debug,trace", 
	                "--warn",
	                "-U", DB_URL,
	                "cAuSe", "*.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternUpperExact_TabColWild_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "TED", "HISTORY.*");
	        List<String> expected = Arrays.asList(
	                "history: [EVENT,AUTHOR]",
	                "history: completed all outstanding tasks!,craig",
	                "history: crafting new ideas,ted"
	            );
	        String[] args = new String[] {
	                "-d",
	                "-i",
	                //"-X", "debug,trace", 
	                "-U", DB_URL,
	                "TED", "HISTORY.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternMixedExact_TabColWild_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "crA", "HISTORY.*");
	        List<String> expected = Arrays.asList(
	                "history: [EVENT,AUTHOR]",
	                "history: completed all outstanding tasks!,craig",
	                "history: crafting new ideas,ted",
	                "history: More Crazy ideas,Stan"
	            );
	        String[] args = new String[] {
	                "-d",
	                "-i",
	                //"-X", "debug,trace", 
	                "-U", DB_URL,
	                "crA", "HISTORY.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabColWild2_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "stan", "HISTORY.*");
	        List<String> expected = Arrays.asList(
	                "history: [EVENT,AUTHOR]",
	                "history: completed all outstanding tasks!,craig",
	                "history: More Crazy ideas,Stan"
	            );
	        String[] args = new String[] {
	                "-d",
	                "-i",
	                //"-X", "debug,trace", 
	                "-U", DB_URL,
	                "stan", "HISTORY.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPatternLowerExact_TabCol2_NameData_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "stan", "HISTORY.AUTHOR");
	        List<String> expected = Arrays.asList(
	                "history: [AUTHOR]",
	                "history: Stan"
	            );
	        String[] args = new String[] {
	                "-d",
	                "-i",
	                //"-X", "debug,trace", 
	                "-U", DB_URL,
	                "stan", "HISTORY.AUTHOR"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }


	    @Test
	    public void testPattern_TabColWild_Listing_Case() throws GrepException {
	        title("Sqlite (ignore case)", "cause", "*.*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]"
	            );
	        String[] args = new String[] {
	                "-d",
	                "-l",
	                //"-X", "debug,trace", 
	                "-U", DB_URL,
	                "cause", "*.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabColWild_Listing_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "cause", "*.*");
	        List<String> expected = Arrays.asList(
	                "causes: [ID,NAME,CAUSE_STUFF]"
	            );
	        String[] args = new String[] {
	                "-d",
	                "-i",
	                "-l",
	                //"-X", "debug,trace", 
	                "-U", DB_URL,
	                "cause", "*.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    @Test
	    public void testPattern_TabPattColWild_TwoListing_IgnoreCase() throws GrepException {
	        title("Sqlite (ignore case)", "n", "CA*.*");
	        List<String> expected = Arrays.asList(
	                "catalog: [NAME]",
	                "causes: [ID,NAME,CAUSE_STUFF]"
	            );
	        String[] args = new String[] {
	                "-d",
	                "-i",
	                "-l",
	                //"-X", "debug,trace", 
	                "-U", DB_URL,
	                "n", "CA*.*"
	        };
	        ResourceGrep rg = new ResourceGrep(args);
	        initDisplay(rg);
	        rg.execute();
	        List<String> actual = display.messages();
	        assertTrue(
	            actualList(actual, expected),
	            ListUtils.isEqualList(expected, actual));
	    }

	    private void initDisplay(ResourceGrep rg) {
	        display = new DisplayRecorder(rg.getDisplay());
	        display.setSortedPaths(false);
	        rg.setDisplay(display);
	    }
}
