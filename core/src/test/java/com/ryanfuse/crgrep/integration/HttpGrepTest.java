/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.integration;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.FileResourceList;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.HttpGrep;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.ResourceList;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Web page (HTTP GET) tests.
 * 
 * If this test fails due to assert failure, it is most likely google have changed the html around the favicon text.
 * 
 * @author Craig Ryan
 */
public class HttpGrepTest extends TestCommon {
    
	@Test
	public void testHttp() {
		title("http", "google_favicon", "http://www.google.com");
		List<String> expected = Arrays.asList(
			"http://www.google.com:<!doctype html><html itemscope=\"\" itemtype=\"http://schema.org/WebPage\" lang=\"en-AU\"><head><meta content=\"/images/google_favicon_128.png\" itemprop=\"image\"><title>Google</title><script>(function(){"
		);
		String[] args = new String[] {
			//"--warn", "-X", "debug",
			"google_favicon", "http://www.google.com" 
		};
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		try {
            rg.execute();
        } catch (GrepException e) {
            fail("unexpected error: : " + e.getLocalizedMessage());
        }
		List<String> actual = display.messages();
		assertTrue(
		        ListUtils.subtract(expected, actual).toString(),
		        ListUtils.isEqualList(expected, actual));
	}

    @Test
    public void testHttpInvalidUrl() {
        title("http", "nip", "not-a-url");
        Switches switches = new Switches();
        HttpGrep rg = new HttpGrep(switches);
        FileResourceList rl = new FileResourceList("not-a-url");
        rg.setArguments(null, rl);
        try {
            rg.execute();
            fail("expected malformed url exception");
        } catch (GrepException e) {
            // expected
            assertTrue(e.getCause() instanceof MalformedURLException);
        }
    }

    @Test
    public void testHttpUnreachableUrl() {
        title("http", "nip", "http://nolikelyasite.ryanfuse.com.nob100dyway/");
        String[] args = new String[] {
            //"--warn", "-X", "debug",
            "nip", "http://nolikelyasite.ryanfuse.com.nob100dyway/" 
        };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        try {
            rg.execute();
            fail("expected IO exception");
        } catch (GrepException e) {
            // expected
            assertTrue(e.getCause() instanceof IOException);
        }
    }
}
