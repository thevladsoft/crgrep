package com.ryanfuse.crgrep;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GrepExceptionTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGrepException() {
        GrepException ge = new GrepException("oops");
        assertEquals("oops", ge.getMessage());
    }

    @Test
    public void testGrepExceptionWithCause() {
        GrepException ge = new GrepException("oops", new RuntimeException("yikes"));
        assertEquals("oops", ge.getMessage());
        assertNotNull(ge.getCause());
        assertTrue(ge.getCause() instanceof RuntimeException);
        assertEquals("yikes", ge.getCause().getMessage());
    }

}
