/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.doReturn;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mysql.jdbc.Driver;
import com.ryanfuse.crgrep.ResourceList;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.util.Display;

/**
 * Test the MySQL parser against a mocked SQL interface.
 * 
 * @author 'Craig Ryan'
 */
@RunWith(MockitoJUnitRunner.class)
public class MysqlParserTest {

	private static final String URI = "jdbc:mysql:dummy";
	private static final String USER = "user";
	private static final String PW = "pw";
	
	private MysqlParser par;
	
	@Mock private Driver driver;
	@Mock private Connection conn;
	@Mock private Statement stmt;
	@Mock private ResultSet rs;
	@Mock private ResultSetMetaData rsmd;
	@Mock private DatabaseDriverManager dbDriverManager;
	@Mock private ResourceList resourceList;
			
	@Before
	public void setUp() throws Exception {
		par = new MysqlParser(URI, USER, PW);
		par.setDbDriver(driver);
		par.setDatabaseDriverManager(dbDriverManager);
		when(dbDriverManager.getConnection(any(String.class), any(String.class), any(String.class))).thenReturn(conn);
		when(conn.createStatement()).thenReturn(stmt);
		when(stmt.executeQuery(any(String.class))).thenReturn(rs);
		Properties parts = new Properties();
		parts.put(com.mysql.jdbc.Driver.DBNAME_PROPERTY_KEY, "db");
		when(driver.parseURL(any(String.class), any(Properties.class))).thenReturn(parts);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetDefaults() {
		assertEquals(par.MYSQL_DEF_USER, par.getDefaultUser());
		assertEquals(par.MYSQL_DEF_PW, par.getDefaultPassword());
		par.setVerbose(true);
		par.setDisplay(new Display());
		assertTrue(par.isVerbose());
		assertNotNull(par.getDisplay());
		assertNotNull(par.getDatabaseDriverManager());
		MysqlParser defaultPar = new MysqlParser(null, null, null);
        assertNotNull(defaultPar.getUser());
        assertEquals(defaultPar.getDefaultUser(), defaultPar.getUser());
        assertNotNull(defaultPar.getPassword());
        assertEquals(defaultPar.getDefaultPassword(), defaultPar.getPassword());
	}

	@Test
	public void testGetLogPrefix() {
		assertNotNull(par.getLogPrefix());
	}

	@Test
	public void testDbConnection() throws SQLException {
		try {
			Connection newConn = par.dbConnection();
			assertEquals(conn, newConn);
			// schemaName isn't set when mocked
			//assertEquals("db", par.getSchemaName());
		} catch (Exception e) {
			fail("unexpected error: " + e.getLocalizedMessage());
		}
		par.dbClose();
	}

	@Test
	public void testColumnIdentifiers() throws Exception {
		par.setIgnoreCase(false);
		String colId = par.columnIdentifier("myCol", par.isIgnoreCase(), false);
		assertNotNull(colId);
		assertEquals("`myCol`", colId);
		par.setIgnoreCase(true);
		colId = par.columnIdentifier("newcol", par.isIgnoreCase(), false);
		assertNotNull(colId);
		assertEquals(" UPPER(`newcol`) ", colId);
	}
	
    @Test
    public void testGetFullTableMap() throws Exception {
        when(rs.getString("TABLE_NAME")).thenReturn("tab1").thenReturn("tab2");
        when(rs.getString("COLUMN_NAME")).thenReturn("col1").thenReturn("col2");
        when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        
        Map<String, List<String>> ftm = par.getFullTableMap();
		
		assertNotNull(ftm);
		assertEquals(2, ftm.size());
		assertNotNull(ftm.get("tab1"));
		assertEquals(1, ftm.get("tab1").size());
		assertEquals("col1", ftm.get("tab1").get(0));
		assertNotNull(ftm.get("tab2"));
		assertEquals(1, ftm.get("tab2").size());
		assertEquals("col2", ftm.get("tab2").get(0));
	}

    @Test
    public void testResourceList() throws Exception {
        when(rs.getString("TABLE_NAME")).thenReturn("tab1").thenReturn("tab2");
        when(rs.getString("COLUMN_NAME")).thenReturn("col1").thenReturn("col2");
        when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);

        par.setResourceList(resourceList);

        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.firstPartPattern()).thenReturn("a");
        when(resourceList.lastPartPattern()).thenReturn("b");

        when(resourceList.firstPartContainsWild()).thenReturn(false); // use =
        when(resourceList.lastPartContainsWild()).thenReturn(false);

        Map<String, List<String>> ftm = par.getFullTableMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.firstPartPattern()).thenReturn("a");
        when(resourceList.lastPartPattern()).thenReturn("b");
        when(resourceList.firstPartContainsWild()).thenReturn(true); // use =
        when(resourceList.lastPartContainsWild()).thenReturn(false);

        ftm = par.getFullTableMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.firstPartPattern()).thenReturn("a");
        when(resourceList.lastPartPattern()).thenReturn("b");
        when(resourceList.firstPartContainsWild()).thenReturn(false);
        when(resourceList.lastPartContainsWild()).thenReturn(true);

        ftm = par.getFullTableMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.firstPartPattern()).thenReturn("a");
        when(resourceList.lastPartPattern()).thenReturn("b");
        when(resourceList.firstPartContainsWild()).thenReturn(true);
        when(resourceList.lastPartContainsWild()).thenReturn(true);

        ftm = par.getFullTableMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(true);
        when(resourceList.isLastPartWild()).thenReturn(true);

        ftm = par.getFullTableMap();
        
        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(true);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.lastPartPattern()).thenReturn("b");

        ftm = par.getFullTableMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(true);
        when(resourceList.firstPartPattern()).thenReturn("a");

        ftm = par.getFullTableMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(true);
        ftm = par.getFullTableMap();
    }

    @Test
    public void testGetColumnSubset() throws Exception {
        when(rs.getString("COLUMN_NAME")).thenReturn("col1").thenReturn("col2");
        when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        
        List<String> cs = par.getColumnSubset("tab1");
        
        assertNotNull(cs);
        assertEquals(2, cs.size());
        assertNotNull(cs.get(0));
        assertEquals("col1", cs.get(0));
        assertNotNull(cs.get(1));
        assertEquals("col2", cs.get(1));
    }

    @Test
    public void testGetColumnSubsetWithSchema() throws Exception {
        when(rs.getString("COLUMN_NAME")).thenReturn("col1").thenReturn("col2");
        when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);

        MysqlParser spyPar = spy(par);  // mock one method on real instance 
        doReturn("schema").when(spyPar).getSchemaName();
        List<String> cs = spyPar.getColumnSubset("tab1");
        assertNotNull(cs);
    }

    @Test
    public void testGetMatchingColumns() throws Exception {
        when(rs.getMetaData()).thenReturn(rsmd);
        when(rsmd.getColumnCount()).thenReturn(2);
        when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        // results will be 2 rows, 2 cols for table 'tab1'
        //  row1data1:row1data2
        //  row2data1:row2data2
        when(rs.getString(1)).thenReturn("row1data1").thenReturn("row2data1");
        when(rs.getString(2)).thenReturn("row1data2").thenReturn("row2data2");

        List<String> cl = new ArrayList<String>();
        cl.add("col1");
        cl.add("col2");
        String t = "tab1";
        
        ResourcePattern pattern = new ResourcePattern("col*");
        List<List<String>> mc = par.getMatchingColumns(cl, t, pattern);
		
		assertNotNull(mc);
		assertEquals(2, mc.size());
		assertNotNull(mc.get(0)); // row1
		assertEquals(2, mc.get(0).size());
		assertEquals("row1data1", mc.get(0).get(0));
		assertEquals("row1data2", mc.get(0).get(1));
		assertNotNull(mc.get(1)); // row2
		assertEquals(2, mc.get(1).size());
		assertEquals("row2data1", mc.get(1).get(0));
		assertEquals("row2data2", mc.get(1).get(1));
	}

    @Test
    public void testGetMatchingColumnsEmpty() throws Exception {
        List<String> cl = new ArrayList<String>();
        String t = "tab1";
        ResourcePattern pattern = new ResourcePattern("col*");
        List<List<String>> mc = par.getMatchingColumns(cl, t, pattern);
        assertNull(mc);
    }
}
