/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.neo4j.graphdb.Transaction;
import org.neo4j.rest.graphdb.RestAPI;
import org.neo4j.rest.graphdb.RestGraphDatabase;
import org.neo4j.rest.graphdb.entity.RestNode;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;
import org.neo4j.rest.graphdb.util.QueryResult;

import com.ryanfuse.crgrep.DbResourceList;
import com.ryanfuse.crgrep.ResourceList;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.util.Display;

/**
 * Test the NeoGraph parser against a mocked Rest + Cypher interface.
 * 
 * @author 'Craig Ryan'
 */
@RunWith(MockitoJUnitRunner.class)
public class NeoGraphParserTest {

	private static final String URI = "http://localhost:7474/db/data";
	private static final String USER = "user";
	private static final String PW = "pw";
	
	private NeoGraphParser par;
	
	@Mock private ResourceList resourceList;
	@Mock private RestCypherQueryEngine engine;
    @Mock private RestGraphDatabase graphdb;
    @Mock private Transaction tx;
    @Mock private RestAPI restApi;
    @Mock private Iterable<RestNode> iterableNodes;
    @Mock private Iterable<String> iterableProps;
    @Mock private RestNode restNode;
    
	@Before
	public void setUp() throws Exception {
		par = new NeoGraphParser(URI, USER, PW);
		par.setEngine(engine);
		par.setGraphDatabase(graphdb);
		when(graphdb.isAvailable(any(Long.class))).thenReturn(true);
		when(graphdb.beginTx()).thenReturn(tx);
		when(graphdb.getRestAPI()).thenReturn(restApi);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetDefaults() {
		assertEquals(par.NEO_DEF_USER, par.getDefaultUser());
		assertEquals(par.NEO_DEF_PW, par.getDefaultPassword());
		par.setVerbose(true);
        par.setDisplay(new Display());
		assertTrue(par.isVerbose());
		assertNotNull(par.getDisplay());
		NeoGraphParser defaultPar = new NeoGraphParser(null, null, null);
        assertNull(defaultPar.getUser());
        assertEquals(defaultPar.getDefaultUser(), defaultPar.getUser());
        assertNull(defaultPar.getPassword());
        assertEquals(defaultPar.getDefaultPassword(), defaultPar.getPassword());
	}

	@Test
	public void testGetLogPrefix() {
		assertNotNull(par.getLogPrefix());
	}

	@Test
	public void testDbConnection() throws SQLException {
		try {
			assertTrue(par.openConnection());
		} catch (Exception e) {
			fail("unexpected error: " + e.getLocalizedMessage());
		}
	}

    @Test
    public void testGetMatchingNodeMapTwoNodesTwoProperties() throws Exception {
        // Iterator mocks
        Iterator<RestNode> mockNodeIterator = mock(Iterator.class);
        Iterator<String> mockPropIterator = mock(Iterator.class);
        
        when(iterableNodes.iterator()).thenReturn(mockNodeIterator);
        when(iterableProps.iterator()).thenReturn(mockPropIterator);

        // two nodes
        when(mockNodeIterator.hasNext()).thenReturn(true, false, true, false);
        when(mockNodeIterator.next()).thenReturn(restNode, restNode);

        // node property iterators
        when(restNode.getId()).thenReturn(1L, 2L);
        when(restNode.getPropertyKeys()).thenReturn(iterableProps, iterableProps);
        
        // two properties for each node
        when(mockPropIterator.hasNext()).thenReturn(true, true, false, true, true, false);
        when(mockPropIterator.next()).thenReturn("prop1", "prop2", "prop3", "prop4");
        
        when(restApi.getNodesByLabel(any(String.class))).thenReturn(iterableNodes);

        // Label lookup
        Collection<String> labels = new ArrayList<String>();
        labels.add("label1");
        labels.add("label2");
        when(graphdb.getAllLabelNames()).thenReturn(labels);
        
        ResourceList rl = new DbResourceList("*.*");
        par.setDisplay(new Display());
        par.setResourceList(rl);
        Map<String, List<Map.Entry<String, List<String>>>> ftm = par.getMatchingNodeMap();
        
        assertNotNull(ftm);
        assertEquals(2, ftm.size()); // Two labels
        
        // 1st node
        List<Map.Entry<String, List<String>>> labelMap = ftm.get("label1");
        assertNotNull(labelMap);
        assertEquals(1, labelMap.size()); // one node id -> list<property> entry for this label
        
        Map.Entry<String, List<String>> nodeIdToProps = labelMap.get(0);
        assertNotNull(nodeIdToProps);
        String nodeId = nodeIdToProps.getKey();
        assertNotNull(nodeId);
        assertEquals("1", nodeId);
        List<String> props = nodeIdToProps.getValue();
        assertNotNull(props);
        assertEquals(2, props.size());
        assertEquals("prop1", props.get(0));
        assertEquals("prop2", props.get(1));
        
        // 2nd node
        labelMap = ftm.get("label2");
        assertNotNull(labelMap);
        assertEquals(1, labelMap.size()); // one node id -> list<property> entry for this label
        
        nodeIdToProps = labelMap.get(0);
        assertNotNull(nodeIdToProps);
        nodeId = nodeIdToProps.getKey();
        assertNotNull(nodeId);
        assertEquals("2", nodeId);
        props = nodeIdToProps.getValue();
        assertNotNull(props);
        assertEquals(2, props.size());
        assertEquals("prop3", props.get(0));
        assertEquals("prop4", props.get(1));
    }

    @Test
    public void testGetMatchingNodeMapTwoNodesOneWithProperties() throws Exception {
        // Iterator mocks
        Iterator<RestNode> mockNodeIterator = mock(Iterator.class);
        Iterator<String> mockPropIterator = mock(Iterator.class);
        
        when(iterableNodes.iterator()).thenReturn(mockNodeIterator);
        when(iterableProps.iterator()).thenReturn(mockPropIterator);

        // two nodes
        when(mockNodeIterator.hasNext()).thenReturn(true, true, false);
        when(mockNodeIterator.next()).thenReturn(restNode, restNode);

        // node property iterators
        when(restNode.getId()).thenReturn(1L, 2L);
        when(restNode.getPropertyKeys()).thenReturn(iterableProps, iterableProps);
        
        // one property for first node, no properties for next node
        when(mockPropIterator.hasNext()).thenReturn(true, false, false);
        when(mockPropIterator.next()).thenReturn("prop1");
        
        when(restApi.getNodesByLabel(any(String.class))).thenReturn(iterableNodes);

        // Label lookup
        Collection<String> labels = new ArrayList<String>();
        labels.add("label1");
        when(graphdb.getAllLabelNames()).thenReturn(labels);
        
        ResourceList rl = new DbResourceList("*.*");
        par.setDisplay(new Display());
//        par.getDisplay().setDebugOn(true);
        par.setResourceList(rl);
        Map<String, List<Map.Entry<String, List<String>>>> ftm = par.getMatchingNodeMap();
        
        assertNotNull(ftm);
        assertEquals(1, ftm.size()); // Two labels
        
        // 1st node
        List<Map.Entry<String, List<String>>> labelMap = ftm.get("label1");
        assertNotNull(labelMap);
        assertEquals(2, labelMap.size());
        
        // 1st node; 1 prop
        Map.Entry<String, List<String>> nodeIdToProps = labelMap.get(0);
        assertNotNull(nodeIdToProps);
        String nodeId = nodeIdToProps.getKey();
        assertNotNull(nodeId);
        assertEquals("1", nodeId);
        List<String> props = nodeIdToProps.getValue();
        assertNotNull(props);
        assertEquals(1, props.size());
        assertEquals("prop1", props.get(0));
        
        // 2nd node, no props
        nodeIdToProps = labelMap.get(1);
        assertNotNull(nodeIdToProps);
        nodeId = nodeIdToProps.getKey();
        assertNotNull(nodeId);
        assertEquals("2", nodeId);
        props = nodeIdToProps.getValue();
        assertNotNull(props);
        assertEquals(0, props.size());
    }

    @Test
    public void testGetMatchingNodeMapNoNodes() throws Exception {
        // Iterator mocks
        Iterator<RestNode> mockNodeIterator = mock(Iterator.class);
        Iterator<String> mockPropIterator = mock(Iterator.class);
        
        when(iterableNodes.iterator()).thenReturn(mockNodeIterator);
        when(iterableProps.iterator()).thenReturn(mockPropIterator);
        when(mockNodeIterator.hasNext()).thenReturn(false);
        when(restApi.getNodesByLabel(any(String.class))).thenReturn(iterableNodes);

        // Label lookup
        Collection<String> labels = new ArrayList<String>();
        labels.add("label1");
        when(graphdb.getAllLabelNames()).thenReturn(labels);
        
        ResourceList rl = new DbResourceList("*.*");
        par.setDisplay(new Display());
        par.getDisplay().setDebugOn(true);
        par.setResourceList(rl);
        Map<String, List<Map.Entry<String, List<String>>>> ftm = par.getMatchingNodeMap();
        
        assertNotNull(ftm);
        assertEquals(0, ftm.size());
    }

    @Test
    public void testMatchesPropertyValueSimpleIsMatched() throws Exception {
        StringBuilder match = new StringBuilder();
        QueryResult<Map<String, Object>> result = mock(QueryResult.class);
        Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
        when(result.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(true, false);
        Map<String, Object> matchingRow = genRow("val", new String("string"));
        when(mockResultIterator.next()).thenReturn(matchingRow);
        when(engine.query(any(String.class), any(Map.class))).thenReturn(result);

        ResourcePattern pattern = new ResourcePattern("str");
        assertTrue(par.matchesPropertyValue("123", "prop1", match, pattern));
    }

    @Test
    public void testMatchesPropertyValueSimpleNotMatched() throws Exception {
        StringBuilder match = new StringBuilder();
        QueryResult<Map<String, Object>> emptyResult = mock(QueryResult.class);
        Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
        when(emptyResult.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(false);
        when(engine.query(any(String.class), any(Map.class))).thenReturn(emptyResult);

        ResourcePattern pattern = new ResourcePattern("str");
        assertFalse(par.matchesPropertyValue("123", "prop1", match, pattern));
    }

    @Test
    public void testMatchesPropertyValueArrayIsMatched() throws Exception {
        StringBuilder match = new StringBuilder();
        QueryResult<Map<String, Object>> result = mock(QueryResult.class);
        Iterator<Map<String, Object>> mockResultIterator = mock(Iterator.class);
        when(result.iterator()).thenReturn(mockResultIterator);
        when(mockResultIterator.hasNext()).thenReturn(true, false);
        List<Integer> ints = new ArrayList<Integer>();
        ints.add(1);
        ints.add(10);
        ints.add(100);
        Map<String, Object> matchingRow = genRow("val", ints);
        when(mockResultIterator.next()).thenReturn(matchingRow);
        when(engine.query(any(String.class), any(Map.class))).thenReturn(result);

        ResourcePattern pattern = new ResourcePattern("1");
        assertTrue(par.matchesPropertyValue("123", "prop1", match, pattern));
    }

    @Test
    public void testResourceList() throws Exception {
        par.setResourceList(resourceList);

        when(resourceList.get(0)).thenReturn("node1.prop1");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.firstPartPattern()).thenReturn("node1");
        when(resourceList.lastPartPattern()).thenReturn("prop1");

        when(resourceList.firstPartContainsWild()).thenReturn(false); // use =
        when(resourceList.lastPartContainsWild()).thenReturn(false);

        Map<String, List<Map.Entry<String, List<String>>>> ftm = par.getMatchingNodeMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.firstPartPattern()).thenReturn("a");
        when(resourceList.lastPartPattern()).thenReturn("b");
        when(resourceList.firstPartContainsWild()).thenReturn(true); // use =
        when(resourceList.lastPartContainsWild()).thenReturn(false);

        ftm = par.getMatchingNodeMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.firstPartPattern()).thenReturn("a");
        when(resourceList.lastPartPattern()).thenReturn("b");
        when(resourceList.firstPartContainsWild()).thenReturn(false);
        when(resourceList.lastPartContainsWild()).thenReturn(true);

        ftm = par.getMatchingNodeMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.firstPartPattern()).thenReturn("a");
        when(resourceList.lastPartPattern()).thenReturn("b");
        when(resourceList.firstPartContainsWild()).thenReturn(true);
        when(resourceList.lastPartContainsWild()).thenReturn(true);

        ftm = par.getMatchingNodeMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(true);
        when(resourceList.isLastPartWild()).thenReturn(true);

        ftm = par.getMatchingNodeMap();
        
        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(true);
        when(resourceList.isLastPartWild()).thenReturn(false);
        when(resourceList.lastPartPattern()).thenReturn("b");

        ftm = par.getMatchingNodeMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(false);
        when(resourceList.isFirstPartWild()).thenReturn(false);
        when(resourceList.isLastPartWild()).thenReturn(true);
        when(resourceList.firstPartPattern()).thenReturn("a");

        ftm = par.getMatchingNodeMap();

        reset(resourceList);
        when(resourceList.get(0)).thenReturn("a.b");
        when(resourceList.isWild()).thenReturn(true);
        ftm = par.getMatchingNodeMap();
    }

    private Map<String, Object> genRow(String key, Object value) {
        Map<String, Object> row = new HashMap<String, Object>();
        row.put(key, value);
        return row;
    }
}
