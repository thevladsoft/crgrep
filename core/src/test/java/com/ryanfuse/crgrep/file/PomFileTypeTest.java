/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doThrow;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.sonatype.aether.resolution.DependencyResolutionException;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.file.pom.PomParser;
import com.ryanfuse.crgrep.util.Display;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Tests POM file type handler
 * 
 * @author 'Craig Ryan'
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class PomFileTypeTest {

    @Mock private PomParser pomParser;
    @Mock private FileGrep fileGrep;
    @Mock private File pomFile;
    @Mock private File depFile;
    @Mock private InputStream depStream;
    @Mock private ResourcePattern resourcePattern;
    private Pattern pattern;
    private DisplayRecorder display;

    @Before
    public void setUp() throws Exception {
        pattern = Pattern.compile(".*");
        display = new DisplayRecorder(new Display());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testNoProperties() {
        initFileGrep(null);
        PomFileType pt = new PomFileType(fileGrep);
    }

    @Test
    public void testDepthNotReached() {
        Properties p = new Properties();
        p.setProperty("maven.depth", "4");
        initFileGrep(p);

        PomFileType pt = new PomFileType(fileGrep);
        
        assertEquals(4, pt.getMaxDepth());

        // number format exception case
        p.setProperty("maven.depth", "abc");
        try {
            pt = new PomFileType(fileGrep);
            // error - defaults back to 1
            assertEquals(1, pt.getMaxDepth());
        } catch (Exception e) {
            fail("unexpected exception: " + e.getLocalizedMessage());
        }
    }
    
    @Test
    public void testDepthLimitReachedByFile() throws DependencyResolutionException, IOException, XmlPullParserException {
        Properties p = new Properties();
        p.setProperty("maven.depth", "0");
        initFileGrep(p);
        PomFileType pt = new PomFileType(fileGrep);
        pt = new PomFileType(fileGrep);

        pt.grepEntry(pomFile);
        // depth limit reached, wont get this far
        verify(pomParser, times(0)).parsePomFile(any(File.class));
    }

    @Test
    public void testDepthLimitReachedByStream() throws DependencyResolutionException, IOException, XmlPullParserException {
        Properties p = new Properties();
        p.setProperty("maven.depth", "0");
        initFileGrep(p);
        PomFileType pt = new PomFileType(fileGrep);
        pt = new PomFileType(fileGrep);

        pt.grepEntryStream(depStream, "foo.txt", "a/b/");
        // depth limit reached, wont get this far
        verify(pomParser, times(0)).parsePomFileStream(eq(depStream), any(String.class));
    }

    @Test
    public void testMatchesSupportedType() {
        initFileGrep(null);
        when(pomFile.getName()).thenReturn("pom.xml");
        when(fileGrep.isMaven()).thenReturn(true);
        
        PomFileType pt = new PomFileType(fileGrep);
        assertTrue(pt.matchesSupportedType(pomFile));
    }
    
    @Test
    public void testMatchesSupportedFileName() {
        initFileGrep(null);
        when(pomFile.getName()).thenReturn("pom.xml"); // valid pom file
        when(fileGrep.isMaven()).thenReturn(true); // -m 

        // valid
        PomFileType pt = new PomFileType(fileGrep);
        assertTrue(pt.matchesSupportedFileName("pom.xml"));

        // maven not enabled (-m)
        when(fileGrep.isMaven()).thenReturn(false);
        assertFalse(pt.matchesSupportedFileName("pom.xml"));

        // wrong file name
        when(pomFile.getName()).thenReturn("pom.txt");
        when(fileGrep.isMaven()).thenReturn(true);
        assertFalse(pt.matchesSupportedFileName("pom.txt"));
    }

    @Test
    public void testGrepEntry() {
        initFileGrep(null);
        PomFileType pt = new PomFileType(fileGrep);
        pt.setPomParser(pomParser);
        when(pomParser.getPrefix()).thenReturn("-> ");
        when(pomFile.getName()).thenReturn("pom.xml");
        when(pomFile.getPath()).thenReturn("a/b");
        when(fileGrep.isContent()).thenReturn(false);
        when(depFile.getName()).thenReturn("junit.foo");
        when(depFile.getPath()).thenReturn("deps/");
        List<File> deps = Arrays.asList(depFile);
        when(pomParser.getDependencyList()).thenReturn(deps);
        
        pt.grepEntry(pomFile);
    }

    @Test
    public void testGrepEntryDepResolutionException() throws DependencyResolutionException, IOException, XmlPullParserException {
        initFileGrep(null);
        PomFileType pt = new PomFileType(fileGrep);
        pt.setPomParser(pomParser);
        DependencyResolutionException e = new DependencyResolutionException(null, new RuntimeException("DependencyResolutionException"));
        doThrow(e).when(pomParser).parsePomFile(pomFile);
        when(pomFile.getName()).thenReturn("pom.xml");
        when(pomFile.getPath()).thenReturn("a/b");
        pt.grepEntry(pomFile);
        assertTrue(display.isWarnCalled());
        assertTrue(display.getWarnText().contains("DependencyResolutionException"));
    }

    @Test
    public void testGrepEntryStreamDepResolutionException() throws DependencyResolutionException, IOException, XmlPullParserException {
        initFileGrep(null);
        PomFileType pt = new PomFileType(fileGrep);
        pt.setPomParser(pomParser);
        DependencyResolutionException e = new DependencyResolutionException(null, new RuntimeException("DependencyResolutionException"));
        doThrow(e).when(pomParser).parsePomFileStream(eq(depStream), any(String.class));
        pt.grepEntryStream(depStream, "foo.txt", "a/b/");
        assertTrue(display.isWarnCalled());
        assertTrue(display.getWarnText().contains("DependencyResolutionException"));
    }

    @Test
    public void testGrepEntryXmlPullParserException() throws DependencyResolutionException, IOException, XmlPullParserException {
        initFileGrep(null);
        PomFileType pt = new PomFileType(fileGrep);
        pt.setPomParser(pomParser);
        XmlPullParserException e = new XmlPullParserException("XmlPullParserException");
        doThrow(e).when(pomParser).parsePomFile(pomFile);
        when(pomFile.getName()).thenReturn("pom.xml");
        when(pomFile.getPath()).thenReturn("a/b");
        pt.grepEntry(pomFile);
        assertTrue(display.isWarnCalled());
        assertTrue(display.getWarnText().contains("XmlPullParserException"));
    }

    @Test
    public void testGrepEntryStreamXmlPullParserException() throws DependencyResolutionException, IOException, XmlPullParserException {
        initFileGrep(null);
        PomFileType pt = new PomFileType(fileGrep);
        pt.setPomParser(pomParser);
        XmlPullParserException e = new XmlPullParserException("XmlPullParserException");
        doThrow(e).when(pomParser).parsePomFileStream(eq(depStream), any(String.class));
        pt.grepEntryStream(depStream, "foo.txt", "a/b/");
        assertTrue(display.isWarnCalled());
        assertTrue(display.getWarnText().contains("XmlPullParserException"));
    }

    @Test
    public void testGrepEntryIOException() throws DependencyResolutionException, IOException, XmlPullParserException {
        initFileGrep(null);
        PomFileType pt = new PomFileType(fileGrep);
        pt.setPomParser(pomParser);
        IOException e = new IOException("IOException");
        doThrow(e).when(pomParser).parsePomFile(pomFile);
        when(pomFile.getName()).thenReturn("pom.xml");
        when(pomFile.getPath()).thenReturn("a/b");
        pt.grepEntry(pomFile);
        assertTrue(display.isWarnCalled());
        assertTrue(display.getWarnText().contains("IOException"));
    }

    @Test
    public void testGrepEntryStreamIOException() throws DependencyResolutionException, IOException, XmlPullParserException {
        initFileGrep(null);
        PomFileType pt = new PomFileType(fileGrep);
        pt.setPomParser(pomParser);
        IOException e = new IOException("IOException");
        doThrow(e).when(pomParser).parsePomFileStream(eq(depStream), any(String.class));
        pt.grepEntryStream(depStream, "foo.txt", "a/b/");
        assertTrue(display.isWarnCalled());
        assertTrue(display.getWarnText().contains("IOException"));
    }

    @Test
    public void testGrepEntryNoDeps() {
        initFileGrep(null);
        PomFileType pt = new PomFileType(fileGrep);
        pt.setPomParser(pomParser);
        when(pomParser.getPrefix()).thenReturn("-> ");
        when(pomFile.getName()).thenReturn("pom.xml");
        when(pomFile.getPath()).thenReturn("a/b");
        when(fileGrep.isContent()).thenReturn(false);
        List<File> nodeps = Collections.EMPTY_LIST;
        when(pomParser.getDependencyList()).thenReturn(nodeps);
        
        pt.grepEntry(pomFile);
    }

    @Test
    public void testGrepEntryStream() {
        initFileGrep(null);
        PomFileType pt = new PomFileType(fileGrep);
        pt.setPomParser(pomParser);
        when(pomParser.getPrefix()).thenReturn("-> ");
        when(pomFile.getName()).thenReturn("pom.xml");
        when(pomFile.getPath()).thenReturn("a/b");
        when(depFile.getName()).thenReturn("junit.foo");
        when(depFile.getPath()).thenReturn("deps/");
        List<File> deps = Arrays.asList(depFile);
        when(pomParser.getDependencyList()).thenReturn(deps);
        
        pt.grepEntryStream(depStream, "foo.txt", "a/b/");
    }

    @Test
    public void testGetPomParser() {
        initFileGrep(null);
        PomFileType pt = new PomFileType(fileGrep);
        pt.setPomParser(null);
        PomParser newPP = pt.getPomParser();
        assertNotNull(newPP);
    }

    private void initFileGrep(Properties p) {
        Switches sw = new Switches();
        sw.setUserProperties(p);
        when(fileGrep.getSwitches()).thenReturn(sw);
        when(fileGrep.getResourcePattern()).thenReturn(resourcePattern);
        when(fileGrep.getDisplay()).thenReturn(display);
        when(resourcePattern.getPattern()).thenReturn(pattern);
    }
}
