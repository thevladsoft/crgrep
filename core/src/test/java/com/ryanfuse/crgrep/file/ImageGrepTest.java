/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.any;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import org.apache.commons.collections.ListUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.drew.imaging.ImageMetadataReader;
import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.GrepException;
import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.Display;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Test for matches against specific meta data stored within an image files 
 * and mocked OCR text extraction tests. 
 * 
 * @author Craig Ryan
 */
@RunWith(MockitoJUnitRunner.class)
public class ImageGrepTest extends TestCommon {

    @Mock private Tesseract ocrReader;
    @Mock private FileGrep fileGrep;
    @Mock private ResourcePattern resourcePattern;

    private Pattern pattern;
    private DisplayRecorder display;
    
    @Before
    public void setUp() throws Exception {
        pattern = Pattern.compile(".*");
    }
    
	@Test
	public void testImageEasyShooting() throws GrepException {
		List<String> expected = Arrays.asList(
				"src/test/resources/images/Canon IXUS v3.jpg: @{ExposureMode=Easy shooting}",
				"src/test/resources/images/Canon PowerShot S300.jpg: @{ExposureMode=Easy shooting}"
				);
		title("images easy shooting", "hoo", "src/test/resources/images/Can*");
        String[] args = new String[] { "-r", 
                //"-X", "debug,trace", 
                "hoo", "src/test/resources/images/Can*" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

    @Test
    public void testImageDPI() throws GrepException {
        List<String> expected = Arrays.asList(
            "src/test/resources/images/Canon EOS D60.jpg: @{ResolutionInfo=300.0x300.0 DPI}",
            "src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{ResolutionInfo=300.0x300.0 DPI}"
            );
        title("images DPI", "DPI", "src/test/resources/images");
        String[] args = new String[] { "-r", 
                //"-X", "debug",
                "DPI", "src/test/resources/images" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testImageDPIIgnoreCase() throws GrepException {
        List<String> expected = Arrays.asList(
            "src/test/resources/images/Canon EOS D60.jpg: @{ResolutionInfo=300.0x300.0 DPI}",
            "src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{ResolutionInfo=300.0x300.0 DPI}"
            );
        title("images DPI", "DpI", "src/test/resources/images");
        String[] args = new String[] { "-r", 
                //"-X", "debug",
                "-i",
                "DpI", "src/test/resources/images" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testImageDPIWild() throws GrepException {
        List<String> expected = Arrays.asList(
            "src/test/resources/images/Canon EOS D60.jpg: @{ResolutionInfo=300.0x300.0 DPI}",
            "src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{ResolutionInfo=300.0x300.0 DPI}"
            );
        title("images DPI", "D?I", "src/test/resources/images");
        String[] args = new String[] { "-r", 
                //"-X", "debug",
                "D?I", "src/test/resources/images" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

    @Test
    public void testImageDPIWildIgnoreCase() throws GrepException {
        List<String> expected = Arrays.asList(
            //"src/test/resources/images/Canon EOS D60.jpg: @{ResolutionInfo=300.0x300.0 DPI}",
            //"src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{ResolutionInfo=300.0x300.0 DPI}"
            "src/test/resources/images/Canon EOS D60.jpg: @{ResolutionInfo=300.0x300.0 DPI}",
            "src/test/resources/images/Epson PhotoPC 3000Z.jpg: @{SceneType=Directly photographed image}",
            "src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{ResolutionInfo=300.0x300.0 DPI}",
            "src/test/resources/images/Minolta DiMAGE X.jpg: @{SceneType=Directly photographed image}",
            "src/test/resources/images/Nikon D1X.jpg: @{SceneType=Directly photographed image}",
            "src/test/resources/images/Nikon E5000.jpg: @{SceneType=Directly photographed image}",
            "src/test/resources/images/Olympus C4040Z.jpg: @{SceneType=Directly photographed image}",
            "src/test/resources/images/Sony Cybershot (3).jpg: @{SceneType=Directly photographed image}",
            "src/test/resources/images/Sony DigitalMavica.jpg: @{SceneType=Directly photographed image}"
            );
        title("images DPI", "D?i", "src/test/resources/images");
        String[] args = new String[] { "-r", 
                //"-X", "debug",
                "-i", "D?i", "src/test/resources/images" };
        ResourceGrep rg = new ResourceGrep(args);
        display = new DisplayRecorder(rg.getDisplay());
        rg.setDisplay(display);
        rg.execute();
        List<String> actual = display.messages();
        assertTrue(
                actualList(actual, expected),
                ListUtils.isEqualList(expected, actual));
    }

	@Test
	public void testImageDotsPerInch() throws GrepException {
		List<String> expected = Arrays.asList(
			"src/test/resources/images/Apple iPhone 4.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Canon EOS D60.jpg: @{XResolution=300 dots per inch, YResolution=300 dots per inch}",
			"src/test/resources/images/Canon EOS D60.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Canon IXUS v3.jpg: @{XResolution=180 dots per inch, YResolution=180 dots per inch}",
			"src/test/resources/images/Canon PowerShot S300.jpg: @{XResolution=180 dots per inch, YResolution=180 dots per inch}",
			"src/test/resources/images/Epson PhotoPC 3000Z.jpg: @{XResolution=480 dots per inch, YResolution=480 dots per inch}",
			"src/test/resources/images/Epson PhotoPC 3000Z.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/FujiFilm DS-7 (2).jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{XResolution=300 dots per inch, YResolution=300 dots per inch}",
			"src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/HP PhotoSmart 735.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Kodak DC210.jpg: @{XResolution=216 dots per inch, YResolution=216 dots per inch}",
			"src/test/resources/images/Kodak DC210.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Minolta DiMAGE X.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Nikon D1X.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Nikon E5000.jpg: @{XResolution=300 dots per inch, YResolution=300 dots per inch}",
			"src/test/resources/images/Olympus C4040Z.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Pentax Optio S4.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Ricoh DC-3Z (low res).jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Sanyo SR6.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Sony Cybershot (3).jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Sony DigitalMavica.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}"
			);
		title("images dots per inch", "dots per inch", "src/test/resources/images");
		String[] args = new String[] { "-r", /*"-X", "debug,trace",*/ "dots per inch", "src/test/resources/images" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testImageDotsPerInchNoContent() throws GrepException {
		List<String> expected = Arrays.asList(
			"src/test/resources/images/Apple iPhone 4.jpg",
			"src/test/resources/images/Canon EOS D60.jpg",
			"src/test/resources/images/Canon IXUS v3.jpg",
			"src/test/resources/images/Canon PowerShot S300.jpg",
			"src/test/resources/images/Epson PhotoPC 3000Z.jpg",
			"src/test/resources/images/FujiFilm DS-7 (2).jpg",
			"src/test/resources/images/FujiFilm FinePixS2Pro.jpg",
			"src/test/resources/images/HP PhotoSmart 735.jpg",
			"src/test/resources/images/Kodak DC210.jpg",
			"src/test/resources/images/Minolta DiMAGE X.jpg",
			"src/test/resources/images/Nikon D1X.jpg",
			"src/test/resources/images/Nikon E5000.jpg",
			"src/test/resources/images/Olympus C4040Z.jpg",
			"src/test/resources/images/Pentax Optio S4.jpg",
			"src/test/resources/images/Ricoh DC-3Z (low res).jpg",
			"src/test/resources/images/Sanyo SR6.jpg",
			"src/test/resources/images/Sony Cybershot (3).jpg",
			"src/test/resources/images/Sony DigitalMavica.jpg"
			);
		title("images dots per inch no content", "dots", "src/test/resources/images");
		String[] args = new String[] { "-r", "-l", /*"-X", "debug",*/ "dots", "src/test/resources/images" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testImageUserComment() throws GrepException {
		List<String> expected = Arrays.asList(
				"src/test/resources/images/Kodak DC210.jpg: @{JpegComment=Lovely smoke effect here, of which I'm secretly very proud.}"
				);
        title("images user comment", "smoke", "src/test/resources/images");
		String[] args = new String[] { "-r", /*"-X", "debug",*/ "smoke", "src/test/resources/images" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}
	
    @Test
    public void testOCR() throws GrepException, TesseractException {
        title("image OCR", "*", "src/test/resources/images/Apple iPhone 4.jpg");
        initFileGrep(true, null);
        ImageFileType image = new ImageFileType(fileGrep);
        image.setTesseractReader(ocrReader);
        when(ocrReader.doOCR(any(BufferedImage.class))).thenReturn("text");
        image.grepEntry(new File("src/test/resources/images/Apple iPhone 4.jpg"));
        verify(ocrReader).doOCR(any(BufferedImage.class));
    }

    @Test
    public void testOCRStream() throws GrepException, TesseractException, FileNotFoundException {
        title("image OCR Stream", "*", "src/test/resources/images/Apple iPhone 4.jpg");
        initFileGrep(true, null);
        ImageFileType image = new ImageFileType(fileGrep);
        image.setTesseractReader(ocrReader);
        when(ocrReader.doOCR(any(BufferedImage.class))).thenReturn("text");
        image.grepEntryStream(new FileInputStream("src/test/resources/images/Apple iPhone 4.jpg"), "entryName", "entryPath");
        verify(ocrReader).doOCR(any(BufferedImage.class));
    }

    private void initFileGrep(boolean isOcr, Properties p) {
        Switches sw = new Switches();
        sw.setOcr(isOcr);
        sw.setUserProperties(p);
        display = new DisplayRecorder(new Display());
        when(fileGrep.getSwitches()).thenReturn(sw);
        when(fileGrep.getResourcePattern()).thenReturn(resourcePattern);
        when(fileGrep.getDisplay()).thenReturn(display);
        when(resourcePattern.getPattern()).thenReturn(pattern);
    }
}
