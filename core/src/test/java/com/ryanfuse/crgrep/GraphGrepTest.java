package com.ryanfuse.crgrep;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.neo4j.graphdb.Transaction;
import org.neo4j.rest.graphdb.RestAPI;
import org.neo4j.rest.graphdb.RestGraphDatabase;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;

import com.ryanfuse.crgrep.db.NeoGraphParser;
import com.ryanfuse.crgrep.util.Switches;

@RunWith(MockitoJUnitRunner.class)
public class GraphGrepTest {

    private static final String URI = "http://localhost:7474/db/data";
    private static final String USER = "user";
    private static final String PW = "pw";
    
    private Switches switches;
    @Mock private NeoGraphParser par;
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        switches = new Switches();
        Map<String, List<Map.Entry<String, List<String>>>> ftm = new HashMap<String, List<Map.Entry<String, List<String>>>>();  
        List<Map.Entry<String, List<String>>> nodeList = new ArrayList<Map.Entry<String, List<String>>>();
        List<String> props = new ArrayList<String>();
        props.add("prop");
        Map.Entry<String, List<String>> entry = new AbstractMap.SimpleEntry("1234", props);
        nodeList.add(entry);
        ftm.put("label", nodeList);
        when(par.getMatchingNodeMap()).thenReturn(ftm);
        when(par.openConnection()).thenReturn(true);
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void testGrepGraphExecuteNoMatchingNodes() throws Exception {
        GraphGrep gg = new GraphGrep(switches);
        // empty
        when(par.getMatchingNodeMap()).thenReturn(new HashMap<String, List<Map.Entry<String, List<String>>>>());
        gg.setParser(par);
        ResourceList<String> resList = new DbResourceList("*.*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteNoConnection() throws Exception {
        GraphGrep gg = new GraphGrep(switches);
        // no connection
        when(par.openConnection()).thenReturn(false);
        gg.setParser(par);
        ResourceList<String> resList = new DbResourceList("*.*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteListing() {
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        ResourceList<String> resList = new DbResourceList("*.*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            e.printStackTrace();
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteListingResourceListNotWild() {
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        ResourceList<String> resList = new DbResourceList("*.prop1");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            e.printStackTrace();
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteListingResourceListContainsWild() {
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        ResourceList<String> resList = new DbResourceList("*.prop?");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            e.printStackTrace();
            fail("exception: " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testGrepGraphExecuteContent() {
        switches.setAllcontent(true);
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        ResourceList<String> resList = new DbResourceList("*.*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*"));
        try {
            gg.execute();
        } catch (GrepException e) {
            e.printStackTrace();
            fail("exception: " + e.getLocalizedMessage());
        }
    }
    
    @Test
    public void testGrepGraphExecuteCleanupGrepException() throws GrepException {
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        doThrow(new GrepException("yikes")).when(par).cleanup();
        ResourceList<String> resList = new DbResourceList("*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*.*"));
        try {
            gg.execute();
            fail("exception expected");
        } catch (GrepException e) {
            assertEquals("yikes", e.getMessage());
            assertNull(e.getCause());
        }
    }

    @Test
    public void testGrepGraphExecuteCleanupUnknownException() throws GrepException {
        GraphGrep gg = new GraphGrep(switches);
        gg.setParser(par);
        doThrow(new RuntimeException("yikes")).when(par).cleanup();
        ResourceList<String> resList = new DbResourceList("*");
        gg.setResourceList(resList);
        gg.setResourcePattern(new ResourcePattern("*.*"));
        try {
            gg.execute();
            fail("exception expected");
        } catch (GrepException e) {
            assertEquals("crgrep: failed graph database grep", e.getMessage());
            assertNotNull(e.getCause());
            assertTrue(e.getCause() instanceof RuntimeException);
            assertEquals("yikes", e.getCause().getMessage());
        }
    }
}
