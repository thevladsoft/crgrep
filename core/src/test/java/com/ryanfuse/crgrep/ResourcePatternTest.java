/*
 * (C) Copyright 2013-2014 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import static org.junit.Assert.*;

import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ResourcePatternTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetPattern() {
		ResourcePattern rp = new ResourcePattern("");
		Pattern p = rp.getPattern();
		assertNotNull(p);
		rp = new ResourcePattern("xyz");
		assertNotNull(rp.getPatternString());
		assertEquals("xyz", rp.getPatternString());
	}

	@Test
	public void testRegularExpression() {
		ResourcePattern rp = new ResourcePattern("foo");
		String re = rp.getRegularExpression();
		assertNotNull(re);
		assertEquals("foo", re);
		assertFalse(rp.isRegularExpressionWildcard());
		assertFalse(rp.regularExpressionContainsWildcard());

		rp = new ResourcePattern("*");
		re = rp.getRegularExpression();
		assertNotNull(re);
		assertEquals(".*", re);
		assertTrue(rp.isRegularExpressionWildcard());
		assertTrue(rp.regularExpressionContainsWildcard());

		rp = new ResourcePattern("foo?");
		re = rp.getRegularExpression();
		assertNotNull(re);
		assertEquals("foo.", re);
		assertFalse(rp.isRegularExpressionWildcard());
		assertTrue(rp.regularExpressionContainsWildcard());

		rp = new ResourcePattern("foo*");
		re = rp.getRegularExpression();
		assertNotNull(re);
		assertEquals("foo.*", re);
		assertFalse(rp.isRegularExpressionWildcard());
		assertTrue(rp.regularExpressionContainsWildcard());

		rp = new ResourcePattern("?foo*bah?*");
		re = rp.getRegularExpression();
		assertNotNull(re);
		assertEquals(".foo.*bah..*", re);
		assertFalse(rp.isRegularExpressionWildcard());
		assertTrue(rp.regularExpressionContainsWildcard());

		rp = new ResourcePattern("(foo|bah)\\*");
		re = rp.getRegularExpression();
		assertNotNull(re);
		assertEquals("\\(foo\\|bah\\)\\\\.*", re);
		assertFalse(rp.isRegularExpressionWildcard());
		assertTrue(rp.regularExpressionContainsWildcard());
	}

	@Test
	public void testDatabaseExpression() {
		ResourcePattern rp = new ResourcePattern("foo");
		String de = rp.getDatabaseExpression();
		assertNotNull(de);
		assertEquals("foo", de);
		assertFalse(rp.isDatabaseExpressionWildcard());
		assertFalse(rp.databaseExpressionContainsWildcard());
		
		rp = new ResourcePattern("*");
		de = rp.getDatabaseExpression();
		assertNotNull(de);
		assertEquals("%", de);
		assertTrue(rp.isDatabaseExpressionWildcard());
		assertTrue(rp.databaseExpressionContainsWildcard());

		rp = new ResourcePattern("foo?");
		de = rp.getDatabaseExpression();
		assertNotNull(de);
		assertEquals("foo_", de);
		assertFalse(rp.isDatabaseExpressionWildcard());
		assertTrue(rp.databaseExpressionContainsWildcard());

		rp = new ResourcePattern("foo*");
		de = rp.getDatabaseExpression();
		assertNotNull(de);
		assertEquals("foo%", de);
		assertFalse(rp.isDatabaseExpressionWildcard());
		assertTrue(rp.databaseExpressionContainsWildcard());

		rp = new ResourcePattern("?foo*bah?*");
		de = rp.getDatabaseExpression();
		assertNotNull(de);
		assertEquals("_foo%bah_%", de);
		assertFalse(rp.isDatabaseExpressionWildcard());
		assertTrue(rp.databaseExpressionContainsWildcard());

		rp = new ResourcePattern("*foo");
		de = rp.getDatabaseExpression();
		assertNotNull(de);
		assertEquals("%foo", de);
		assertFalse(rp.isDatabaseExpressionWildcard());
		assertTrue(rp.databaseExpressionContainsWildcard());
	}
}
